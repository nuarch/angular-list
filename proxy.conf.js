const PROXY_CONFIG =

  {
    "/login**": {
      "target": "http://10.155.229.102:62772/",
      "secure": false
    },

    "/me**": {
      "target": "http://10.155.229.102:62772/",
      "secure": false
    },

    "/logout**": {
      "target": "http://10.155.229.102:62772/",
      "secure": false
    },

    "/lawmgmt/api/list.json": {
      "target": "http://localhost:3000/",
      "pathRewrite": {
        "^/lawmgmt/api/list.json": "/laws"
      },
      "secure": false
    },

    "/refdata/api/items/date/legalPrefixes": {
      "target": "http://localhost:3000/",
      "pathRewrite": {
        "^/refdata/api/items/date/legalPrefixes": "/legalPrefixes"
      },
      "secure": false
    },

    "/lawmgmt/api/details": {
      "target": "http://localhost:3000/",
      "pathRewrite": {
        "^/lawmgmt/api/details": "/laws/details"
      },
      "secure": false
    },

    "/refdata/api/items/date/**": {
      "target": "http://localhost:4200/",
      "pathRewrite": {
        "^/refdata/api/items/date/": "/assets/data/"
      },
      "secure": false
    }
  };


module.exports = PROXY_CONFIG;

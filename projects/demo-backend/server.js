// Test in memory express server for testing angular-list

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

let data = require("./db.json");

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/laws', (req, res) => {
  const paginationFields = new Set(["page", "size", "sort"]);
  const page = (!req.query["page"])? 0 : Number(req.query["page"]);
  const size = (!req.query["size"])? data.laws.length : Number(req.query["size"]);
  const sort = (!req.query["sort"])? "id,asc": req.query["sort"];
  let [sortField, ascending] = ["id", true];
  if (sort) {
    const sortQueryDecomposed = sort.split(",");
    if (sortQueryDecomposed[0]) {
      sortField = sortQueryDecomposed[0];
    }
    if (sortQueryDecomposed.length > 1 && sortQueryDecomposed[1]) {
      ascending = sortQueryDecomposed[1].toLocaleUpperCase() === "ASC";
    }
  }
  const pagedLaws = [... data.laws].sort((a, b) => {
    if (a[sortField] < b[sortField]) {
      return ascending? -1: 1;
    }
    if (a[sortField] > b[sortField]) {
      return ascending? 1: -1;
    }
    return 0;
  }).filter(law => {
    for(let field in req.query) {
      if (field === "search") {
        if(!law["lawSection"].includes(req.query["search"] || "") &&
          !law["abbreviatedLaw"].includes(req.query["search"]) || "") {
          return false;
        }
      } else if (!paginationFields.has(field)) {
        if (law[field] && law[field] !== req.query[field]) {
          return false;
        }
      }
    }
    return true;
  }).slice(page * size, (page + 1) * size);
  res.send({
    page: page,
    size: size,
    content: pagedLaws
  });
});

app.get('/laws/details/:id', (req, res) => {
  if (!req.params["id"]) {
    return res.status(400);
  }
  const id = Number(req.params["id"]);
  const filteredLaws = data.laws.filter(law => law["id"] === id);
  if(filteredLaws.length < 1) {
    return res.status(404);
  }
  res.send(filteredLaws[0]);
});

app.post('/laws/details', (req, res) => {
  const sorted = data.laws.sort((a, b) => {
    if (a["id"] < b["id"]) {
      return -1;
    }
    if (a["id"] > b["id"]) {
      return 1;
    }
    return 0;
  });
  const id = (!sorted || sorted.length < 1)? 0: sorted[sorted.length - 1]["id"] + 1
  const newLaw = {... req.body, id: id};
  data.laws = [... data.laws, newLaw];
  res.send(newLaw);
})

app.put('/laws/details/:id', (req, res) => {
  if (!req.params["id"]) {
    return res.status(400);
  }
  const id = Number(req.params["id"]);
  if(data.laws.filter(law => law["id"] === id).length < 1) {
    return res.status(400);
  }
  const updatedLaw = {... req.body, id: id};
  data.laws = data.laws.map(law => (law["id"] === id)?updatedLaw:law);
  res.send(updatedLaw);
});

app.delete('/laws/details/:id', (req, res) => {
  if (!req.params["id"]) {
    return res.status(400);
  }
  const id = Number(req.params["id"]);
  if(data.laws.filter(law => law["id"] === id).length < 1) {
    return res.status(400);
  }
  const lawToDelete = {... data.laws[id], id: id};
  data.laws = data.laws.filter(law => law["id"] !== id);
  res.send(lawToDelete);
});

app.get("/legalPrefixes", (req, res) => {
  res.send(data.legalPrefixes);
});

const port = 3000;
app.listen(port, () => console.log(`Listening on port ${port}!`))



// Karma configuration file, see link for more information
// https://karma-runner.github.io/0.13/config/configuration-file.html

const isDocker = require('is-docker')();

module.exports = function (config) {
  config.set({
    basePath: '',
    browserDisconnectTimeout: 120000,
    captureTimeout: 120000,
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    jasmineNodeOpts: {
      defaultTimeoutInterval: 120000,
    },
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    jasmineHtmlReporter: {
      suppressAll: true // removes the duplicated traces
    },
    customLaunchers: {
      ChromeCustom: {
        base: isDocker ? 'ChromeHeadless' : 'Chrome',
        // We must disable the Chrome sandbox when running Chrome inside Docker (Chrome's sandbox needs
        // more permissions than Docker allows by default)
        flags: isDocker ? ['--no-sandbox', '--headless'] : ['--no-sandbox']
      }
    },
    mime: {
      'text/x-typescript': ['ts', 'tsx']
    },
    coverageReporter: {
      dir: require('path').join(__dirname, '../../coverage/nuarch/angular-lib'),
      subdir: '.',
      reporters: [
        { type: 'html' },
        { type: 'text-summary' }
      ]
    },
    angularCli: {
      environment: 'dev'
    },
    reporters: config.angularCli && config.angularCli.codeCoverage
      ? ['progress', 'coverage-istanbul']
      : ['progress', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeCustom'],
    singleRun: false,
    reportSlowerThan: 100
  });

};

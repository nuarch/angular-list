import { Injectable, Type } from '@angular/core';
import { FormControl, ValidatorFn, Validators } from '@angular/forms';
import { concat, find, flatMap, get, isArray, isEqual, isNil, uniq } from 'lodash';
import { BehaviorSubject } from 'rxjs';
import { Entity } from '../../core/models/entity';
import { ReferenceData } from '../../core/models/reference-data';
import { autocompleteValidator } from '../../shared/validators/autocomplete-validator';
import { phoneValidator } from '../../shared/validators/phone-validator';
import { ssnValidator } from '../../shared/validators/ssn-validator';
import { zipCodeValidator } from '../../shared/validators/zip-code-validator';
import { NuDynamicAutocompleteComponent } from '../dynamic-elements/dynamic-autocomplete/dynamic-autocomplete.component';
import { NuDynamicCheckboxComponent } from '../dynamic-elements/dynamic-checkbox/dynamic-checkbox.component';
import { NuDynamicDatepickerComponent } from '../dynamic-elements/dynamic-datepicker/dynamic-datepicker.component';
import { NuDynamicFileInputComponent } from '../dynamic-elements/dynamic-file-input/dynamic-file-input.component';

import { NuDynamicInputComponent } from '../dynamic-elements/dynamic-input/dynamic-input.component';
import { NuDynamicSelectComponent } from '../dynamic-elements/dynamic-select/dynamic-select.component';
import { NuDynamicSlideToggleComponent } from '../dynamic-elements/dynamic-slide-toggle/dynamic-slide-toggle.component';
import { NuDynamicSliderComponent } from '../dynamic-elements/dynamic-slider/dynamic-slider.component';
import { NuDynamicTextareaComponent } from '../dynamic-elements/dynamic-textarea/dynamic-textarea.component';
import { NuDynamicRadioButtonComponent } from '../dynamic-elements/dynamic-radio-button/dynamic-radio-button.component';
import moment from 'moment';

export enum NuDynamicType {
  Text = 'text',
  Boolean = 'boolean',
  Number = 'number',
  Array = 'array',
  Date = 'date',
}

export enum NuFilterType {
  StartsWith = 'startsWith',
  Contains = 'contains',
}

export enum NuDynamicElement {
  Input = 'input',
  Datepicker = 'datepicker',
  Password = 'password',
  Textarea = 'textarea',
  Slider = 'slider',
  SlideToggle = 'slide-toggle',
  Checkbox = 'checkbox',
  Select = 'select',
  FileInput = 'file-input',
  Autocomplete = 'autocomplete',
  Radio = 'radio,',
}

export interface INuDynamicElementValidator {
  validator: ValidatorFn;
}

export const defaultDateFormat: string = 'MM/DD/YYYY';

export interface INuDynamicElementConfig {
  label?: string;
  name: string;
  hint?: string;
  row?: number;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  type: NuDynamicType | NuDynamicElement | Type<any>;
  required?: boolean;
  disabled?: boolean;
  hidden?: boolean;
  readonly?: boolean;
  showClear?: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  min?: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  max?: any;
  appearance?: string;
  referenceData?: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  minLength?: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  maxLength?: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  selections?: string[] | { value: any; label: string; displayText: string }[] | BehaviorSubject<any>;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  concatToSelection?: any;
  filter?: boolean;
  filterType?: NuFilterType;
  matchFunction?: Function;
  loading?: BehaviorSubject<boolean>;
  multiple?: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  default?: any;
  flex?: number;
  dateFormat?: string;
  displayWith?: Function;
  validators?: INuDynamicElementValidator[];
}

export const DYNAMIC_ELEMENT_NAME_REGEX: RegExp = /^[^0-9][^\@]*$/;

@Injectable({
  providedIn: 'root',
})
export class NuDynamicFormsService {

  readonly customValidators: { [validatorName: string]: ValidatorFn } = {
    phone: phoneValidator,
    zipCode: zipCodeValidator,
    email: Validators.email,
    ssn: ssnValidator,
  };

  /**
   * Method to validate if the [name] is a proper element name.
   * Throws error if name is not valid.
   */
  validateDynamicElementName(name: string): void {
    if (!DYNAMIC_ELEMENT_NAME_REGEX.test(name)) {
      throw new Error('Dynamic element name: "${name}" is not valid.');
    }
  }

  /**
   * Gets component to be rendered depending on [NuDynamicElement | NuDynamicType]
   * Throws error if it does not exists or not supported.
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getDynamicElement(element: NuDynamicElement | NuDynamicType | Type<any>, readonly: boolean = false): any {
    if (readonly) {
      // eslint-disable-next-line eqeqeq
      if (element == NuDynamicElement.Textarea) {
        return NuDynamicTextareaComponent;
      }

      // eslint-disable-next-line eqeqeq
      if (element == NuDynamicElement.Autocomplete) {
        return NuDynamicAutocompleteComponent;
      }

      // eslint-disable-next-line eqeqeq
      if (element == NuDynamicElement.Select) {
        return NuDynamicSelectComponent;
      }

      return NuDynamicInputComponent;
    }

    switch (element) {
      case NuDynamicType.Text:
      case NuDynamicType.Number:
      case NuDynamicElement.Input:
      case NuDynamicElement.Password:
        return NuDynamicInputComponent;
      case NuDynamicElement.Textarea:
        return NuDynamicTextareaComponent;
      case NuDynamicType.Boolean:
      case NuDynamicElement.SlideToggle:
        return NuDynamicSlideToggleComponent;
      case NuDynamicElement.Checkbox:
        return NuDynamicCheckboxComponent;
      case NuDynamicElement.Slider:
        return NuDynamicSliderComponent;
      case NuDynamicType.Array:
      case NuDynamicElement.Select:
        return NuDynamicSelectComponent;
      case NuDynamicElement.Autocomplete:
        return NuDynamicAutocompleteComponent;
      case NuDynamicElement.FileInput:
        return NuDynamicFileInputComponent;
      case NuDynamicElement.Datepicker:
      case NuDynamicType.Date:
        return NuDynamicDatepickerComponent;
      case NuDynamicElement.Radio:
        return NuDynamicRadioButtonComponent;
      default:
        throw new Error(`Error: type ${element} does not exist or not supported.`);
    }
  }

  /**
   * Creates form control for element depending [INuDynamicElementConfig] properties.
   */
  createFormControl(config: INuDynamicElementConfig): FormControl {
    const validator: ValidatorFn = this.createValidators(config);
    return new FormControl({value: config.default, disabled: config.disabled}, validator);
  }

  /**
   * Creates form validationdepending [INuDynamicElementConfig] properties.
   */
  createValidators(config: INuDynamicElementConfig): ValidatorFn|null {
    let validator: ValidatorFn;
    if (config.required) {
      validator = Validators.required;
    }
    if (config.max || config.max === 0) {
      validator = Validators.compose([validator, Validators.max(parseFloat(config.max))]);
    }
    if (config.min || config.min === 0) {
      validator = Validators.compose([validator, Validators.min(parseFloat(config.min))]);
    }
    if (config.maxLength || config.maxLength === 0) {
      validator = Validators.compose([validator, Validators.maxLength(parseFloat(config.maxLength))]);
    }
    if (config.minLength || config.minLength === 0) {
      validator = Validators.compose([validator, Validators.minLength(parseFloat(config.minLength))]);
    }
    // Add provided custom validators to the validator function
    if (config.validators) {
      config.validators.forEach((validatorConfig: INuDynamicElementValidator) => {
        validator = Validators.compose([validator, validatorConfig.validator]);
      });
    }
    // eslint-disable-next-line no-null/no-null
    return validator || null;
  }

  getReferenceDataValue(referenceData: ReferenceData[], value: { correlation: string } | string): ReferenceData {
    if (!value) {
      return undefined;
    }

    return find(referenceData, (rd: ReferenceData) => {
      const referenceDataCorrelationId: string = get(rd, 'correlationId.correlation');
      const correlation: string = get(value, 'correlation');
      if (!!referenceDataCorrelationId) {
        return isEqual(correlation, referenceDataCorrelationId) || isEqual(value, referenceDataCorrelationId);
      }

      return false;
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  mapFormData(entity: Entity, elements: INuDynamicElementConfig[], referenceData: { [type: string]: ReferenceData[] }): any {
    return flatMap(elements, (element: INuDynamicElementConfig) => this.buildElement(element, entity, referenceData));
  }

  private isSelectionElement(element: INuDynamicElementConfig): boolean {
    return element.type === NuDynamicElement.Autocomplete || element.type === NuDynamicElement.Select;
  }

  private isDatePicker(element: INuDynamicElementConfig): boolean {
    return element.type === NuDynamicElement.Datepicker;
  }

  private buildNonSelectElement(element: INuDynamicElementConfig, entity: Entity): INuDynamicElementConfig {
    const initialValue: string = get(entity, element.name);
    return {
      ...element,
      default: isNil(initialValue) ? element.default : initialValue,
    } as INuDynamicElementConfig;
  }

  private buildDatePickerElement(element: INuDynamicElementConfig, entity: Entity): INuDynamicElementConfig {
    const initialValue: string = get(entity, element.name);
    const value: string = isNil(initialValue) ? element.default : initialValue;
    return {
      ...element,
      default: isNil(value) ? value : moment(value).toDate(),
    } as INuDynamicElementConfig;
  }

  private selections(referenceDataType: string, referenceData: { [type: string]: ReferenceData[] },
                     // eslint-disable-next-line @typescript-eslint/no-explicit-any
                     concatToSelection: any): BehaviorSubject<ReferenceData[]> {
    const subject: BehaviorSubject<ReferenceData[]> = new BehaviorSubject<ReferenceData[]>(undefined);
    if (!!referenceData) {
      if (!!concatToSelection) {
        subject.next(uniq(concat(isArray(concatToSelection) ? concatToSelection : [concatToSelection],
          get(referenceData, referenceDataType))));
      } else {
        subject.next(get(referenceData, referenceDataType));
      }

    }
    return subject;
  }

  private buildSelectionElement(element: INuDynamicElementConfig, entity: Entity,
                                referenceData: { [type: string]: ReferenceData[] }): INuDynamicElementConfig {
    const selectionsSubject: BehaviorSubject<ReferenceData[]> = this.selections(element.referenceData, referenceData, element.concatToSelection);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const formValue: any = get(entity, element.name);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const initialValue: any = get(formValue, 'correlation') ?
      this.getReferenceDataValue(get(element, 'selections.value') || get(referenceData, element.referenceData), formValue) : formValue;
    return {
      ...element,
      selections: element.selections ? element.selections : selectionsSubject,
      default: isNil(initialValue) ? element.default : initialValue,
      validators: element.type === NuDynamicElement.Autocomplete ? [{validator: autocompleteValidator(selectionsSubject)}] : undefined,
    } as INuDynamicElementConfig;
  }

  private buildElement(element: INuDynamicElementConfig, entity: Entity,
                       referenceData: { [type: string]: ReferenceData[] }): INuDynamicElementConfig {
    if (this.isSelectionElement(element)) {
      return this.buildSelectionElement(element, entity, referenceData);
    }
    if (this.isDatePicker(element)) {
      return this.buildDatePickerElement(element, entity);
    }
    return this.buildNonSelectElement(element, entity);
  }

}

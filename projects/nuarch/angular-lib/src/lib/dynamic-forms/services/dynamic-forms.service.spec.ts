import { async, inject, TestBed } from '@angular/core/testing';
import { NuDynamicElement, NuDynamicFormsService, NuDynamicType } from './dynamic-forms.service';

describe('Service: NuDynamicFormsService', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [NuDynamicFormsService],
    });
  }));

  it('expect to validate element names correctly', async(
    inject([NuDynamicFormsService], (service: NuDynamicFormsService) => {
      try {
        service.validateDynamicElementName('normal-name');
      } catch (e) {
        expect(e).toBeFalsy('name should be validated correctly');
      }

      try {
        service.validateDynamicElementName('normal_22_name');
      } catch (e) {
        expect(e).toBeFalsy('name should be validated correctly');
      }

      try {
        service.validateDynamicElementName('normal_22-name_22');
      } catch (e) {
        expect(e).toBeFalsy('name should be validated correctly');
      }

      try {
        service.validateDynamicElementName('王先生');
      } catch (e) {
        expect(e).toBeFalsy('王先生 (chinese) name should be validated correctly');
      }

      try {
        service.validateDynamicElementName('日本語');
      } catch (e) {
        expect(e).toBeFalsy('日本語 (japanese) name should be validated correctly');
      }

      try {
        service.validateDynamicElementName('2normal_22-name_22');
        expect(false).toBeTruthy('2normal_22-name_22 name should not be validated correctly');
      } catch (e) {
        /* */
      }

      try {
        service.validateDynamicElementName('normal@22-name_22');
        expect(false).toBeTruthy('normal@22-name_22 name should not be validated correctly');
      } catch (e) {
        /* */
      }
    }),
  ));

  it('expect to return components depending on type | element', async(
    inject([NuDynamicFormsService], (service: NuDynamicFormsService) => {
      expect(service.getDynamicElement(NuDynamicType.Text)).toBeTruthy();
      expect(service.getDynamicElement(NuDynamicType.Number)).toBeTruthy();
      expect(service.getDynamicElement(NuDynamicType.Boolean)).toBeTruthy();
      expect(service.getDynamicElement(NuDynamicType.Array)).toBeTruthy();

      expect(service.getDynamicElement(NuDynamicElement.Input)).toBeTruthy();
      expect(service.getDynamicElement(NuDynamicElement.Textarea)).toBeTruthy();
      expect(service.getDynamicElement(NuDynamicElement.Checkbox)).toBeTruthy();
      expect(service.getDynamicElement(NuDynamicElement.Slider)).toBeTruthy();
      expect(service.getDynamicElement(NuDynamicElement.SlideToggle)).toBeTruthy();
      expect(service.getDynamicElement(NuDynamicElement.Select)).toBeTruthy();

      try {
        expect(service.getDynamicElement(undefined)).toBeFalsy('expect not to return a component');
      } catch (e) {
        expect(e).toBeTruthy();
      }
    }),
  ));
});

import { Component, TemplateRef } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'nuarch-dynamic-input',
  styleUrls: ['./dynamic-input.component.scss'],
  templateUrl: './dynamic-input.component.html',
})
export class NuDynamicInputComponent {
  control: FormControl;

  label: string = '';

  hint: string = '';

  type: string = undefined;

  required: boolean = undefined;

  appearance: string = '';

  name: string = '';

  min: number = undefined;

  max: number = undefined;

  minLength: number = undefined;

  maxLength: number = undefined;

  readonly: boolean = false;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  errorMessageTemplate: TemplateRef<any> = undefined;

  hidden: boolean = false;

  showClear: boolean = false;

  clear(control: FormControl): void {
    control.patchValue('', {emitEvent: true});
  }
}

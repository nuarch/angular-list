import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'nuarch-dynamic-radio-button',
  templateUrl: './dynamic-radio-button.component.html',
  styleUrls: ['./dynamic-radio-button.component.scss'],
})
export class NuDynamicRadioButtonComponent {

  control: FormControl;

  label: string = '';

  hint: string = '';

  name: string = '';

  required: boolean = undefined;

  appearance: string = '';

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  selections: Observable<any[]> | any[] = undefined;

  hidden: boolean = false;

  readonly: boolean = false;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  errorMessageTemplate: TemplateRef<any> = undefined;

  get isAsyncSelections(): boolean {
    return this.selections instanceof Observable;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  get asyncSelections(): Observable<any> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return this.selections as Observable<any>;
  }

}

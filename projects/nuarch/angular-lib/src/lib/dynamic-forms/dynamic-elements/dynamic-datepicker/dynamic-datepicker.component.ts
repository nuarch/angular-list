import { Component, TemplateRef } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'nuarch-dynamic-datepicker',
  styleUrls: ['./dynamic-datepicker.component.scss'],
  templateUrl: './dynamic-datepicker.component.html',
})
export class NuDynamicDatepickerComponent {
  control: FormControl;

  label: string = '';

  hint: string = '';

  name: string = '';

  appearance: string = '';

  type: string = undefined;

  required: boolean = undefined;

  min: number = undefined;

  max: number = undefined;

  readonly: boolean = false;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  errorMessageTemplate: TemplateRef<any> = undefined;

  hidden: boolean = false;

  showClear: boolean = false;

  clear(control: FormControl): void {
    control.patchValue('', {emitEvent: true});
  }
}

import { Component, TemplateRef } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'nuarch-dynamic-file-input',
  styleUrls: ['./dynamic-file-input.component.scss'],
  templateUrl: './dynamic-file-input.component.html',
})
export class NuDynamicFileInputComponent {
  control: FormControl;

  required: boolean = undefined;

  label: string = '';

  name: string = '';

  hint: string = '';

  appearance: string = '';

  readonly: boolean = false;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  errorMessageTemplate: TemplateRef<any> = undefined;

  hidden: boolean = false;

  _handlefileDrop(value: File): void {
    this.control.setValue(value);
  }
}

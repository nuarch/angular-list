import { Component, OnInit, ViewChild } from '@angular/core';
import { get, includes, isEqual, startsWith, toLower } from 'lodash';
import { isObservable, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NuDynamicSelectComponent } from '../dynamic-select/dynamic-select.component';
import { NuFilterType } from '../../services/dynamic-forms.service';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';

@Component({
  selector: 'nuarch-dynamic-autocomplete',
  styleUrls: ['./dynamic-autocomplete.component.scss'],
  templateUrl: './dynamic-autocomplete.component.html',
})
export class NuDynamicAutocompleteComponent extends NuDynamicSelectComponent implements OnInit {

  @ViewChild(MatAutocompleteTrigger, {static: false}) autocomplete: MatAutocompleteTrigger;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  filteredSelections: any[];
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  filteredSelectionsAsync: Observable<any>;

  get isAsyncSelections(): boolean {
    return isObservable(this.selections);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  get asyncSelections(): Observable<any> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return this.selections as Observable<any>;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  get syncSelections(): any[] {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return this.selections as any[];
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  autocompleteDisplay(selection: any): string {

    if (get(selection, 'displayText')) {
      return selection.displayText;
    }

    const code: string = get(selection, 'code');
    if (code) {
      const description: string = get(selection, 'description');

      if (description && !isEqual(description, code)) {
        return `${code} - ${description}`;
      }

      return code;
    }

    return selection;
  }

  ngOnInit(): void {
    window.addEventListener('scroll', this.scrollEvent, true);

    if (this.isAsyncSelections) {
      this.filteredSelectionsAsync = this.asyncSelections;

      if (this.filter) {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this.control.valueChanges.subscribe((value: any) => {
          this.filteredSelectionsAsync = this.filterSelectionsAsync(value);
        });
      }
    } else {
      this.filteredSelections = this.filterSelections(this.control.value);

      if (this.filter) {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this.control.valueChanges.subscribe((value: any) => {
          this.filteredSelections = this.filterSelections(value);
        });
      }
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  scrollEvent = (event: any): void => {
    if (this.autocomplete.panelOpen && !includes(event.srcElement.classList, 'mat-autocomplete-panel')) {
      this.autocomplete.closePanel();
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  filterSelections(value: any): any[] {
    if (!value) {
      return this.syncSelections;
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return this.syncSelections.filter(((selection: any) => this.isValueMatched(selection, value)));

  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  filterSelectionsAsync(value: any): Observable<any[]> {
    if (!value) {
      return this.asyncSelections;
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return this.asyncSelections.pipe(map((selections: any[]) => selections.filter((selection: any) =>
      this.isValueMatched(selection, value))));

  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  isValueMatched(selection: any, value: any): boolean {
    const compareWith: string = toLower(value);
    if (selection.displayText) {
      return this.matches(get(selection, 'displayText'), compareWith);
    }
    if (selection.correlation || selection.correlationId) {
      return this.matches(get(selection, 'correlationId.correlation', get(selection, 'correlation')), compareWith)
        || this.matches(get(selection, 'description'), compareWith)
        || this.matches(get(selection, 'code'), compareWith);
    }
    return this.matches(selection.label ? selection.label : selection, compareWith);
  }

  matches(compareTo: string, compareWith: string): boolean {
    if (this.filterType === NuFilterType.Contains) {
      return includes(toLower(this.displayWith ? this.displayWith(compareTo) : compareTo), toLower(compareWith));
    } else {
      return startsWith(toLower(this.displayWith ? this.displayWith(compareTo) : compareTo), toLower(compareWith));
    }
  }

}

import { Component, TemplateRef, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { NuFilterType } from '../../services/dynamic-forms.service';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { get,  includes as _includes, remove } from 'lodash';

@Component({
  selector: 'nuarch-dynamic-select',
  styleUrls: ['./dynamic-select.component.scss'],
  templateUrl: './dynamic-select.component.html',
})
export class NuDynamicSelectComponent {

  @ViewChild('trigger', {read: MatAutocompleteTrigger, static: false}) trigger: MatAutocompleteTrigger;

  control: FormControl;

  label: string = '';

  hint: string = '';

  name: string = '';

  required: boolean = undefined;

  appearance: string = '';

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  selections: Observable<any[]> | any[] = undefined;

  loading: Observable<boolean> = undefined;

  multiple: boolean = undefined;

  hidden: boolean = false;

  readonly: boolean = false;

  filter: boolean = true;

  displayWith: Function;

  filterType: NuFilterType;

  showClear: boolean;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  errorMessageTemplate: TemplateRef<any> = undefined;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  optionTemplate: TemplateRef<any> = undefined;

  get isAsyncSelections(): boolean {
    return this.selections instanceof Observable;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  get asyncSelections(): Observable<any> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return this.selections as Observable<any>;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  isValueMatched(selection: any, value: any): boolean {
    const valueCorrelation: string = get(value, 'correlation');
    const selectionCorrelation: string = get(selection, 'correlation');
    const selectionCorrelationId: string = get(selection, 'correlationId.correlation');

    if ((selectionCorrelation || selectionCorrelationId)) {
      return _includes(remove([selectionCorrelation, selectionCorrelationId], undefined), valueCorrelation);
    }

    return selection === value;
  }

  clear(control: FormControl): void {
    control.patchValue('', {emitEvent: true});
    setTimeout(() => {
      this.trigger.openPanel();
    });
  }

}

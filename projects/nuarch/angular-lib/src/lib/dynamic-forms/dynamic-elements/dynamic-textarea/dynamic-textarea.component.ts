import { Component, TemplateRef } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'nuarch-dynamic-textarea',
  styleUrls: ['./dynamic-textarea.component.scss'],
  templateUrl: './dynamic-textarea.component.html',
})
export class NuDynamicTextareaComponent {
  control: FormControl;

  label: string = '';

  hint: string = '';

  name: string = '';

  required: boolean = undefined;

  hidden: boolean = false;

  minLength: number = undefined;

  maxLength: number = undefined;

  appearance: string = '';

  readonly: boolean = false;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  errorMessageTemplate: TemplateRef<any> = undefined;
}

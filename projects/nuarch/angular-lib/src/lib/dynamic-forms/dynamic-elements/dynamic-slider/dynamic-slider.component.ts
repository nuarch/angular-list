import { ChangeDetectorRef, Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'nuarch-dynamic-slider',
  styleUrls: ['./dynamic-slider.component.scss'],
  templateUrl: './dynamic-slider.component.html',
})
export class NuDynamicSliderComponent {
  control: FormControl;

  label: string = '';

  required: boolean = undefined;

  name: string = '';

  hint: string = '';

  min: number = undefined;

  max: number = undefined;

  hidden: boolean = false;

  readonly: boolean = false;

  constructor(private _changeDetectorRef: ChangeDetectorRef) {
  }

  _handleBlur(): void {
    setTimeout(() => {
      this._changeDetectorRef.markForCheck();
    });
  }
}

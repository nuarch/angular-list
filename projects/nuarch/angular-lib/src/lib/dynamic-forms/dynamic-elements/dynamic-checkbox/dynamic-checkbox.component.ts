import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'nuarch-dynamic-checkbox',
  styleUrls: ['./dynamic-checkbox.component.scss'],
  templateUrl: './dynamic-checkbox.component.html',
})
export class NuDynamicCheckboxComponent {
  control: FormControl;

  label: string = '';

  name: string = '';

  hint: string = '';

  required: boolean = false;

  readonly: boolean = false;

  hidden: boolean = false;
}

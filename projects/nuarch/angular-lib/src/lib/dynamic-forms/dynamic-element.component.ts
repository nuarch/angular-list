import { TemplatePortalDirective } from '@angular/cdk/portal';
import {
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  forwardRef,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  TemplateRef,
  Type,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';

import { IControlValueAccessor, mixinControlValueAccessor } from '../shared/behaviors';

import { NuDynamicElement, NuDynamicFormsService, NuDynamicType, NuFilterType } from './services/dynamic-forms.service';
import { Observable } from 'rxjs/internal/Observable';

export class NuDynamicElementBase {
  constructor(public _changeDetectorRef: ChangeDetectorRef) {
  }
}

/* eslint-disable-next-line */
export const _NuDynamicElementMixinBase = mixinControlValueAccessor(NuDynamicElementBase);

/* eslint-disable-next-line */
@Directive({selector: '[tdDynamicFormsError]ng-template'})
export class NuDynamicFormsErrorTemplateDirective extends TemplatePortalDirective {
  @Input() tdDynamicFormsError: string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public templateRef: TemplateRef<any>, viewContainerRef: ViewContainerRef) {
    super(templateRef, viewContainerRef);
  }
}

/* eslint-disable-next-line */
@Directive({selector: '[tdDynamicSelectOption]ng-template'})
export class NuDynamicSelectionOptionTemplateDirective extends TemplatePortalDirective {
  @Input() tdDynamicSelectOption: string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public templateRef: TemplateRef<any>, viewContainerRef: ViewContainerRef) {
    super(templateRef, viewContainerRef);
  }
}

@Directive({
  /* eslint-disable-next-line */
  selector: '[tdDynamicContainer]',
})
export class NuDynamicElementDirective {
  constructor(public viewContainer: ViewContainerRef) {
  }
}

@Component({
  providers: [
    NuDynamicFormsService,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => NuDynamicElementComponent),
      multi: true,
    },
  ],
  selector: 'nuarch-dynamic-element',
  template: '<div tdDynamicContainer></div>',
})
export class NuDynamicElementComponent extends _NuDynamicElementMixinBase
  implements IControlValueAccessor, OnInit, OnChanges {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private _instance: any;
  /**
   * Sets form control of the element.
   */
  @Input() dynamicControl: FormControl;
  /**
   * Sets label to be displayed.
   */
  @Input() label: string = '';
  /**
   * Sets hint to be displayed.
   */
  @Input() hint: string = '';
  /**
   * Sets name to be displayed as attribute.
   */
  @Input() name: string = '';
  /**
   * Sets name to be displayed as attribute.
   */
  @Input() value: string = '';
  /**
   * Sets name to be displayed as attribute.
   */
  @Input() filterType: NuFilterType;
  /**
   * Sets name to be displayed as attribute.
   */
  @Input() readonly: boolean = undefined;
  /**
   * Sets name to be displayed as attribute.
   */
  @Input() appearance: string = '';
  /**
   * Sets name to be displayed as attribute.
   */
  @Input() displayWith: Function;
  /**
   * Sets min validation checkup (if supported by element).
   */
  @Input() dateFormat: string = undefined;
  /**
   * Sets type or element of element to be rendered.
   * Throws error if does not exist or no supported.
   */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @Input() type: NuDynamicElement | NuDynamicType | Type<any> = undefined;
  /**
   * Sets required validation checkup (if supported by element).
   */
  @Input() required: boolean = undefined;
  /**
   * Sets required validation checkup (if supported by element).
   */
  @Input() showClear: boolean = undefined;
  /**
   * Sets required validation checkup (if supported by element).
   */
  @Input() filter: boolean = undefined;
  /**
   * Sets min validation checkup (if supported by element).
   */
  @Input() min: number = undefined;
  /**
   * Sets max validation checkup (if supported by element).
   */
  @Input() max: number = undefined;
  /**
   * Sets minLength validation checkup (if supported by element).
   */
  @Input() minLength: number = undefined;
  /**
   * Sets maxLength validation checkup (if supported by element).
   */
  @Input() maxLength: number = undefined;
  /**
   * Sets selections for array elements (if supported by element).
   */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @Input() selections: Observable<any> | any[] = undefined;
  /**
   * Sets loading for async requests (if supported by element).
   */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @Input() loading: Observable<boolean> = undefined;
  /**
   * Sets multiple property for array elements (if supported by element).
   */
  @Input() multiple: boolean = undefined;
  /**
   * Sets hidden property (if supported by element).
   */
  @Input() hidden: boolean = undefined;
  /**
   * Sets error message template so it can be injected into dynamic components.
   */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @Input() errorMessageTemplate: TemplateRef<any> = undefined;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @Input() optionTemplate: TemplateRef<any> = undefined;

  @ViewChild(NuDynamicElementDirective, {static: true}) childElement: NuDynamicElementDirective;

  constructor(private _componentFactoryResolver: ComponentFactoryResolver,
              private _dynamicFormsService: NuDynamicFormsService,
              _changeDetectorRef: ChangeDetectorRef) {
    super(_changeDetectorRef);
  }

  @HostBinding('attr.max')
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  get maxAttr(): any {
    return this.max;
  }

  @HostBinding('attr.min')
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  get minAttr(): any {
    return this.min;
  }

  ngOnInit(): void {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const component: any =
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      <any>this.type instanceof Type ? this.type : this._dynamicFormsService.getDynamicElement(this.type, this.readonly);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const ref: ComponentRef<any> = this._componentFactoryResolver
      .resolveComponentFactory(component)
      .create(this.childElement.viewContainer.injector);
    this.childElement.viewContainer.insert(ref.hostView);
    this._instance = ref.instance;
    this._instance.control = this.dynamicControl;
    this._instance.label = this.label;
    this._instance.hint = this.hint;
    this._instance.name = this.name;
    this._instance.type = this.type;
    this._instance.value = this.value;
    this._instance.filter = this.filter;
    this._instance.filterType = this.filterType;
    this._instance.readonly = this.readonly;
    this._instance.required = this.required;
    this._instance.showClear = this.showClear;
    this._instance.dateFormat = this.dateFormat;
    this._instance.appearance = this.appearance;
    this._instance.min = this.min;
    this._instance.max = this.max;
    this._instance.minLength = this.minLength;
    this._instance.maxLength = this.maxLength;
    this._instance.selections = this.selections;
    this._instance.loading = this.loading;
    this._instance.multiple = this.multiple;
    this._instance.hidden = this.hidden;
    this._instance.errorMessageTemplate = this.errorMessageTemplate;
    this._instance.optionTemplate = this.optionTemplate;
    this._instance.displayWith = this.displayWith;
  }

  /**
   * Reassign any inputs that have changed
   */
  ngOnChanges(changes: SimpleChanges): void {
    if (this._instance) {
      for (const prop of Object.keys(changes)) {
        this._instance[prop] = changes[prop].currentValue;
      }
    }
  }
}

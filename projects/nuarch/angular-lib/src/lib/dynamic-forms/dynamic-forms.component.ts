import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren, EventEmitter,
  Input,
  OnDestroy, Output,
  QueryList,
  TemplateRef,
} from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { Observable, Subject, timer } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import {
  NuDynamicFormsErrorTemplateDirective,
  NuDynamicSelectionOptionTemplateDirective,
} from './dynamic-element.component';

import { INuDynamicElementConfig, NuDynamicFormsService, NuDynamicElement } from './services/dynamic-forms.service';
import { DomSanitizer } from '@angular/platform-browser';
import { get, keys, each, isNil, some, reduce, includes } from 'lodash';

@Component({
  selector: 'nuarch-dynamic-forms',
  templateUrl: './dynamic-forms.component.html',
  styleUrls: ['./dynamic-forms.component.scss'],
})
export class NuDynamicFormsComponent implements AfterContentInit, OnDestroy {

  private _renderedElements: INuDynamicElementConfig[] = [];
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private _errorTemplateMap: Map<string, TemplateRef<any>> = new Map<string, TemplateRef<any>>();
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private _optionTemplateMap: Map<string, TemplateRef<any>> = new Map<string, TemplateRef<any>>();
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private _destroy$: Subject<any> = new Subject();
  private _destroyControl$: Subject<string> = new Subject();
  private _elements: INuDynamicElementConfig[];
  private _formControls: FormGroup;

  readonly typeWithoutNameAsLabel: string[] = [NuDynamicElement.Radio];

  keys: Function = keys;

  @Output() init: EventEmitter<void> = new EventEmitter<void>();
  @ContentChildren(NuDynamicFormsErrorTemplateDirective, {descendants: true}) _errorTemplates: QueryList<NuDynamicFormsErrorTemplateDirective>;
  @ContentChildren(NuDynamicSelectionOptionTemplateDirective, {descendants: true}) _optionTemplates:
    QueryList<NuDynamicSelectionOptionTemplateDirective>;
  dynamicForm: FormGroup;

  constructor(private _formBuilder: FormBuilder,
              private _dynamicFormsService: NuDynamicFormsService,
              private _changeDetectorRef: ChangeDetectorRef,
              public sanitizer: DomSanitizer) {
    this.dynamicForm = this._formBuilder.group({});
  }

  getLabel(label: string, name: string, type: string): string {
    if (includes(this.typeWithoutNameAsLabel, type)) {
      return label;
    }
    return label || name;
  }

  get elementsByRow(): INuDynamicElementConfig[] {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return reduce(this._renderedElements, (acc: any, rec: INuDynamicElementConfig) => {
      if (!acc[get(rec, 'row', 0)]) {
        acc[get(rec, 'row', 0)] = [];
      }

      acc[get(rec, 'row', 0)].push(rec);
      return acc;
    }, {});
  }

  get elements(): INuDynamicElementConfig[] {
    return this._renderedElements;
  }

  @Input('formControls')
  set formControls(fg: FormGroup) {
    if (!!fg) {
      this._formControls = fg;
      this._createElements();
    }
  }
  /**
   * elements: INuDynamicElementConfig[]
   * JS Object that will render the elements depending on its config.
   * [name] property is required.
   */
  @Input('elements')
  set elements(elements: INuDynamicElementConfig[]) {
    if (elements) {
      this._elements = elements;
    } else {
      this._elements = [];
    }
    this._createElements();
  }

  _createElements(): void {
    if (!!this._elements && !!this._formControls) {
      this._renderedElements = this._elements.slice();
      this._initialize();
    } else if (!!this._elements && !this._formControls) {
      this._rerenderElements();
    }
  }

  get dynamicFormGroup(): FormGroup {
    return this._formControls || this.dynamicForm;
  }

  /**
   * Getter property for dynamic [FormGroup].
   */
  get form(): FormGroup {
    return this.dynamicForm;
  }

  /**
   * Getter property for [valid] of dynamic [FormGroup].
   */
  get valid(): boolean {
    if (this.dynamicForm) {
      return this.dynamicForm.valid;
    }
    return false;
  }

  /**
   * Getter property for [value] of dynamic [FormGroup].
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  get value(): any {
    if (this.dynamicForm) {
      return this.dynamicForm.value;
    }
    return {};
  }

  /**
   * Getter property for [errors] of dynamic [FormGroup].
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  get errors(): { [name: string]: any } {
    if (this.dynamicForm) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const errors: { [name: string]: any } = {};
      for (const name of Object.keys(this.dynamicForm.controls)) {
        errors[name] = this.dynamicForm.controls[name].errors;
      }
      return errors;
    }
    return {};
  }

  /**
   * Getter property for [controls] of dynamic [FormGroup].
   */
  get controls(): { [key: string]: AbstractControl } {
    if (this.dynamicForm) {
      return this.dynamicForm.controls;
    }
    return {};
  }

  ngAfterContentInit(): void {
    this._updateErrorTemplates();
    this._updateOptionTemplates();
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
    this._destroyControl$.complete();
  }

  /**
   * Refreshes the form and rerenders all validator/element modifications.
   */
  refresh(): void {
    this._rerenderElements();
    this._updateErrorTemplates();
    this._updateOptionTemplates();
  }

  /**
   * Getter method for error template references
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getErrorTemplateRef(name: string): TemplateRef<any> {
    return this._errorTemplateMap.get(name);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getOptionTemplateRef(name: string): TemplateRef<any> {
    return this._optionTemplateMap.get(name);
  }

  /**
   * Validate all from controls
   */
  updateValueAndValidity(): void {
    each(keys(this.controls), (key: string) => {
      const control: AbstractControl = this.controls[key];
      control.updateValueAndValidity();
    });
  }

  /**
   * Loads error templates and sets them in a map for faster access.
   */
  private _updateErrorTemplates(): void {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    this._errorTemplateMap = new Map<string, TemplateRef<any>>();
    for (const errorTemplate of this._errorTemplates.toArray()) {
      this._errorTemplateMap.set(errorTemplate.tdDynamicFormsError, errorTemplate.templateRef);
    }
  }

  private _updateOptionTemplates(): void {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    this._optionTemplateMap = new Map<string, TemplateRef<any>>();
    for (const optionTemplate of this._optionTemplates.toArray()) {
      this._optionTemplateMap.set(optionTemplate.tdDynamicSelectOption, optionTemplate.templateRef);
    }
  }

  private _rerenderElements(): void {
    this._clearRemovedElements();
    this._renderedElements = [];
    const duplicates: string[] = [];
    this._elements.forEach((elem: INuDynamicElementConfig) => {
      this._dynamicFormsService.validateDynamicElementName(elem.name);
      if (duplicates.indexOf(elem.name) > -1) {
        throw new Error(`Dynamic element name: "${elem.name}" is duplicated`);
      }
      duplicates.push(elem.name);
      const dynamicElement: AbstractControl = this.dynamicForm.get(elem.name);
      if (!dynamicElement) {
        this.dynamicForm.addControl(elem.name, this._dynamicFormsService.createFormControl(elem));
        this._subscribeToControlStatusChanges(elem.name);
      } else {
        dynamicElement.setValue(elem.default);
        dynamicElement.markAsPristine();
        dynamicElement.markAsUntouched();
        if (elem.disabled) {
          dynamicElement.disable();
        } else {
          dynamicElement.enable();
        }
        dynamicElement.setValidators(this._dynamicFormsService.createValidators(elem));
      }
      // copy objects so they are only changes when calling this method
      this._renderedElements.push(Object.assign({}, elem));
    });
    this._initialize();
  }

  private _initialize(): void {
    // call a change detection since the whole form might change
    this._changeDetectorRef.detectChanges();
    timer()
      .toPromise()
      .then(() => {
        // call a markForCheck so elements are rendered correctly in OnPush
        this._changeDetectorRef.markForCheck();
        this.init.emit();
      });
  }

  private _clearRemovedElements(): void {
    this._renderedElements = this._renderedElements.filter(
      (renderedElement: INuDynamicElementConfig) =>
        !this._elements.some((element: INuDynamicElementConfig) => element.name === renderedElement.name),
    );
    // remove elements that were removed from the array
    this._renderedElements.forEach((elem: INuDynamicElementConfig) => {
      this._destroyControl$.next(elem.name);
      this.dynamicForm.removeControl(elem.name);
    });
  }

  // Updates component when manually adding errors to controls
  private _subscribeToControlStatusChanges(elementName: string): void {
    const control: AbstractControl = this.controls[elementName];

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const controlDestroyed$: Observable<any> = this._destroyControl$.pipe(
      filter((destroyedElementName: string) => destroyedElementName === elementName),
    );

    control.statusChanges.pipe(takeUntil(this._destroy$), takeUntil(controlDestroyed$)).subscribe(() => {
      this._changeDetectorRef.markForCheck();
    });
  }
}

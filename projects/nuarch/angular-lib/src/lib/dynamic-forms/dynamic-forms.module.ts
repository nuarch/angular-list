import { CommonModule } from '@angular/common';
import { NgModule, Type } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { FileModule } from '../file/file.module';
import { NuDynamicElementComponent, NuDynamicElementDirective, NuDynamicFormsErrorTemplateDirective } from './dynamic-element.component';
import { NuDynamicAutocompleteComponent } from './dynamic-elements/dynamic-autocomplete/dynamic-autocomplete.component';
import { NuDynamicCheckboxComponent } from './dynamic-elements/dynamic-checkbox/dynamic-checkbox.component';
import { NuDynamicDatepickerComponent } from './dynamic-elements/dynamic-datepicker/dynamic-datepicker.component';
import { NuDynamicFileInputComponent } from './dynamic-elements/dynamic-file-input/dynamic-file-input.component';

import { NuDynamicInputComponent } from './dynamic-elements/dynamic-input/dynamic-input.component';
import { NuDynamicSelectComponent } from './dynamic-elements/dynamic-select/dynamic-select.component';
import { NuDynamicSlideToggleComponent } from './dynamic-elements/dynamic-slide-toggle/dynamic-slide-toggle.component';
import { NuDynamicSliderComponent } from './dynamic-elements/dynamic-slider/dynamic-slider.component';
import { NuDynamicTextareaComponent } from './dynamic-elements/dynamic-textarea/dynamic-textarea.component';

import { NuDynamicFormsComponent } from './dynamic-forms.component';
import { NuDynamicRadioButtonComponent } from './dynamic-elements/dynamic-radio-button/dynamic-radio-button.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { DateAdapter, MAT_DATE_LOCALE, MatNativeDateModule } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MatMomentDateModule, MomentDateAdapter } from '@angular/material-moment-adapter';
import { NuDynamicSelectionOptionTemplateDirective } from './dynamic-element.component';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const TD_DYNAMIC_FORMS: Type<any>[] = [
  NuDynamicFormsComponent,
  NuDynamicElementComponent,
  NuDynamicElementDirective,
  NuDynamicFormsErrorTemplateDirective,
  NuDynamicSelectionOptionTemplateDirective,
];

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const TD_DYNAMIC_FORMS_ENTRY_COMPONENTS: Type<any>[] = [
  NuDynamicInputComponent,
  NuDynamicFileInputComponent,
  NuDynamicTextareaComponent,
  NuDynamicSlideToggleComponent,
  NuDynamicCheckboxComponent,
  NuDynamicSliderComponent,
  NuDynamicSelectComponent,
  NuDynamicDatepickerComponent,
  NuDynamicAutocompleteComponent,
  NuDynamicRadioButtonComponent,
];

@NgModule({
    declarations: [TD_DYNAMIC_FORMS, TD_DYNAMIC_FORMS_ENTRY_COMPONENTS],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatCheckboxModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatIconModule,
        MatButtonModule,
        MatDatepickerModule,
        MatMomentDateModule,
        MatAutocompleteModule,
        FileModule,
        MatRadioModule,
        MatProgressSpinnerModule,
    ],
    providers: [
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },
    ],
    exports: [TD_DYNAMIC_FORMS, TD_DYNAMIC_FORMS_ENTRY_COMPONENTS]
})
export class DynamicFormsModule {
}

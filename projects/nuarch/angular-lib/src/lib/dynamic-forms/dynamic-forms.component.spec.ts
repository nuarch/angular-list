import { Component, DebugElement, NgModule } from '@angular/core';
import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { AbstractControl, FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NuDynamicElementComponent } from './dynamic-element.component';
import { NuDynamicFormsComponent } from './dynamic-forms.component';
import { DynamicFormsModule } from './dynamic-forms.module';
import { INuDynamicElementConfig, NuDynamicElement, NuDynamicType } from './services/dynamic-forms.service';

@Component({
  template: `
    <nuarch-dynamic-forms [elements]="elements">
      <ng-template let-control="control" tdDynamicFormsError="customElement">
        <span id="custom-error" *ngIf="control.hasError('customError')">{{ control.errors['customError'] }}</span>
      </ng-template>
    </nuarch-dynamic-forms>
  `,
})
class NuDynamicFormsTestComponent {
  elements: INuDynamicElementConfig[];
}

@Component({
  selector: 'nuarch-dynamic-input-test',
  template: `
    <input [formControl]="control"/>
  `,
})
export class NuDynamicTestComponent {
  control: FormControl;
}

@NgModule({
    declarations: [NuDynamicTestComponent],
    imports: [ReactiveFormsModule],
    exports: [NuDynamicTestComponent]
})
export class NuDynamicTestModule {
}

describe('Component: NuDynamicForms', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        MatNativeDateModule,
        DynamicFormsModule,
        NuDynamicTestModule,
      ],
      declarations: [NuDynamicFormsTestComponent],
    });
    TestBed.compileComponents();
  }));

  it('should create the component', async(
    inject([], () => {
      const fixture: ComponentFixture<NuDynamicFormsTestComponent> = TestBed.createComponent(NuDynamicFormsTestComponent);
      const component: NuDynamicFormsTestComponent = fixture.debugElement.componentInstance;

      fixture.detectChanges();
      expect(component).toBeTruthy();
    }),
  ));

  it('should render dynamic elements', async(
    inject([], () => {
      const fixture: ComponentFixture<NuDynamicFormsTestComponent> = TestBed.createComponent(NuDynamicFormsTestComponent);
      const component: NuDynamicFormsTestComponent = fixture.debugElement.componentInstance;

      expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(0);
      component.elements = [
        {
          name: 'first_name',
          type: NuDynamicType.Text,
        },
        {
          name: 'password',
          type: NuDynamicElement.Password,
        },
        {
          name: 'on_it',
          type: NuDynamicType.Boolean,
        },
        {
          name: 'age',
          type: NuDynamicType.Number,
        },
        {
          name: 'nodes',
          type: NuDynamicElement.Slider,
        },
        {
          name: 'description',
          type: NuDynamicElement.Textarea,
        },
        {
          name: 'sex',
          type: NuDynamicType.Array,
          selections: ['M', 'F'],
        },
        {
          name: 'dob',
          type: NuDynamicElement.Datepicker,
        },
      ] as INuDynamicElementConfig[];
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(8);
      });
    }),
  ));

  it('should render dynamic elements and show form invalid because an input is required', async(
    inject([], () => {
      const fixture: ComponentFixture<NuDynamicFormsTestComponent> = TestBed.createComponent(NuDynamicFormsTestComponent);
      const component: NuDynamicFormsTestComponent = fixture.debugElement.componentInstance;

      expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(0);
      component.elements = [
        {
          name: 'first_name',
          type: NuDynamicType.Text,
          required: true,
        },
        {
          name: 'on_it',
          type: NuDynamicType.Boolean,
          default: true,
        },
      ] as INuDynamicElementConfig[];
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(2);
        const dynamicFormsComponent: NuDynamicFormsComponent = fixture.debugElement.query(
          By.directive(NuDynamicFormsComponent),
        ).componentInstance;
        expect(dynamicFormsComponent.valid).toBeFalsy();
        /* eslint-disable-next-line */
        expect(JSON.stringify(dynamicFormsComponent.value)).toBe(JSON.stringify({on_it: true}));
      });
    }),
  ));

  it('should render dynamic elements and show form invalid because a number is less than min', async(
    inject([], () => {
      const fixture: ComponentFixture<NuDynamicFormsTestComponent> = TestBed.createComponent(NuDynamicFormsTestComponent);
      const component: NuDynamicFormsTestComponent = fixture.debugElement.componentInstance;

      expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(0);
      component.elements = [
        {
          name: 'first_name',
          type: NuDynamicType.Text,
          required: true,
        },
        {
          name: 'age',
          type: NuDynamicType.Number,
          min: 18,
          default: 17,
        },
      ] as INuDynamicElementConfig[];
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(2);
        const dynamicFormsComponent: NuDynamicFormsComponent = fixture.debugElement.query(
          By.directive(NuDynamicFormsComponent),
        ).componentInstance;
        expect(dynamicFormsComponent.valid).toBeFalsy();
        /* eslint-disable-next-line */
        expect(JSON.stringify(dynamicFormsComponent.value)).toBe(JSON.stringify({age: 17}));
      });
    }),
  ));

  it('should render dynamic elements and show form valid', async(
    inject([], () => {
      const fixture: ComponentFixture<NuDynamicFormsTestComponent> = TestBed.createComponent(NuDynamicFormsTestComponent);
      const component: NuDynamicFormsTestComponent = fixture.debugElement.componentInstance;

      expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(0);
      const dob: Date = new Date();
      component.elements = [
        {
          name: 'first_name',
          type: NuDynamicType.Text,
          required: true,
          default: 'name',
        },
        {
          name: 'age',
          type: NuDynamicType.Number,
          default: 20,
          min: 18,
          max: 30,
        },
        {
          name: 'dob',
          type: NuDynamicType.Date,
          default: dob,
          required: true,
        },
      ] as INuDynamicElementConfig[];
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(3);
        const dynamicFormsComponent: NuDynamicFormsComponent = fixture.debugElement.query(
          By.directive(NuDynamicFormsComponent),
        ).componentInstance;
        expect(dynamicFormsComponent.valid).toBeTruthy();
        expect(JSON.stringify(dynamicFormsComponent.value)).toBe(JSON.stringify({first_name: 'name', age: 20, dob}));
      });
    }),
  ));

  it('should render dynamic elements and show form invalid because character length is less than minLength', async(
    inject([], () => {
      const fixture: ComponentFixture<NuDynamicFormsTestComponent> = TestBed.createComponent(NuDynamicFormsTestComponent);
      const component: NuDynamicFormsTestComponent = fixture.debugElement.componentInstance;

      expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(0);
      component.elements = [
        {
          name: 'password',
          type: NuDynamicType.Text,
          minLength: 8,
          default: 'mypwd',
        },
      ] as INuDynamicElementConfig[];
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(1);
        const dynamicFormsComponent: NuDynamicFormsComponent = fixture.debugElement.query(
          By.directive(NuDynamicFormsComponent),
        ).componentInstance;
        expect(dynamicFormsComponent.valid).toBeFalsy();
        // eslint-disable-next-line 
        expect(JSON.stringify(dynamicFormsComponent.value)).toBe(JSON.stringify({password: 'mypwd'}));
      });
    }),
  ));

  it('should render dynamic elements and show form invalid because character length is more than maxLength', async(
    inject([], () => {
      const fixture: ComponentFixture<NuDynamicFormsTestComponent> = TestBed.createComponent(NuDynamicFormsTestComponent);
      const component: NuDynamicFormsTestComponent = fixture.debugElement.componentInstance;

      expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(0);
      component.elements = [
        {
          name: 'password',
          type: NuDynamicType.Text,
          maxLength: 8,
          default: 'myVeryLongString',
        },
      ] as INuDynamicElementConfig[];
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(1);
        const dynamicFormsComponent: NuDynamicFormsComponent = fixture.debugElement.query(
          By.directive(NuDynamicFormsComponent),
        ).componentInstance;
        expect(dynamicFormsComponent.valid).toBeFalsy();
        // eslint-disable-next-line 
        expect(JSON.stringify(dynamicFormsComponent.value)).toBe(JSON.stringify({password: 'myVeryLongString'}));
      });
    }),
  ));

  it('should render dynamic elements and show form valid', async(
    inject([], () => {
      const fixture: ComponentFixture<NuDynamicFormsTestComponent> = TestBed.createComponent(NuDynamicFormsTestComponent);
      const component: NuDynamicFormsTestComponent = fixture.debugElement.componentInstance;

      expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(0);
      component.elements = [
        {
          name: 'password',
          type: NuDynamicType.Text,
          minLength: 8,
          maxLength: 20,
          default: 'mySuperSecretPw',
        },
      ] as INuDynamicElementConfig[];
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(1);
        const dynamicFormsComponent: NuDynamicFormsComponent = fixture.debugElement.query(
          By.directive(NuDynamicFormsComponent),
        ).componentInstance;
        expect(dynamicFormsComponent.valid).toBeTruthy();
        // eslint-disable-next-line
        expect(JSON.stringify(dynamicFormsComponent.value)).toBe(JSON.stringify({password: 'mySuperSecretPw'}));
      });
    }),
  ));

  it('should render dynamic elements and show form invalid with custom validation', async(
    inject([], () => {
      const fixture: ComponentFixture<NuDynamicFormsTestComponent> = TestBed.createComponent(NuDynamicFormsTestComponent);
      const component: NuDynamicFormsTestComponent = fixture.debugElement.componentInstance;

      expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(0);
      component.elements = [
        {
          name: 'number',
          type: NuDynamicType.Number,
          default: 15,
          appearance: '',
          validators: [
            {
              validator: (control: AbstractControl) => {
                const isValid: boolean = control.value > 21 && control.value < 23;
                return !isValid ? {length: true} : undefined;
              },
            },
          ],
        },
      ] as INuDynamicElementConfig[];
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(1);
        const dynamicFormsComponent: NuDynamicFormsComponent = fixture.debugElement.query(
          By.directive(NuDynamicFormsComponent),
        ).componentInstance;
        expect(dynamicFormsComponent.valid).toBeFalsy();
        expect(JSON.stringify(dynamicFormsComponent.value)).toBe(JSON.stringify({number: 15}));
      });
    }),
  ));

  it('should render dynamic elements and show form invalid with Angular validation', async(
    inject([], () => {
      const fixture: ComponentFixture<NuDynamicFormsTestComponent> = TestBed.createComponent(NuDynamicFormsTestComponent);
      const component: NuDynamicFormsTestComponent = fixture.debugElement.componentInstance;

      expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(0);
      component.elements = [
        {
          name: 'hexColor',
          type: NuDynamicType.Text,
          required: true,
          default: '#ZZZZZZ',
          validators: [
            {
              validator: Validators.pattern(/^#[A-Fa-f0-9]{6}$/),
            },
          ],
        },
      ] as INuDynamicElementConfig[];
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(1);
        const dynamicFormsComponent: NuDynamicFormsComponent = fixture.debugElement.query(
          By.directive(NuDynamicFormsComponent),
        ).componentInstance;
        expect(dynamicFormsComponent.valid).toBeFalsy();
        expect(JSON.stringify(dynamicFormsComponent.value)).toBe(JSON.stringify({hexColor: '#ZZZZZZ'}));
      });
    }),
  ));

  it('should render dynamic elements and show form valid with custom validations', async(
    inject([], () => {
      const fixture: ComponentFixture<NuDynamicFormsTestComponent> = TestBed.createComponent(NuDynamicFormsTestComponent);
      const component: NuDynamicFormsTestComponent = fixture.debugElement.componentInstance;

      expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(0);
      component.elements = [
        {
          name: 'hexColor',
          type: NuDynamicType.Text,
          required: true,
          default: '#F1F1F1',
          validators: [
            {
              validator: Validators.pattern(/^#[A-Fa-f0-9]{6}$/),
            },
          ],
        },
        {
          name: 'number',
          type: NuDynamicType.Number,
          default: 22,
          validators: [
            {
              validator: (control: AbstractControl) => {
                const isValid: boolean = control.value > 21 && control.value < 23;
                return !isValid ? {length: true} : undefined;
              },
            },
          ],
        },
      ] as INuDynamicElementConfig[];
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(2);
        const dynamicFormsComponent: NuDynamicFormsComponent = fixture.debugElement.query(
          By.directive(NuDynamicFormsComponent),
        ).componentInstance;
        expect(dynamicFormsComponent.valid).toBeTruthy();
        expect(JSON.stringify(dynamicFormsComponent.value)).toBe(JSON.stringify({hexColor: '#F1F1F1', number: 22}));
      });
    }),
  ));

  it('should render errors with manual validations', async(
    inject([], () => {
      const fixture: ComponentFixture<NuDynamicFormsTestComponent> = TestBed.createComponent(NuDynamicFormsTestComponent);
      const component: NuDynamicFormsTestComponent = fixture.debugElement.componentInstance;

      component.elements = [
        {
          name: 'customElement',
          label: 'Custom Element',
          type: NuDynamicType.Text,
        },
      ] as INuDynamicElementConfig[];

      fixture.detectChanges();
      fixture.whenStable().then(() => {
        const dynamicFormsComponent: NuDynamicFormsComponent = fixture.debugElement.query(
          By.directive(NuDynamicFormsComponent),
        ).componentInstance;

        fixture.detectChanges();
        fixture.whenStable().then(() => {
          const key: string = 'customElement';
          dynamicFormsComponent.controls[key].markAsTouched();
          dynamicFormsComponent.controls[key].setErrors({customError: 'CUSTOM_ERROR'});

          fixture.detectChanges();
          fixture.whenStable().then(() => {
            expect(fixture.debugElement.queryAll(By.css('#custom-error')).length).toBe(1);
            expect(fixture.debugElement.query(By.css('#custom-error')).nativeElement.textContent).toBe('CUSTOM_ERROR');
          });
        });
      });
    }),
  ));

  it('should render dynamic elements with one element disabled', async(
    inject([], () => {
      const fixture: ComponentFixture<NuDynamicFormsTestComponent> = TestBed.createComponent(NuDynamicFormsTestComponent);
      const component: NuDynamicFormsTestComponent = fixture.debugElement.componentInstance;

      expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(0);
      component.elements = [
        {
          name: 'hexColor',
          type: NuDynamicType.Text,
          required: true,
          default: '#F1F1F1',
        },
        {
          name: 'number',
          type: NuDynamicType.Number,
          disabled: true,
          required: true,
        },
      ] as INuDynamicElementConfig[];
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(2);
        const dynamicFormsComponent: NuDynamicFormsComponent = fixture.debugElement.query(
          By.directive(NuDynamicFormsComponent),
        ).componentInstance;
        expect(dynamicFormsComponent.valid).toBeTruthy();
        /* eslint-disable-next-line */
        expect(JSON.stringify(dynamicFormsComponent.value)).toBe(JSON.stringify({hexColor: '#F1F1F1'}));
      });
    }),
  ));

  it('should render disabled file input', async(
    inject([], () => {
      const fixture: ComponentFixture<NuDynamicFormsTestComponent> = TestBed.createComponent(NuDynamicFormsTestComponent);
      const component: NuDynamicFormsTestComponent = fixture.debugElement.componentInstance;

      expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(0);
      component.elements = [
        {
          name: 'file',
          type: NuDynamicElement.FileInput,
          disabled: true,
        },
      ] as INuDynamicElementConfig[];
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        const button: DebugElement = fixture.debugElement.query(By.css('nuarch-file-input button'));
        const hiddenFileInput: DebugElement = fixture.debugElement.query(By.css('nuarch-file-input .nuarch-file-input-hidden'));

        expect(button.attributes.disabled).not.toBeNull();
        expect(hiddenFileInput.attributes.disabled).not.toBeNull();
      });
    }),
  ));

  it('should render dynamic custom element', async(
    inject([], () => {
      const fixture: ComponentFixture<NuDynamicFormsTestComponent> = TestBed.createComponent(NuDynamicFormsTestComponent);
      const component: NuDynamicFormsTestComponent = fixture.debugElement.componentInstance;

      expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(0);
      component.elements = [
        {
          name: 'custom',
          type: NuDynamicTestComponent,
          default: 'value',
        },
      ] as INuDynamicElementConfig[];
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.queryAll(By.directive(NuDynamicElementComponent)).length).toBe(1);
        const dynamicFormsComponent: NuDynamicFormsComponent = fixture.debugElement.query(
          By.directive(NuDynamicFormsComponent),
        ).componentInstance;
        expect(dynamicFormsComponent.valid).toBeTruthy();
        expect(JSON.stringify(dynamicFormsComponent.value)).toBe(JSON.stringify({custom: 'value'}));
      });
    }),
  ));
});

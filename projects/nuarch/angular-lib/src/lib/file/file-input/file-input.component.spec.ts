import { Component } from '@angular/core';
import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { FileModule } from '../file.module';
import { NuFileInputComponent } from './file-input.component';

@Component({
  selector: 'nuarch-file-input-basic-test',
  template: `
    <nuarch-file-input [multiple]="multiple" [disabled]="disabled" (select)="files = $event">
      <span>test</span>
    </nuarch-file-input>
  `,
})
class NuFileInputBasicTestComponent {
  accept: string;
  multiple: boolean;
  disabled: boolean;
  files: File | FileList;
}

@Component({
  selector: 'nuarch-file-input-model-test',
  template: `
    <nuarch-file-input [(ngModel)]="files" [multiple]="multiple" [disabled]="disabled">
      <span>test</span>
    </nuarch-file-input>
  `,
})
class NuFileInputModelTestComponent {
  multiple: boolean;
  disabled: boolean;
  files: File | FileList;
}

describe('Component: FileInput', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NuFileInputBasicTestComponent, NuFileInputModelTestComponent],
      imports: [FormsModule, FileModule],
    });
    TestBed.compileComponents();
  }));

  it('should render content inside .nuarch-file-input button', async(
    inject([], () => {
      const fixture: ComponentFixture<NuFileInputBasicTestComponent> = TestBed.createComponent(NuFileInputBasicTestComponent);
      const component: NuFileInputBasicTestComponent = fixture.debugElement.componentInstance;
      component.multiple = false;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.query(By.css('.nuarch-file-input span'))).toBeTruthy();
      });
    }),
  ));

  it('should mimic file selection and then clear it', async(
    inject([], () => {
      const fixture: ComponentFixture<NuFileInputBasicTestComponent> = TestBed.createComponent(NuFileInputBasicTestComponent);
      const component: NuFileInputBasicTestComponent = fixture.debugElement.componentInstance;
      component.multiple = false;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        fixture.debugElement.query(By.directive(NuFileInputComponent)).componentInstance.handleSelect([{}]);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          fixture.debugElement.query(By.directive(NuFileInputComponent)).componentInstance.clear();
          fixture.detectChanges();
          fixture.whenStable().then(() => {
            expect(
              fixture.debugElement.query(By.directive(NuFileInputComponent)).componentInstance.value,
            ).toBeUndefined();
          });
        });
      });
    }),
  ));

  it('should mimic file selection and then clear it by disabling it', async(
    inject([], () => {
      const fixture: ComponentFixture<NuFileInputBasicTestComponent> = TestBed.createComponent(NuFileInputBasicTestComponent);
      const component: NuFileInputBasicTestComponent = fixture.debugElement.componentInstance;
      component.multiple = false;
      component.disabled = false;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        fixture.debugElement.query(By.directive(NuFileInputComponent)).componentInstance.handleSelect([{}]);
        component.disabled = true;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(
            fixture.debugElement.query(By.directive(NuFileInputComponent)).componentInstance.value,
          ).toBeUndefined();
        });
      });
    }),
  ));

  it('should mimic file (select) event', async(
    inject([], () => {
      const fixture: ComponentFixture<NuFileInputBasicTestComponent> = TestBed.createComponent(NuFileInputBasicTestComponent);
      const component: NuFileInputBasicTestComponent = fixture.debugElement.componentInstance;
      component.multiple = false;
      fixture.detectChanges();
      expect(component.files).toBeUndefined();
      fixture.whenStable().then(() => {
        fixture.debugElement.query(By.directive(NuFileInputComponent)).componentInstance.handleSelect([{}]);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(component.files).toBeTruthy();
        });
      });
    }),
  ));

  it('should mimic file selection event and check model', async(
    inject([], () => {
      const fixture: ComponentFixture<NuFileInputModelTestComponent> = TestBed.createComponent(NuFileInputModelTestComponent);
      const component: NuFileInputModelTestComponent = fixture.debugElement.componentInstance;
      component.multiple = false;
      fixture.detectChanges();
      expect(component.files).toBeUndefined();
      fixture.whenStable().then(() => {
        fixture.debugElement.query(By.directive(NuFileInputComponent)).componentInstance.handleSelect([{}]);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(component.files).toBeTruthy();
        });
      });
    }),
  ));

  it('should mimic file selection event and check model and then clear it by disabling it', async(
    inject([], () => {
      const fixture: ComponentFixture<NuFileInputModelTestComponent> = TestBed.createComponent(NuFileInputModelTestComponent);
      const component: NuFileInputModelTestComponent = fixture.debugElement.componentInstance;
      component.multiple = false;
      fixture.detectChanges();
      expect(component.files).toBeUndefined();
      fixture.whenStable().then(() => {
        fixture.debugElement.query(By.directive(NuFileInputComponent)).componentInstance.handleSelect([{}]);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(component.files).toBeTruthy();
          component.disabled = true;
          fixture.componentRef.changeDetectorRef.detectChanges();
          fixture.detectChanges();
          fixture.whenStable().then(() => {
            expect(component.files).toBeUndefined();
          });
        });
      });
    }),
  ));
});

import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { FileModule } from '../file.module';
import { NuFileSelectDirective } from './file-select.directive';

@Component({
  selector: 'nuarch-file-select-basic-test',
  template: `
    <input nuarchFileSelect type="file" [multiple]="multiple" (fileSelect)="files = $event" />
  `,
})
class NuFileSelectBasicTestComponent {
  multiple: boolean;
  disabled: boolean;
  files: FileList | File;
}

describe('Directive: FileSelect', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NuFileSelectBasicTestComponent],
      imports: [FileModule],
    });
    TestBed.compileComponents();
  }));

  it('should add multiple attr', async(
    inject([], () => {
      const fixture: ComponentFixture<NuFileSelectBasicTestComponent> = TestBed.createComponent(NuFileSelectBasicTestComponent);
      const component: NuFileSelectBasicTestComponent = fixture.debugElement.componentInstance;
      component.multiple = true;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        const directive: DebugElement = fixture.debugElement.query(By.directive(NuFileSelectDirective));
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        expect((<any>directive.attributes).multiple).toBeDefined();
      });
    }),
  ));

  it('should throw (fileSelect) event for a single file', async(
    inject([], () => {
      const fixture: ComponentFixture<NuFileSelectBasicTestComponent> = TestBed.createComponent(NuFileSelectBasicTestComponent);
      const component: NuFileSelectBasicTestComponent = fixture.debugElement.componentInstance;
      component.multiple = false;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        const directive: DebugElement = fixture.debugElement.query(By.directive(NuFileSelectDirective));
        directive.triggerEventHandler('change', {
          target: directive.nativeElement,
        });
      });
    }),
  ));
});

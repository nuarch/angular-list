import { Component } from '@angular/core';
import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { FileModule } from '../file.module';
import { NuFileUploadComponent } from './file-upload.component';

@Component({
  selector: 'nuarch-file-upload-basic-test',
  template: `
    <nuarch-file-upload
      #fileUpload
      [multiple]="multiple"
      [disabled]="disabled"
      (select)="selectEvent($event)"
      (upload)="uploadEvent($event)"
      (cancel)="cancelEvent()"
    >
      <span>{{ fileUpload.value?.name }}</span>
      <ng-template nuarch-file-input-label>
        <span>Choose a file...</span>
      </ng-template>
    </nuarch-file-upload>
  `,
})
class NuFileUploadBasicTestComponent {
  accept: string;
  multiple: boolean;
  disabled: boolean;
  files: File | FileList;
  selectFiles: File | FileList;

  selectEvent(files: File | FileList): void {
    this.selectFiles = files;
  }

  uploadEvent(files: File | FileList): void {
    this.files = files;
  }

  cancelEvent(): void {
    this.selectFiles = undefined;
    this.files = undefined;
  }
}

describe('Component: FileUpload', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NuFileUploadBasicTestComponent],
      imports: [FileModule],
    });
    TestBed.compileComponents();
  }));

  it('should render content inside .nuarch-file-input button', async(
    inject([], () => {
      const fixture: ComponentFixture<NuFileUploadBasicTestComponent> = TestBed.createComponent(NuFileUploadBasicTestComponent);
      const component: NuFileUploadBasicTestComponent = fixture.debugElement.componentInstance;
      component.multiple = false;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.query(By.css('.nuarch-file-input span'))).toBeTruthy();
      });
    }),
  ));

  it('should mimic file selection and then clear it', async(
    inject([], () => {
      const fixture: ComponentFixture<NuFileUploadBasicTestComponent> = TestBed.createComponent(NuFileUploadBasicTestComponent);
      const component: NuFileUploadBasicTestComponent = fixture.debugElement.componentInstance;
      component.multiple = false;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.query(By.css('nuarch-file-input'))).toBeTruthy();
        expect(fixture.debugElement.query(By.css('.nuarch-file-upload'))).toBeFalsy();
        fixture.debugElement.query(By.directive(NuFileUploadComponent)).componentInstance.handleSelect([{}]);
        fixture.debugElement.query(By.css('nuarch-file-input')).triggerEventHandler('click', new Event('click'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(fixture.debugElement.query(By.css('nuarch-file-input'))).toBeFalsy();
          expect(fixture.debugElement.query(By.css('.nuarch-file-upload'))).toBeTruthy();
          fixture.debugElement.query(By.css('.nuarch-file-upload-cancel')).triggerEventHandler('click', new Event('click'));
          fixture.detectChanges();
          fixture.whenStable().then(() => {
            expect(fixture.debugElement.query(By.css('nuarch-file-input'))).toBeTruthy();
            expect(fixture.debugElement.query(By.css('.nuarch-file-upload'))).toBeFalsy();
            expect(
              fixture.debugElement.query(By.directive(NuFileUploadComponent)).componentInstance.value,
            ).toBeUndefined();
          });
        });
      });
    }),
  ));

  it('should mimic file selection and then clear it by disabling it', async(
    inject([], () => {
      const fixture: ComponentFixture<NuFileUploadBasicTestComponent> = TestBed.createComponent(NuFileUploadBasicTestComponent);
      const component: NuFileUploadBasicTestComponent = fixture.debugElement.componentInstance;
      component.multiple = false;
      component.disabled = false;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.query(By.css('nuarch-file-input'))).toBeTruthy();
        expect(fixture.debugElement.query(By.css('.nuarch-file-upload'))).toBeFalsy();
        fixture.debugElement.query(By.directive(NuFileUploadComponent)).componentInstance.handleSelect([{}]);
        fixture.debugElement.query(By.css('nuarch-file-input')).triggerEventHandler('click', new Event('click'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(fixture.debugElement.query(By.css('nuarch-file-input'))).toBeFalsy();
          expect(fixture.debugElement.query(By.css('.nuarch-file-upload'))).toBeTruthy();
          component.disabled = true;
          fixture.detectChanges();
          fixture.whenStable().then(() => {
            expect(fixture.debugElement.query(By.css('nuarch-file-input'))).toBeTruthy();
            expect(fixture.debugElement.query(By.css('.nuarch-file-upload'))).toBeFalsy();
            expect(
              fixture.debugElement.query(By.directive(NuFileUploadComponent)).componentInstance.value,
            ).toBeUndefined();
          });
        });
      });
    }),
  ));

  it('should mimic file selection and then upload it', async(
    inject([], () => {
      const fixture: ComponentFixture<NuFileUploadBasicTestComponent> = TestBed.createComponent(NuFileUploadBasicTestComponent);
      const component: NuFileUploadBasicTestComponent = fixture.debugElement.componentInstance;
      component.multiple = false;
      component.disabled = false;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.query(By.css('nuarch-file-input'))).toBeTruthy();
        expect(fixture.debugElement.query(By.css('.nuarch-file-upload'))).toBeFalsy();
        fixture.debugElement.query(By.directive(NuFileUploadComponent)).componentInstance.handleSelect([{}]);
        fixture.debugElement.query(By.css('nuarch-file-input')).triggerEventHandler('click', new Event('click'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(fixture.debugElement.query(By.css('nuarch-file-input'))).toBeFalsy();
          expect(fixture.debugElement.query(By.css('.nuarch-file-upload'))).toBeTruthy();
          fixture.debugElement.query(By.css('.nuarch-file-upload')).triggerEventHandler('click', new Event('click'));
          fixture.detectChanges();
          fixture.whenStable().then(() => {
            expect(component.files).toBeTruthy();
          });
        });
      });
    }),
  ));

  it('should mimic file selection and throw (select) event', async(
    inject([], () => {
      const fixture: ComponentFixture<NuFileUploadBasicTestComponent> = TestBed.createComponent(NuFileUploadBasicTestComponent);
      const component: NuFileUploadBasicTestComponent = fixture.debugElement.componentInstance;

      const eventSpy: jasmine.Spy = spyOn(component, 'selectEvent');

      component.multiple = false;
      component.disabled = false;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.query(By.css('nuarch-file-input'))).toBeTruthy();
        expect(fixture.debugElement.query(By.css('.nuarch-file-upload'))).toBeFalsy();
        expect(eventSpy.calls.count()).toBe(0);
        fixture.debugElement.query(By.directive(NuFileUploadComponent)).componentInstance.handleSelect([{}]);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(eventSpy.calls.count()).toBe(1);
        });
      });
    }),
  ));

  it('should mimic file selection, upload click and throw (upload) event', async(
    inject([], () => {
      const fixture: ComponentFixture<NuFileUploadBasicTestComponent> = TestBed.createComponent(NuFileUploadBasicTestComponent);
      const component: NuFileUploadBasicTestComponent = fixture.debugElement.componentInstance;

      const eventSpy: jasmine.Spy = spyOn(component, 'uploadEvent');

      component.multiple = false;
      component.disabled = false;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.query(By.css('nuarch-file-input'))).toBeTruthy();
        expect(fixture.debugElement.query(By.css('.nuarch-file-upload'))).toBeFalsy();
        expect(eventSpy.calls.count()).toBe(0);
        fixture.debugElement.query(By.directive(NuFileUploadComponent)).componentInstance.handleSelect([{}]);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(component.selectFiles).toBeTruthy();
          expect(eventSpy.calls.count()).toBe(0);
          fixture.debugElement.query(By.css('.nuarch-file-upload')).triggerEventHandler('click', new Event('click'));
          fixture.detectChanges();
          fixture.whenStable().then(() => {
            expect(eventSpy.calls.count()).toBe(1);
          });
        });
      });
    }),
  ));

  it('should mimic file selection, cancel click and throw (cancel) event', async(
    inject([], () => {
      const fixture: ComponentFixture<NuFileUploadBasicTestComponent> = TestBed.createComponent(NuFileUploadBasicTestComponent);
      const component: NuFileUploadBasicTestComponent = fixture.debugElement.componentInstance;

      const eventSpy: jasmine.Spy = spyOn(component, 'cancelEvent');

      component.multiple = false;
      component.disabled = false;
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.query(By.css('nuarch-file-input'))).toBeTruthy();
        expect(fixture.debugElement.query(By.css('.nuarch-file-upload'))).toBeFalsy();
        fixture.debugElement.query(By.directive(NuFileUploadComponent)).componentInstance.handleSelect([{}]);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(component.selectFiles).toBeTruthy();
          expect(eventSpy.calls.count()).toBe(0);
          fixture.debugElement.query(By.css('.nuarch-file-upload-cancel')).triggerEventHandler('click', new Event('click'));
          fixture.detectChanges();
          fixture.whenStable().then(() => {
            expect(eventSpy.calls.count()).toBe(1);
          });
        });
      });
    }),
  ));
});

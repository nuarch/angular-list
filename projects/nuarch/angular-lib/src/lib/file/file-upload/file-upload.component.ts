import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  EventEmitter,
  forwardRef,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ICanDisable, IControlValueAccessor, mixinControlValueAccessor, mixinDisabled } from '../../shared/behaviors';
import { NuFileInputComponent, NuFileInputLabelDirective } from '../file-input/file-input.component';

export class NuFileUploadBase {
  constructor(public _changeDetectorRef: ChangeDetectorRef) {
  }
}

/* eslint-disable-next-line */
export const _NuFileUploadMixinBase = mixinControlValueAccessor(mixinDisabled(NuFileUploadBase));

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => NuFileUploadComponent),
      multi: true,
    },
  ],
  selector: 'nuarch-file-upload',
  inputs: ['disabled', 'value'],
  styleUrls: ['./file-upload.component.scss'],
  templateUrl: './file-upload.component.html',
})
export class NuFileUploadComponent extends _NuFileUploadMixinBase implements IControlValueAccessor, ICanDisable {
  private _multiple: boolean = false;
  private _required: boolean = false;
  @ViewChild(NuFileInputComponent, {static: false}) fileInput: NuFileInputComponent;
  @ContentChild(NuFileInputLabelDirective, {static: false}) inputLabel: NuFileInputLabelDirective;
  /**
   * defaultColor?: 'accent' | 'primary' | 'warn'
   * Sets browse button color. Uses same color palette accepted as [MatButton] and defaults to 'primary'.
   */
  @Input() defaultColor: 'accent' | 'primary' | 'warn' = 'primary';
  /**
   * activeColor?: 'accent' | 'primary' | 'warn'
   * Sets upload button color. Uses same color palette accepted as [MatButton] and defaults to 'accent'.
   */
  @Input() activeColor: 'accent' | 'primary' | 'warn' = 'accent';
  /**
   * cancelColor?: 'accent' | 'primary' | 'warn'
   * Sets cancel button color. Uses same color palette accepted as [MatButton] and defaults to 'warn'.
   */
  @Input() cancelColor: 'accent' | 'primary' | 'warn' = 'warn';
  /**
   * accept?: string
   * Sets files accepted when opening the file browser dialog.
   * Same as 'accept' attribute in <input/> element.
   */
  @Input() accept: string;
  /**
   * select?: function
   * Event emitted when a file is selected.
   * Emits a [File | FileList] object.
   */
  @Output() select: EventEmitter<File | FileList> = new EventEmitter<File | FileList>();
  /**
   * upload?: function
   * Event emitted when upload button is clicked.
   * Emits a [File | FileList] object.
   */
  @Output() upload: EventEmitter<File | FileList> = new EventEmitter<File | FileList>();
  /**
   * cancel?: function
   * Event emitted when cancel button is clicked.
   */
  @Output() cancel: EventEmitter<void> = new EventEmitter<void>();

  constructor(_changeDetectorRef: ChangeDetectorRef) {
    super(_changeDetectorRef);
  }

  get multiple(): boolean {
    return this._multiple;
  }

  /**
   * multiple?: boolean
   * Sets if multiple files can be dropped/selected at once in [NuFileUploadComponent].
   */
  @Input('multiple')
  set multiple(multiple: boolean) {
    this._multiple = coerceBooleanProperty(multiple);
  }

  get required(): boolean {
    return this._required;
  }

  /**
   * required?: boolean
   * Forces at least one file upload.
   * Defaults to 'false'
   */
  @Input('required')
  set required(required: boolean) {
    this._required = coerceBooleanProperty(required);
  }

  /**
   * Method executed when upload button is clicked.
   */
  uploadPressed(): void {
    if (this.value) {
      this.upload.emit(this.value);
    }
  }

  /**
   * Method executed when a file is selected.
   */
  handleSelect(value: File | FileList): void {
    this.value = value;
    this.select.emit(value);
  }

  /**
   * Methods executed when cancel button is clicked.
   * Clears files.
   */
  _cancel(): void {
    this.value = undefined;
    this.cancel.emit();
    // check if the file input is rendered before clearing it
    if (this.fileInput) {
      this.fileInput.clear();
    }
  }

  /** Method executed when the disabled value changes */
  onDisabledChange(v: boolean): void {
    if (v) {
      this._cancel();
    }
  }
}

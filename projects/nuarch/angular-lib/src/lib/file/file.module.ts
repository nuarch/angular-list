import { PortalModule } from '@angular/cdk/portal';

import { CommonModule } from '@angular/common';
import { NgModule, Type } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NuFileDropDirective } from './directives/file-drop.directive';

import { NuFileSelectDirective } from './directives/file-select.directive';
import { NuFileInputComponent, NuFileInputLabelDirective } from './file-input/file-input.component';
import { NuFileUploadComponent } from './file-upload/file-upload.component';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const TD_FILE: Type<any>[] = [
  NuFileSelectDirective,
  NuFileDropDirective,
  NuFileUploadComponent,
  NuFileInputComponent,
  NuFileInputLabelDirective,
];

@NgModule({
  imports: [FormsModule, CommonModule, MatIconModule, MatButtonModule, PortalModule],
  declarations: [TD_FILE],
  exports: [TD_FILE],
})
export class FileModule {
}

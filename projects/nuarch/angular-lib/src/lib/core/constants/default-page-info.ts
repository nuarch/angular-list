import { PageInfo } from '../models/page-info';
import { defaultPageNumber, defaultPageSize } from './default-page-size';

export const defaultPageInfo: PageInfo = {
  page: defaultPageNumber,
  size: defaultPageSize,
  sort: undefined,
};

export const defaultSortDirection: string = 'desc';

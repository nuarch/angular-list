export const defaultPageSize: number = 20;
export const defaultPageNumber: number = 0;
export const defaultPageSizeOptions: number[] = [20, 50, 100, 200];

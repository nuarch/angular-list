import { Filter } from '../models/filter';

export const defaultFilter: Filter = {
  createdBy: '',
  createdDate: '',
  id: '',
};

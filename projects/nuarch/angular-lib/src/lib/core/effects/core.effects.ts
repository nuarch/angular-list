import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { get } from 'lodash';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import {
  CoreActionTypes,
  DeleteDetailsAction,
  DeleteDetailsFailAction,
  DeleteDetailsSuccessAction,
  failActionTypes,
  LoadDetailsAction,
  LoadDetailsFailAction,
  LoadDetailsSuccessAction,
  LoadListAction,
  LoadListFailAction,
  LoadListSuccessAction,
  SaveDetailsAction,
  SaveDetailsFailAction,
  SaveDetailsSuccessAction,
  UpdateDetailsAction,
  UpdateDetailsFailAction,
  UpdateDetailsSuccessAction,
  UpdateListItemsAction,
  UpdateListItemsFailAction,
  UpdateListItemsSuccessAction,
} from '../actions/core.action';
import { handleLoadFail } from '../functions/handle-load-fail';
import { Entity } from '../models/entity';
import { LoadListPayload } from '../models/load-list-payload';
import { LoadDetailsPayload } from '../models/load-details-payload';
import { Page } from '../models/page';
import { SaveDetailsPayload } from '../models/save-details-payload';
import { UpdateDetailsPayload } from '../models/update-details-payload';
import { DeleteDetailsPayload } from '../models/delete-details-payload';
import { CoreService } from '../services/core.service';
import { defaultSuccessMessage, SuccessPayload } from '../models/success-payload';
import { MassUpdateDetailsPayload } from '../models/mass-update-details-payload';
import { MassUpdateDetailsReponse } from '../models/mass-update-details-response';

@Injectable()
export class CoreEffects<T extends Entity> {

  @Effect()
  loadList$: Observable<Action> = this.actions$.pipe(
    ofType<LoadListAction>(CoreActionTypes.LoadList),
    map((action: LoadListAction) => action.payload),
    switchMap((payload: LoadListPayload) => {
      return this.coreService.loadList(payload)
        .pipe(map((page: Page<T>) => new LoadListSuccessAction(page)),
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          catchError((err: any) => of(new LoadListFailAction(err))),
        );
    }),
  );

  @Effect()
  loadDetails$: Observable<Action> = this.actions$.pipe(
    ofType<LoadDetailsAction>(CoreActionTypes.LoadDetails),
    map((action: LoadDetailsAction) => action.payload),
    switchMap((payload: LoadDetailsPayload) => {
      return this.coreService.loadDetails(payload)
        .pipe(map((entity: Entity) => new LoadDetailsSuccessAction(entity)),
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          catchError((err: any) => of(new LoadDetailsFailAction(err))),
        );
    }),
  );

  @Effect({dispatch: false})
  loadFail: Observable<Action> = this.actions$.pipe(
    ofType(...failActionTypes),
    tap((action: Action) => {
      handleLoadFail(get(action, 'payload'), this.snackbar);
    }),
  );

  @Effect()
  saveDetails$: Observable<Action> = this.actions$.pipe(
    ofType<SaveDetailsAction<T>>(CoreActionTypes.SaveDetails),
    map((action: SaveDetailsAction<T>) => action.payload),
    switchMap((payload: SaveDetailsPayload<T>) => {
      return this.coreService.saveDetails(payload)
        .pipe(
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          map(() => new SaveDetailsSuccessAction({
            redirect: payload.redirect,
            message: payload.message,
            redirectUrl: payload.redirectUrl,
          })),
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          catchError((err: any) => {
            return of(new SaveDetailsFailAction(err));
          }),
        );
    }),
  );

  @Effect()
  updateDetails$: Observable<Action> = this.actions$.pipe(
    ofType<UpdateDetailsAction<T>>(CoreActionTypes.UpdateDetails),
    map((action: UpdateDetailsAction<T>) => action.payload),
    switchMap((payload: UpdateDetailsPayload<T>) => {
      return this.coreService.updateDetails(payload)
        .pipe(
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          map((entity: Entity) => new UpdateDetailsSuccessAction({
              updatedEntityId: entity.id,
              redirect: payload.redirect,
              message: payload.message,
              redirectUrl: payload.redirectUrl,
          })),
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          catchError((err: any) => {
            return of(new UpdateDetailsFailAction(err));
          }),
        );
    }),
  );

  @Effect()
  deleteDetails$: Observable<Action> = this.actions$.pipe(
    ofType<DeleteDetailsAction>(CoreActionTypes.DeleteDetails),
    map((action: DeleteDetailsAction) => action.payload),
    switchMap((payload: DeleteDetailsPayload) => {
      return this.coreService.deleteDetails(payload)
        .pipe(
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          map(() => new DeleteDetailsSuccessAction()),
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          catchError((err: any) => {
            return of(new DeleteDetailsFailAction(err));
          }),
        );
    }),
  );

  @Effect({dispatch: false})
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  redirectToList$: any = this.actions$.pipe(
    ofType(CoreActionTypes.SaveDetailsSuccess, CoreActionTypes.UpdateDetailsSuccess, CoreActionTypes.UpdateListItemsSuccess),
    map((action: SaveDetailsSuccessAction | UpdateDetailsSuccessAction | UpdateListItemsSuccessAction) => action.payload),
    tap((payload: SuccessPayload) => {
      this.snackbar.open(payload.message || defaultSuccessMessage, 'OK', {panelClass: ['success-snackbar']});
      if (payload.redirect) {
        this.router.navigate([payload.redirectUrl]);
      }
    }),
  );

  @Effect()
  massUpdateDetails$: Observable<Action> = this.actions$.pipe(
    ofType<UpdateListItemsAction<T>>(CoreActionTypes.UpdateListItems),
    map((action: UpdateListItemsAction<T>) => action.payload),
    switchMap((payload: MassUpdateDetailsPayload<T>) => {
      return this.coreService.massUpdateDetails(payload)
        .pipe(
          map((massResponse: MassUpdateDetailsReponse) => {
            const message: string = !get(massResponse, 'failureModels.length') ? payload.message || defaultSuccessMessage :
              `${massResponse.failureModels.length} records were not updated.`;

            return new UpdateListItemsSuccessAction({redirect: payload.redirect, redirectUrl: payload.redirectUrl, message, massResponse});
          }),
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          catchError((err: any) => {
            return of(new UpdateListItemsFailAction(err));
          }),
        );
    }),
  );

  constructor(private actions$: Actions,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private coreService: CoreService<T>,
              private snackbar: MatSnackBar) {
  }

}

import { CommonModule } from '@angular/common';
import { TestBed } from '@angular/core/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions } from '@ngrx/effects';
import { cold, hot } from 'jasmine-marbles';
import { TestColdObservable } from 'jasmine-marbles/src/test-observables';
import moment from 'moment';
import { EMPTY, Observable, of, throwError } from 'rxjs';
import { anything, instance, mock, reset, when } from 'ts-mockito/lib/ts-mockito';

import {
  LoadListAction,
  LoadListSuccessAction,
  LoadDetailsAction,
  LoadDetailsSuccessAction,
  LoadDetailsFailAction,
  SaveDetailsAction,
  SaveDetailsSuccessAction,
  SaveDetailsFailAction,
  UpdateDetailsAction,
  UpdateDetailsSuccessAction,
  UpdateDetailsFailAction,
} from '../actions/core.action';
import { CoreService } from '../services/core.service';
import { CoreEffects } from './core.effects';
import { Entity } from '../models/entity';
import { defaultPageInfo } from '../constants/default-page-info';
import { LoadListPayload } from '../models/load-list-payload';
import { Page } from '../models/page';
import { SaveDetailsPayload } from '../models/save-details-payload';
import { UpdateDetailsPayload } from '../models/update-details-payload';

export class TestActions extends Actions {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  source: Observable<any>;

  constructor() {
    super(EMPTY);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  set stream(source: Observable<any>) {
    this.source = source;
  }
}

export function getActions(): TestActions {
  return new TestActions();
}

describe('Effects: CoreEffects', () => {
  let effects: CoreEffects<Entity>;
  let actions: TestActions;

  const mockCoreService: CoreService<Entity> = mock(CoreService);
  const coreService: CoreService<Entity> = instance(mockCoreService);

  let mockActivatedRoute: ActivatedRoute = mock(ActivatedRoute);
  let activatedRoute: ActivatedRoute = instance(mockActivatedRoute);

  const mockRouter: Router = mock(Router);
  const router: Router = instance(mockRouter);

  const snackBarSpy: MatSnackBar = jasmine.createSpyObj<MatSnackBar>('MatSnackBar', ['open']);

  const testEentity: Entity = {
    id: '1',
    createdBy: '2020-02-03',
    createdDate: '2020-02-03',
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        MatSnackBarModule,
      ],
      declarations: [],
      providers: [
        CoreEffects,
        { provide: CoreService, useValue: coreService },
        { provide: Router, useValue: router },
        { provide: Actions, useFactory: getActions },
        { provide: MatSnackBar, useValue: snackBarSpy },
        { provide: ActivatedRoute, useValue: activatedRoute},
      ],
    });

    effects = TestBed.get(CoreEffects);
    actions = TestBed.get(Actions);
    reset(mockRouter);
  });

  it('loadList - should return a LoadUsersSuccess action, with the lawsPageInfo and laws, on success', () => {
    const action: LoadListAction = new LoadListAction(new LoadListPayload({pageInfo: defaultPageInfo}));
    const outcomeData: Page<Entity> = {entity: [], pageInfo: defaultPageInfo, serverPageInfo: { totalElements: 5 }};
    const outcome: LoadListSuccessAction<Entity> = new LoadListSuccessAction(outcomeData);
    when(mockCoreService.loadList(anything())).thenReturn(of(outcomeData));
    actions.stream = hot('-a', { a: action });
    const expected: TestColdObservable = cold('-b', { b: outcome });

    expect(effects.loadList$).toBeObservable(expected);
  });

  it('loadDetails success - should return a LoadDetailsSuccess action, with the entity on success', () => {
    const action: LoadDetailsAction = new LoadDetailsAction({
      id: '1',
      url: 'someurl',
    });
    const outcome: LoadDetailsSuccessAction<Entity> = new LoadDetailsSuccessAction(testEentity);
    when(mockCoreService.loadDetails(anything())).thenReturn(of(testEentity));
    actions.stream = hot('-a', { a: action });
    const expected: TestColdObservable = cold('-b', { b: outcome });
    expect(effects.loadDetails$).toBeObservable(expected);
  });

  it('loadDetails fail - should return a LoadDetailsFailAction action, with the error message', () => {
    const action: LoadDetailsAction = new LoadDetailsAction({
      id: '1',
      url: 'someurl',
    });
    const outcomeData: string = 'Error message';
    const outcome: LoadDetailsFailAction = new LoadDetailsFailAction(outcomeData);
    when(mockCoreService.loadDetails(anything())).thenReturn(throwError(outcomeData));
    actions.stream = hot('-a', { a: action });
    const expected: TestColdObservable = cold('-b', { b: outcome });

    expect(effects.loadDetails$).toBeObservable(expected);
  });

  it('saveDetails success - should return a SaveDetailsSuccessAction action', () => {
    const payload: SaveDetailsPayload<Entity> = {
      entity: testEentity,
      url: 'someurl',
      redirect: true,
      message: 'test',
    };
    const action: SaveDetailsAction<Entity> = new SaveDetailsAction(payload);
    const outcome: SaveDetailsSuccessAction = new SaveDetailsSuccessAction({redirect: true, message: 'test', redirectUrl: undefined});
    when(mockCoreService.saveDetails(anything())).thenReturn(of(undefined));
    actions.stream = hot('-a', { a: action });
    const expected: TestColdObservable = cold('-b', { b: outcome });

    expect(effects.saveDetails$).toBeObservable(expected);
  });

  it('saveDetails fail - should return a SaveDetailsFailAction action, with error message',
    () => {
      const payload: SaveDetailsPayload<Entity> = {
        entity: testEentity,
        url: 'someurl',
      };
      const action: SaveDetailsAction<Entity> = new SaveDetailsAction(payload);
      const outcomeData: string = 'Error message';
      const outcome: SaveDetailsFailAction = new SaveDetailsFailAction(outcomeData);
      when(mockCoreService.saveDetails(anything())).thenReturn(throwError(outcomeData));
      actions.stream = hot('-a', { a: action });
      const expected: TestColdObservable = cold('-b', { b: outcome });

      expect(effects.saveDetails$).toBeObservable(expected);
  });

  it('updateDetails success - should return a UpdateDetailsSuccessAction', () => {
    const payload: UpdateDetailsPayload<Entity> = {
      id: '1',
      entity: testEentity,
      url: 'someurl',
      redirect: true,
      message: 'test',
    };
    const action: UpdateDetailsAction<Entity> = new UpdateDetailsAction(payload);
    const outcome: UpdateDetailsSuccessAction = new UpdateDetailsSuccessAction({
      updatedEntityId: '1',
      redirect: true,
      message: 'test',
      redirectUrl: undefined,
    });
    when(mockCoreService.updateDetails(anything())).thenReturn(of(testEentity));
    actions.stream = hot('-a', { a: action });
    const expected: TestColdObservable = cold('-b', { b: outcome });

    expect(effects.updateDetails$).toBeObservable(expected);
  });

  it('updateDetails fail - should return a SaveDetailsFailAction action, with error message',
    () => {
      const payload: UpdateDetailsPayload<Entity> = {
        id: '1',
        entity: testEentity,
        url: 'someurl',
      };
      const action: UpdateDetailsAction<Entity> = new UpdateDetailsAction(payload);
      const outcomeData: string = 'Error message';
      const outcome: UpdateDetailsFailAction = new UpdateDetailsFailAction(outcomeData);
      when(mockCoreService.updateDetails(anything())).thenReturn(throwError(outcomeData));
      actions.stream = hot('-a', { a: action });
      const expected: TestColdObservable = cold('-b', { b: outcome });

      expect(effects.updateDetails$).toBeObservable(expected);
    });
});

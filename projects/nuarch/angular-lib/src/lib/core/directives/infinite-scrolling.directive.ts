import { AfterViewInit, Directive, ElementRef, EventEmitter, Input, Output } from '@angular/core';

import { fromEvent, Observable } from 'rxjs';
import { takeUntil, filter, map, pairwise } from 'rxjs/operators';
import { AbstractComponent } from '../components/abstract-component/abstract-component';

interface ScrollPosition {
  sH: number;
  sT: number;
  cH: number;
}

const DEFAULT_SCROLL_POSITION: ScrollPosition = {
  sH: 0,
  sT: 0,
  cH: 0,
};

@Directive({
  selector: '[nuarchInfiniteScrolling]',
})
export class InfiniteScrollingDirective extends AbstractComponent implements AfterViewInit {

  @Output() loadData: EventEmitter<void> = new EventEmitter<void>();

  @Input() loading: Observable<boolean>;
  @Input() scrollPercent: number = 60;

  constructor(private elm: ElementRef) {
    super();
  }

  ngAfterViewInit(): void {

    fromEvent<Event>(this.elm.nativeElement, 'scroll')
      .pipe(
        takeUntil(this.componentDestroyed),
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        map((e: any) => {
          return {
            sH: e.target.scrollHeight,
            sT: e.target.scrollTop,
            cH: e.target.clientHeight,
          };
        }),
        pairwise(),
        filter((positions: ScrollPosition[]) =>
          this.isUserScrollingDown(positions) && this.isScrollExpectedPercent(positions[1])),
      )
      .subscribe(() => {
        this.loadData.emit();
      });
  }

  afterOnDestroy(): void {
    // afterOnDestroy
  }

  private isUserScrollingDown = (positions: ScrollPosition[]) => {
    return positions[0].sT < positions[1].sT;
  }
  private isScrollExpectedPercent = (position: ScrollPosition) => {
    return ((position.sT + position.cH) / position.sH) >= 0.99;
  }

}

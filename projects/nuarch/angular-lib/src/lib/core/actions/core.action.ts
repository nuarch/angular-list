import { Action } from '@ngrx/store';
import { DeleteDetailsPayload } from '../models/delete-details-payload';
import { keys, map } from 'lodash';
import { Entity } from '../models/entity';
import { Filter } from '../models/filter';
import { LoadListPayload } from '../models/load-list-payload';
import { LoadDetailsPayload } from '../models/load-details-payload';
import { Page } from '../models/page';
import { PageInfo } from '../models/page-info';
import { SaveDetailsPayload } from '../models/save-details-payload';
import { UpdateDetailsPayload } from '../models/update-details-payload';
import { SuccessPayload } from '../models/success-payload';
import { MassUpdateDetailsPayload } from '../models/mass-update-details-payload';

export enum CoreActionTypes {

  LoadList = '[List] Load',
  LoadListSuccess = '[List] Load Success',
  LoadListFail = '[List] Load Fail',
  ResetList = '[List] Reset List',
  ResetAll = '[List] Reset All',

  SetFilter = '[Filter] Set',
  SetDefaultFilter = '[Filter] Default Set',
  ClearFilter = '[Filter] Clear',
  ReadyFilter = '[Filter] Ready',

  SetSearch = '[Search] Set',
  ClearSearch = '[Search] Clear',

  UpdatePageInfo = '[Page Info] Update',

  LoadDetails = '[Details] Load',
  LoadDetailsSuccess = '[Details] Load Success',
  LoadDetailsFail = '[Details] Load Fail',

  SaveDetails = '[Details] Save',
  SaveDetailsSuccess = '[Details] Save Success',
  SaveDetailsFail = '[Details] Save Fail',

  DeleteDetails = '[Details] Delete',
  DeleteDetailsSuccess = '[Details] Delete Success',
  DeleteDetailsFail = '[Details] Delete Fail',

  UpdateDetails = '[Details] Update',
  UpdateDetailsSuccess = '[Details] Update Success',
  UpdateDetailsFail = '[Details] Update Fail',
  ResetDetails = '[Details] Reset',

  SelectListItem = '[List] Select Item',
  UnselectListItem = '[List] Unselect Item',

  SelectAllListItems = '[List] Select All',
  UnselectAllListItems = '[List] Unselect All',

  UpdateListItems = '[List] Update List Items',
  UpdateListItemsSuccess = '[List] Update List Items Success',
  UpdateListItemsFail = '[List] Update List Items Fail',
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const successActionTypes: any[] = actionTypesByNameContains('Success');

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const failActionTypes: any[] = actionTypesByNameContains('Fail');

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function actionTypesByNameContains(name: string): any[] {
  const actionTypes: string[] = keys(CoreActionTypes);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return map(actionTypes.filter((k: string) => k.indexOf(name) > -1), (s: keyof typeof CoreActionTypes) => CoreActionTypes[s]);
}

export class LoadListAction implements Action {
  readonly type: string = CoreActionTypes.LoadList;

  constructor(public payload: LoadListPayload) {
  }
}

export class LoadListSuccessAction<T extends Entity> implements Action {
  readonly type: string = CoreActionTypes.LoadListSuccess;

  constructor(public payload: Page<T>) {
  }
}

export class LoadListFailAction implements Action {
  readonly type: string = CoreActionTypes.LoadListFail;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public payload?: any) {
  }
}

export class LoadDetailsAction implements Action {
  readonly type: string = CoreActionTypes.LoadDetails as string;

  constructor(public payload: LoadDetailsPayload) {
  }
}

export class LoadDetailsSuccessAction<T extends Entity> implements Action {
  readonly type: string = CoreActionTypes.LoadDetailsSuccess as string;

  constructor(public payload: T) {
  }
}

export class LoadDetailsFailAction implements Action {
  readonly type: string = CoreActionTypes.LoadDetailsFail as string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public payload?: any) {
  }
}

export class DeleteDetailsAction implements Action {
  readonly type: string = CoreActionTypes.DeleteDetails as string;

  constructor(public payload: DeleteDetailsPayload) {
  }
}

export class DeleteDetailsSuccessAction implements Action {
  readonly type: string = CoreActionTypes.DeleteDetailsSuccess as string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public payload?: any) {
  }
}

export class DeleteDetailsFailAction implements Action {
  readonly type: string = CoreActionTypes.DeleteDetailsFail as string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public payload?: any) {
  }
}

export class SaveDetailsAction<T extends Entity> implements Action {
  readonly type: string = CoreActionTypes.SaveDetails as string;

  constructor(public payload: SaveDetailsPayload<T>) {
  }
}

export class SaveDetailsSuccessAction implements Action {
  readonly type: string = CoreActionTypes.SaveDetailsSuccess as string;

  constructor(public payload: SuccessPayload) {
  }
}

export class SaveDetailsFailAction implements Action {
  readonly type: string = CoreActionTypes.SaveDetailsFail as string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public payload?: any) {
  }
}

export class UpdateDetailsAction<T extends Entity> implements Action {
  readonly type: string = CoreActionTypes.UpdateDetails as string;

  constructor(public payload: UpdateDetailsPayload<T>) {
  }
}

export class UpdateDetailsSuccessAction implements Action {
  readonly type: string = CoreActionTypes.UpdateDetailsSuccess as string;

  constructor(public payload: SuccessPayload) {
  }
}

export class UpdateDetailsFailAction implements Action {
  readonly type: string = CoreActionTypes.UpdateDetailsFail as string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public payload?: any) {
  }
}

export class ResetListAction implements Action {
  readonly type: string = CoreActionTypes.ResetList;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public payload?: any) {
  }
}

export class ResetAllAction implements Action {
  readonly type: string = CoreActionTypes.ResetAll;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public payload?: any) {
  }
}

export class UpdatePageInfoAction implements Action {
  readonly type: string = CoreActionTypes.UpdatePageInfo as string;

  constructor(public payload?: PageInfo) {
  }
}

export class ResetDetailsAction implements Action {
  readonly type: string = CoreActionTypes.ResetDetails;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public payload?: any) {
  }
}

export class ReadyFilterAction implements Action {
  readonly type: string = CoreActionTypes.ReadyFilter as string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public payload?: any) {
  }
}

export class SetFilterAction implements Action {
  readonly type: string = CoreActionTypes.SetFilter as string;

  constructor(public payload?: Filter) {
  }
}

export class SetDefaultFilterAction implements Action {
  readonly type: string = CoreActionTypes.SetDefaultFilter as string;

  constructor(public payload?: Filter) {
  }
}

export class ClearFilterAction implements Action {
  readonly type: string = CoreActionTypes.ClearFilter as string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public payload?: any) {
  }
}

export class SetSearchAction implements Action {
  readonly type: string = CoreActionTypes.SetSearch as string;

  constructor(public payload: string) {
  }
}

export class ClearSearchAction implements Action {
  readonly type: string = CoreActionTypes.ClearSearch as string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public payload?: any) {
  }
}

export class SelectListItemAction implements Action {
  readonly type: string = CoreActionTypes.SelectListItem as string;

  constructor(public payload: string) {

  }
}

export class UnselectListItemAction implements Action {
  readonly type: string = CoreActionTypes.UnselectListItem as string;

  constructor(public payload: string) {

  }
}

export class SelectAllListItemsAction implements Action {
  readonly type: string = CoreActionTypes.SelectAllListItems as string;

  constructor(public payload: unknown = undefined) {

  }
}

export class UnselectAllListItemsAction implements Action {
  readonly type: string = CoreActionTypes.UnselectAllListItems as string;

  constructor(public payload: unknown = undefined) {

  }
}

export class UpdateListItemsAction<T extends Entity> implements Action {
  readonly type: string = CoreActionTypes.UpdateListItems as string;

  constructor(public payload: MassUpdateDetailsPayload<T>) {

  }
}

export class UpdateListItemsSuccessAction implements Action {
  readonly type: string = CoreActionTypes.UpdateListItemsSuccess as string;

  constructor(public payload: SuccessPayload) {

  }
}

export class UpdateListItemsFailAction implements Action {
  readonly type: string = CoreActionTypes.UpdateListItemsFail as string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public payload?: any) {
  }
}

export type CoreActions<T extends Entity>
  = LoadListAction
  | LoadListSuccessAction<T>
  | LoadListFailAction
  | UpdatePageInfoAction
  | LoadDetailsAction
  | LoadDetailsSuccessAction<T>
  | LoadDetailsFailAction
  | DeleteDetailsAction
  | DeleteDetailsSuccessAction
  | DeleteDetailsFailAction
  | SaveDetailsAction<T>
  | SaveDetailsSuccessAction
  | SaveDetailsFailAction
  | UpdateDetailsAction<T>
  | UpdateDetailsSuccessAction
  | UpdateDetailsFailAction
  | ResetDetailsAction
  | ResetListAction
  | ResetAllAction
  | ReadyFilterAction
  | SetDefaultFilterAction
  | SetFilterAction
  | ClearFilterAction
  | SetSearchAction
  | ClearSearchAction
  | SelectAllListItemsAction
  | SelectListItemAction
  | UnselectAllListItemsAction
  | UnselectListItemAction
  | UpdateListItemsAction<T>
  | UpdateListItemsSuccessAction
  | UpdateListItemsFailAction;

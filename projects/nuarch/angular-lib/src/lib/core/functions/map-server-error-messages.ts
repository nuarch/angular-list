import { get, isEmpty, isString, map, uniq } from 'lodash';
import { optimisticlLockErrorRegexp } from '../constants/optimistic-lock-error-regexp';

export const optimisticLockErrorMessageInForm: string = `Optimistic locking error. You can try again by clicking
  refresh button but you will lose your changes.`;
export const optimisticLockErrorMessageInList: string = 'Optimistic locking error. You can try again.';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function mapServerErrorMessages(serverError: any, optimisticLockErrorMessage: string = optimisticLockErrorMessageInForm): string[] {
  if (optimisticlLockErrorRegexp.test(get(serverError, 'message'))) {
    return [optimisticLockErrorMessage];
  }
  if (isString(serverError)) {
    return [serverError];
  }
  if (isEmpty(get(serverError, 'error.errors'))) {
    return [get(serverError, 'error.message', get(serverError, 'message'))];
  } else {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return uniq(map(serverError.error.errors, (error: any) => {
      const fieldName: string = get(error, 'field');
      const errorMessage: string = get(error, 'message', get(error, 'defaultMessage'));
      return !!fieldName ? `${fieldName} ${errorMessage}` : errorMessage;
    }));
  }
}

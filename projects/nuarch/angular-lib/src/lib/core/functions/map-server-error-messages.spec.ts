import { mapServerErrorMessages, optimisticLockErrorMessageInForm } from './map-server-error-messages';

describe('mapServerErrorMessages method', () => {

  it('should return an array contains optimistic lock error', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const serverError: any = {
      message: 'optimistic lock exception',
    };
    expect(mapServerErrorMessages(serverError)).toEqual([optimisticLockErrorMessageInForm]);
  });

  it('should return an array contains optimistic lock error', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const serverError: any = {
      message: 'Optimistic Lock exception',
    };
    expect(mapServerErrorMessages(serverError)).toEqual([optimisticLockErrorMessageInForm]);
  });

  it('should return an array of error when serverError is string', () => {
    const serverError: string = 'server error';
    expect(mapServerErrorMessages(serverError)).toEqual([serverError]);
  });

  it('should return an array of errors when errors returned in message property', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const serverError: any = {
      'message': 'server error',
    };
    expect(mapServerErrorMessages(serverError)).toEqual([serverError.message]);
  });

  it('should return an array of errors when serverErrors in format [{ "field": "", "message": "" }]', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const serverError: any = {
      error: {
        errors: [{
          field: 'applicant',
          message: 'ssn not exist',
        }],
      },
    };
    const res: string[] = ['applicant ssn not exist'];
    expect(mapServerErrorMessages(serverError)).toEqual(res);
  });

  it('should return an array of errors when serverErrors in format [{ "message": "" }]', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const serverError: any = {
      error: {
        errors: [{
          message: 'ssn not exist',
        }],
      },
    };
    const res: string[] = ['ssn not exist'];
    expect(mapServerErrorMessages(serverError)).toEqual(res);
  });

  it('should return an array of errors when serverErrors in format [{ "defaultMessage": "" }]', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const serverError: any = {
      error: {
        errors: [{
          defaultMessage: 'ssn not exist',
        }],
      },
    };
    const res: string[] = ['ssn not exist'];
    expect(mapServerErrorMessages(serverError)).toEqual(res);
  });

  it('should return an array of errors when serverErrors in format [{ "field": "", "defaultMessage": "" }]', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const serverError: any = {
      error: {
        errors: [{
          field: 'applicant',
          defaultMessage: 'ssn not exist',
        }],
      },
    };
    const res: string[] = ['applicant ssn not exist'];
    expect(mapServerErrorMessages(serverError)).toEqual(res);
  });

  it('should return uniq error messages', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const serverError: any = {
      error: {
        errors: [{
          timestamp: '2019-09-25T21:36:54.631',
          exception: 'gov.nyc.dsny.smart.eis.preemployment.exceptions.ApplicationException',
          message: 'One or more applicants are missing Medical Appointment Date.',
          errorData: {},
        }, {
          timestamp: '2019-09-25T21:36:54.657',
          exception: 'gov.nyc.dsny.smart.eis.preemployment.exceptions.ApplicationException',
          message: 'One or more applicants are missing Medical Appointment Date.',
          errorData: {},
        }],
      },
    };
    const res: string[] = ['One or more applicants are missing Medical Appointment Date.'];
    expect(mapServerErrorMessages(serverError)).toEqual(res);
  });
});

import { ComponentFixture, getTestBed, TestBed } from '@angular/core/testing';

export const configureTestSuite: Function = () => {

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const testBedApi: any = getTestBed();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const originReset: any = TestBed.resetTestingModule;

  beforeAll(() => {
    TestBed.resetTestingModule();
    TestBed.resetTestingModule = () => TestBed;
  });

  afterEach(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    testBedApi._activeFixtures.forEach((fixture: ComponentFixture<any>) => fixture.destroy());
    testBedApi._instantiated = false;
  });

  afterAll(() => {
    TestBed.resetTestingModule = originReset;
    TestBed.resetTestingModule();
  });
};

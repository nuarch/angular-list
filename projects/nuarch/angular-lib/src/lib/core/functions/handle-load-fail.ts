import { MatSnackBar } from '@angular/material/snack-bar';
import { join } from 'lodash';
import { mapServerErrorMessages } from './map-server-error-messages';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function handleLoadFail(error: any, snackbar: MatSnackBar): void {
  snackbar.open(join(mapServerErrorMessages(error), ','), 'close', {panelClass: ['warn-snackbar']});

}

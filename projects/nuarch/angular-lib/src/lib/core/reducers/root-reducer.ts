import { EntitySelectors } from '@ngrx/entity/src/models';
/* eslint-disable */
import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import { Entity } from '../models/entity';
import { PageInfo } from '../models/page-info';
import * as fromCore from './core.reducer';
import { ServerPageInfo } from '../models/server-page-info';

export interface CoreState {
  entities: fromCore.LocalState;
}

export interface State {
  coreState: CoreState;
}

export const reducers: any = {
  entities: fromCore.reducer,
};

export const getCoreState: MemoizedSelector<any, any> = createFeatureSelector<CoreState>('list');

export const getCoreEntitiesState: MemoizedSelector<any, any> = createSelector(
  getCoreState, state => state.entities,
);

export const getErrors: MemoizedSelector<any, any> = createSelector(
  getCoreEntitiesState,
  fromCore.getErrors,
);

export const getPageInfo: MemoizedSelector<any, any> = createSelector(
  getCoreEntitiesState,
  fromCore.getPageInfo,
);

export const getFilterReady: MemoizedSelector<any, any> = createSelector(
  getCoreEntitiesState,
  fromCore.getFilterReady,
);

export const getDefaultFilter: MemoizedSelector<any, any> = createSelector(
  getCoreEntitiesState,
  fromCore.getDefaultFilter,
);

export const getFilter: MemoizedSelector<any, any> = createSelector(
  getCoreEntitiesState,
  fromCore.getFilter,
);

export const getSearch: MemoizedSelector<any, any> = createSelector(
  getCoreEntitiesState,
  fromCore.getSearch,
);

export const getListLoading: MemoizedSelector<any, any> = createSelector(
  getCoreEntitiesState,
  fromCore.getPageLoading,
);

export const getNoRecordsFound: MemoizedSelector<any, any> = createSelector(
  getCoreEntitiesState,
  fromCore.getNoRecordsFound,
);

export const getDetailsLoading: MemoizedSelector<any, any> = createSelector(
  getCoreEntitiesState,
  fromCore.getDetailsLoading,
);

export const getSelectedEntityId: MemoizedSelector<any, any> = createSelector(
  getCoreEntitiesState,
  fromCore.getSelectedId,
);

export const getSaveSuccess: MemoizedSelector<any, any> = createSelector(
  getCoreEntitiesState,
  fromCore.getSaveSuccess,
);

export const getUpdatedEntityId: MemoizedSelector<any, any> = createSelector(
  getCoreEntitiesState,
  fromCore.getUpdatedEntityId,
);

export const getServerPageInfo: MemoizedSelector<any, any> = createSelector(
  getCoreEntitiesState,
  fromCore.getServerPageInfo,
);

export const {
  selectIds: getListIds,
  selectEntities: getListEntities,
  selectAll: getAllList,
  selectTotal: getTotalList,
}: EntitySelectors<Entity, State> = fromCore.adapter.getSelectors(getCoreEntitiesState);

export const getPage: MemoizedSelector<any, any> = createSelector(
  getAllList,
  getPageInfo,
  getServerPageInfo,
  (entity: Entity[], pageInfo: PageInfo, serverPageInfo: ServerPageInfo) => {
    return {
      entity,
      pageInfo,
      serverPageInfo,
    };
  },
);

export const getSelectedListItems: MemoizedSelector<any, any> = createSelector(
  getCoreEntitiesState,
  fromCore.getSelectedListItems,
);

export const getSelectedEntity: MemoizedSelector<any, any> = createSelector(
  getListEntities,
  getSelectedEntityId,
  (entities, selectedEntityId) => {
    return selectedEntityId && entities[selectedEntityId];
  },
);

export const getMassResponse: MemoizedSelector<any, any> = createSelector(
  getCoreEntitiesState,
  fromCore.getMassResponse,
);

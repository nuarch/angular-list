import { createEntityAdapter, EntityAdapter } from '@ngrx/entity';
import {
  LoadListAction,
  LoadListSuccessAction,
  ResetListAction,
  SetFilterAction,
  LoadDetailsAction,
  LoadDetailsSuccessAction,
  ClearFilterAction,
  ResetDetailsAction,
  SaveDetailsAction,
  SaveDetailsSuccessAction,
  UpdateDetailsAction,
  UpdateDetailsSuccessAction, SelectListItemAction, UnselectListItemAction, SelectAllListItemsAction,
  UnselectAllListItemsAction, ReadyFilterAction, SetDefaultFilterAction,
} from '../actions/core.action';
import { initialState, LocalState, reducer } from './core.reducer';
import { defaultPageInfo } from '../constants/default-page-info';
import { Entity } from '../models/entity';
import { LoadListPayload } from '../models/load-list-payload';
import { Filter } from '../models/filter';

describe('Core Reducer', () => {

  let adapter: EntityAdapter<Entity>;
  beforeEach(() => {
    adapter = createEntityAdapter({
      selectId: (entity: Entity) => entity.id,
      sortComparer: false,
    });
  });

  it('should let you get the initial state', () => {
    const testInitialState: LocalState = adapter.getInitialState(initialState);

    expect(testInitialState).toEqual({
      ids: [],
      entities: {},
      errors: undefined,
      listLoading: false,
      detailsLoading: false,
      filterReady: false,
      pageInfo: undefined,
      selectedEntityId: undefined,
      updatedEntityId: undefined,
      filter: undefined,
      defaultFilter: undefined,
      search: undefined,
      serverPageInfo: undefined,
      selectedListItems: undefined,
      noRecordsFound: false,
      saveSuccess: false,
      massResponse: undefined,
    });
  });

  describe('actions', () => {

    it('should LoadList', () => {
      const filter: Filter = { id: 'a' };
      const action: LoadListAction = new LoadListAction(new LoadListPayload({ pageInfo: defaultPageInfo, filter: filter }));
      const updatedState: LocalState = reducer(initialState, action);
      expect(updatedState).toEqual({
        ids: [],
        entities: {},
        errors: undefined,
        listLoading: true,
        detailsLoading: false,
        filterReady: false,
        pageInfo: undefined,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: filter,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
    });

    it('should LoadListSuccess', () => {
      const action: LoadListSuccessAction<Entity> = new LoadListSuccessAction(
        { entity: [{ id: '1', createdBy: undefined, createdDate: undefined }], pageInfo: defaultPageInfo, serverPageInfo: { totalElements: 5 } });
      const updatedState: LocalState = reducer(initialState, action);
      expect(updatedState).toEqual({
        ids: ['1'],
        entities: {
          '1': { id: '1', createdBy: undefined, createdDate: undefined } as Entity,
        },
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: defaultPageInfo,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: {
          totalElements: 5,
        },
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
    });

    it('should ResetList', () => {

      let mockInitialState: LocalState = adapter.getInitialState({
        ids: ['1'],
        entities: {
          1: { id: '1' },
        },
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });

      const action: ResetListAction = new ResetListAction(undefined);
      const updatedState: LocalState = reducer(mockInitialState, action);
      expect(updatedState).toEqual({
        ids: [],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
    });

    it('should SetFilter', () => {
      const filter: Filter = {
        id: '1',
        lawSection: 'lawSection',
        createdBy: '',
        createdDate: '',
      } as Filter;
      const action: SetFilterAction = new SetFilterAction(filter);
      const updatedState: LocalState = reducer(initialState, action);
      expect(updatedState).toEqual({
        ids: [],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: filter,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
    });

    it('should SetDefaultFilter', () => {
      const filter: Filter = {
        id: '1',
        lawSection: 'lawSection',
        createdBy: '',
        createdDate: '',
      } as Filter;
      const action: SetFilterAction = new SetDefaultFilterAction(filter);
      const updatedState: LocalState = reducer(initialState, action);
      expect(updatedState).toEqual({
        ids: [],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: filter,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
    });

    it('should ClearFilter', () => {
      const filter: Filter = {
        id: '1',
        lawSection: 'lawSection',
        createdBy: '',
        createdDate: '',
      } as Filter;
      let mockInitialState: LocalState = adapter.getInitialState({
        ids: [],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: filter,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
      const action: ClearFilterAction = new ClearFilterAction();
      const updatedState: LocalState = reducer(mockInitialState, action);
      expect(updatedState).toEqual({
        ids: [],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
    });

    it('should LoadDetails', () => {
      const action: LoadDetailsAction = new LoadDetailsAction({
        id: '0012345',
        url: 'someurl',
      });
      const updatedState: LocalState = reducer(initialState, action);
      expect(updatedState).toEqual({
        ids: [],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: true,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
    });

    it('should LoadDetailsSuccess', () => {
      const entity: Entity = {
        id: '0012345',
        createdBy: '',
        createdDate: '',
      };
      const action: LoadDetailsSuccessAction<Entity> = new LoadDetailsSuccessAction(entity);
      const updatedState: LocalState = reducer(initialState, action);
      expect(updatedState).toEqual({
        ids: ['0012345'],
        entities: {
          '0012345': entity,
        },
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: '0012345',
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
    });

    it('should ResetDetails', () => {
      let mockInitialState: LocalState = adapter.getInitialState({
        ids: ['0012345'],
        entities: {
          '0012345': {
            id: '0012345',
            createdBy: '',
            createdDate: '',
          },
        },
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: '0012345',
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
      const action: ResetDetailsAction = new ResetDetailsAction();
      const updatedState: LocalState = reducer(mockInitialState, action);
      expect(updatedState).toEqual({
        ids: [],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
    });

    it('should SaveDetails', () => {
      const entity: Entity = {
        id: '0012345',
        createdBy: '',
        createdDate: '',
      };
      const action: SaveDetailsAction<Entity> = new SaveDetailsAction({ entity: entity, url: 'someurl' });
      const updatedState: LocalState = reducer(initialState, action);
      expect(updatedState).toEqual({
        ids: [],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: true,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
    });

    it('should SaveDetailsSuccess', () => {
      const action: SaveDetailsSuccessAction = new SaveDetailsSuccessAction({ redirect: true, message: 'test' });
      const updatedState: LocalState = reducer(initialState, action);
      expect(updatedState).toEqual({
        ids: [],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: true,
        massResponse: undefined,
      });
    });

    it('should UpdateDetails', () => {
      const entity: Entity = {
        id: '0012345',
        createdBy: '',
        createdDate: '',
      };
      const action: UpdateDetailsAction<Entity> = new UpdateDetailsAction({ id: '0012345', entity: entity, url: 'someurl', redirectUrl: undefined });
      const updatedState: LocalState = reducer(initialState, action);
      expect(updatedState).toEqual({
        ids: [],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: true,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
    });

    it('should UpdateDetailsSuccess', () => {
      const action: UpdateDetailsSuccessAction = new UpdateDetailsSuccessAction({ redirect: true, message: 'test', redirectUrl: undefined });
      const updatedState: LocalState = reducer(initialState, action);
      expect(updatedState).toEqual({
        ids: [],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: true,
        massResponse: undefined,
      });
    });

    it('should SelectListItem', () => {
      const ids: string[] = ['2', '3', '4'];
      const id: string = '1';

      let mockInitialState: LocalState = adapter.getInitialState({
        ids: [],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: ids,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });

      const expected: string[] = ids.concat(id);
      const action: SelectListItemAction = new SelectListItemAction(id);
      const updatedState: LocalState = reducer(mockInitialState, action);
      expect(updatedState).toEqual({
        ids: [],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: expected,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
    });

    it('should UnselectListItem', () => {
      const ids: string[] = ['2', '3', '4'];
      const id: string = '3';

      let mockInitialState: LocalState = adapter.getInitialState({
        ids: [],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: ids,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });

      const expected: string[] = ['2', '4'];
      const action: UnselectListItemAction = new UnselectListItemAction(id);
      const updatedState: LocalState = reducer(mockInitialState, action);
      expect(updatedState).toEqual({
        ids: [],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: expected,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
    });

    it('should SelectAllListItems', () => {

      let mockInitialState: LocalState = adapter.getInitialState({
        ids: ['2', '3', '4'],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: ['3'],
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });

      const expected: string[] = mockInitialState.ids as string[];
      const action: SelectAllListItemsAction = new SelectAllListItemsAction();
      const updatedState: LocalState = reducer(mockInitialState, action);
      expect(updatedState).toEqual({
        ids: ['2', '3', '4'],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: expected,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
    });

    it('should UnselectAllListItems', () => {

      let mockInitialState: LocalState = adapter.getInitialState({
        ids: ['2', '3', '4'],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: ['2', '3', '4'],
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });

      const action: UnselectAllListItemsAction = new UnselectAllListItemsAction();
      const updatedState: LocalState = reducer(mockInitialState, action);
      expect(updatedState).toEqual({
        ids: ['2', '3', '4'],
        entities: {},
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filterReady: false,
        selectedEntityId: undefined,
        updatedEntityId: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
        serverPageInfo: undefined,
        selectedListItems: undefined,
        noRecordsFound: false,
        saveSuccess: false,
        massResponse: undefined,
      });
    });
  });
  it('should Set Filter Ready', () => {

    let mockInitialState: LocalState = adapter.getInitialState({
      ids: ['2', '3', '4'],
      entities: {},
      errors: undefined,
      listLoading: false,
      detailsLoading: false,
      pageInfo: undefined,
      filterReady: false,
      selectedEntityId: undefined,
      updatedEntityId: undefined,
      filter: undefined,
      defaultFilter: undefined,
      search: undefined,
      serverPageInfo: undefined,
      selectedListItems: undefined,
      noRecordsFound: false,
      saveSuccess: false,
      massResponse: undefined,
    });

    const action: ReadyFilterAction = new ReadyFilterAction();
    const updatedState: LocalState = reducer(mockInitialState, action);
    expect(updatedState).toEqual({
      ids: ['2', '3', '4'],
      entities: {},
      errors: undefined,
      listLoading: false,
      detailsLoading: false,
      pageInfo: undefined,
      filterReady: true,
      selectedEntityId: undefined,
      updatedEntityId: undefined,
      filter: undefined,
      defaultFilter: undefined,
      search: undefined,
      serverPageInfo: undefined,
      selectedListItems: undefined,
      noRecordsFound: false,
      saveSuccess: false,
      massResponse: undefined,
    });
  });
});

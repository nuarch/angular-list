import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { assign, concat, filter, get, includes } from 'lodash';
import * as coreActions from '../actions/core.action';
import { Entity } from '../models/entity';
import { Filter } from '../models/filter';
import { PageInfo } from '../models/page-info';
import { ServerPageInfo } from '../models/server-page-info';
import { MassUpdateDetailsReponse } from '../models/mass-update-details-response';

export interface LocalState extends EntityState<Entity> {
  listLoading: boolean;
  detailsLoading: boolean;
  errors: string[];
  pageInfo: PageInfo;
  filter: Filter;
  defaultFilter: Filter;
  filterReady: boolean;
  search: string;
  selectedEntityId: string;
  updatedEntityId: string;
  serverPageInfo: ServerPageInfo;
  selectedListItems: string[];
  noRecordsFound: boolean;
  saveSuccess: boolean;
  massResponse: MassUpdateDetailsReponse;
}

export const adapter: EntityAdapter<Entity> = createEntityAdapter<Entity>({
  selectId: (l: Entity) => l.id,
  sortComparer: false,
});

export const initialState: LocalState = adapter.getInitialState({
  errors: undefined,
  listLoading: false,
  detailsLoading: false,
  pageInfo: undefined,
  filterReady: false,
  defaultFilter: undefined,
  filter: undefined,
  search: undefined,
  selectedEntityId: undefined,
  updatedEntityId: undefined,
  serverPageInfo: undefined,
  selectedListItems: undefined,
  noRecordsFound: false,
  saveSuccess: false,
  massResponse: undefined,
});

export function reducer<T extends Entity>(state: LocalState = initialState, action: coreActions.CoreActions<T>): LocalState {
  switch (action.type) {

    case coreActions.CoreActionTypes.LoadDetails: {
      const updatedState: LocalState = get(action, 'payload.removeAllEntries') ? adapter.removeAll(state) : state;
      return assign({}, updatedState, {detailsLoading: true});
    }
    case coreActions.CoreActionTypes.ReadyFilter: {
      return assign({}, state, {filterReady: true});
    }
    case coreActions.CoreActionTypes.DeleteDetails:
    case coreActions.CoreActionTypes.UpdateDetails:
    case coreActions.CoreActionTypes.SaveDetails: {
      return assign({}, state, {detailsLoading: true, saveSuccess: false});
    }
    case coreActions.CoreActionTypes.UpdateListItems: {
      return assign({}, state, {massResponse: undefined, detailsLoading: true, saveSuccess: false});
    }
    case coreActions.CoreActionTypes.LoadList: {
      return assign({}, state, {listLoading: true, serverPageInfo: undefined, noRecordsFound: false,
        selectedEntityId: undefined, filter: get(action, 'payload.filter')});
    }
    case coreActions.CoreActionTypes.LoadListSuccess: {
      if (!action.payload) {
        return assign({}, state, {listLoading: false});
      }
      return adapter.addMany(action.payload.entity, {
        ...state,
        listLoading: false,
        pageInfo: action.payload.pageInfo,
        noRecordsFound: get(action, 'payload.entity.length', 0) <= 0,
        serverPageInfo: action.payload.serverPageInfo,
      });
    }
    case coreActions.CoreActionTypes.UpdatePageInfo: {
      return assign({}, state, {listLoading: false, pageInfo: action.payload});
    }
    case coreActions.CoreActionTypes.SetDefaultFilter: {
      return assign({}, state, {listLoading: false, defaultFilter: action.payload});
    }
    case coreActions.CoreActionTypes.SetFilter: {
      return assign({}, state, {listLoading: false, filter: action.payload});
    }
    case coreActions.CoreActionTypes.ClearFilter: {
      return assign({}, state, {listLoading: false, filter: undefined});
    }
    case coreActions.CoreActionTypes.SetSearch: {
      return assign({}, state, {listLoading: false, search: action.payload});
    }
    case coreActions.CoreActionTypes.ClearSearch: {
      return assign({}, state, {listLoading: false, search: undefined});
    }
    case coreActions.CoreActionTypes.UpdateDetailsSuccess: {
      return assign({}, state, {updatedEntityId: action.payload.updatedEntityId,
        detailsLoading: false, errors: undefined, saveSuccess: true});
    }
    case coreActions.CoreActionTypes.DeleteDetailsSuccess:
    case coreActions.CoreActionTypes.SaveDetailsSuccess: {
      return assign({}, state, {detailsLoading: false, errors: undefined, saveSuccess: true});
    }
    case coreActions.CoreActionTypes.UpdateListItemsSuccess: {
      return assign({}, state, {massResponse: action.payload.massResponse, detailsLoading: false, errors: undefined, saveSuccess: true});
    }
    case coreActions.CoreActionTypes.LoadDetailsSuccess: {
      if (!action.payload) {
        return assign({}, state, {detailsLoading: false, errors: undefined});
      }
      const updatedState: LocalState = adapter.removeOne(action.payload.id, state);
      return adapter.addOne(action.payload, {
        ...updatedState,
        selectedEntityId: action.payload.id,
        detailsLoading: false,
        errors: undefined,
      });
    }
    case coreActions.CoreActionTypes.DeleteDetailsFail:
    case coreActions.CoreActionTypes.UpdateDetailsFail:
    case coreActions.CoreActionTypes.UpdateListItemsFail:
    case coreActions.CoreActionTypes.SaveDetailsFail:
    case coreActions.CoreActionTypes.LoadDetailsFail: {
      return assign({}, state, {detailsLoading: false, errors: action.payload, saveSuccess: false});
    }
    case coreActions.CoreActionTypes.LoadListFail: {
      return assign({}, state, {listLoading: false, errors: action.payload, noRecordsFound: true});
    }
    case coreActions.CoreActionTypes.ResetDetails: {
      const updatedState: LocalState = adapter.removeAll(state);
      return assign({}, updatedState, {selectedEntityId: undefined});
    }
    case coreActions.CoreActionTypes.ResetList: {
      return adapter.removeAll(state);
    }
    case coreActions.CoreActionTypes.ResetAll: {
      const updatedState: LocalState = adapter.removeAll(state);
      return assign({}, updatedState, {
        selectedEntityId: undefined,
        errors: undefined,
        listLoading: false,
        detailsLoading: false,
        pageInfo: undefined,
        filter: undefined,
        defaultFilter: undefined,
        search: undefined,
      });
    }
    case coreActions.CoreActionTypes.SelectListItem: {
      if (!includes(state.selectedListItems, action.payload)) {
        const updatedSelectItems: string[] = state.selectedListItems ? concat(state.selectedListItems, action.payload) : [action.payload];
        return assign({}, state, {selectedListItems: updatedSelectItems});
      }

      return state;
    }
    case coreActions.CoreActionTypes.UnselectListItem: {
      if (includes(state.selectedListItems, action.payload)) {
        const updatedSelectedItems: string[] = filter(state.selectedListItems, (id: string) => id !== action.payload);
        return assign({}, state, {
          selectedListItems: updatedSelectedItems.length ? updatedSelectedItems : undefined,
        });
      }

      return state;
    }
    case coreActions.CoreActionTypes.SelectAllListItems: {
      return assign({}, state, {selectedListItems: state.ids as string[]});
    }
    case coreActions.CoreActionTypes.UnselectAllListItems: {
      return assign({}, state, {selectedListItems: undefined});
    }
    default: {
      return state;
    }
  }
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getErrors: any = (state: LocalState) => state.errors;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getPageInfo: any = (state: LocalState) => state.pageInfo;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getFilter: any = (state: LocalState) => state.filter;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getSearch: any = (state: LocalState) => state.search;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getPageLoading: any = (state: LocalState) => state.listLoading;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getDetailsLoading: any = (state: LocalState) => state.detailsLoading;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getSelectedId: any = (state: LocalState) => state.selectedEntityId;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getUpdatedEntityId: any = (state: LocalState) => state.updatedEntityId;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getServerPageInfo: any = (state: LocalState) => state.serverPageInfo;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getSelectedListItems: any = (state: LocalState) => state.selectedListItems;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getFilterReady: any = (state: LocalState) => state.filterReady;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getDefaultFilter: any = (state: LocalState) => state.defaultFilter;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getNoRecordsFound: any = (state: LocalState) => state.noRecordsFound;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getSaveSuccess: any = (state: LocalState) => state.saveSuccess;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getMassResponse: any = (state: LocalState) => state.massResponse;

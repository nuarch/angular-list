import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { InfiniteScrollingDirective } from './directives/infinite-scrolling.directive';
import { CoreEffects } from './effects/core.effects';
import { SortReferenceDataPipe } from './pipes/sort-reference-data.pipe';
import { reducers } from './reducers/root-reducer';
import { CoreService } from './services/core.service';
import { ReferenceDataService } from './services/reference-data.service';
import { SearchPipe } from './pipes/search.pipe';

@NgModule({
  declarations: [
    InfiniteScrollingDirective,
    SortReferenceDataPipe,
    SearchPipe,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule.forFeature('list', reducers),
    EffectsModule.forFeature([
      CoreEffects,
    ]),
  ],
  providers: [
    CoreService,
    ReferenceDataService,
  ],
  exports: [
    InfiniteScrollingDirective,
    SearchPipe,
  ],
})
export class CoreModule {

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public static forRoot(environment: any): ModuleWithProviders<CoreModule> {

    return {
      ngModule: CoreModule,
      providers: [
        CoreService,
        ReferenceDataService,
        {
          provide: 'environment',
          useValue: environment,
        },
      ],
    };
  }
}

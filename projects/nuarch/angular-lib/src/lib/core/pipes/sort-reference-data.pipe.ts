import { Pipe, PipeTransform } from '@angular/core';
import { GenericReferenceData } from '@smart/reference-data-management-ui-library';
import { sortBy } from 'lodash';

@Pipe({
  name: 'sortReferenceData',
})
export class SortReferenceDataPipe implements PipeTransform {
  readonly defaultSortKey: string = 'code';

  transform<T extends GenericReferenceData>(referenceData: T[], sortKey?: string): T[] {
    return sortBy(referenceData, [sortKey ? sortKey : this.defaultSortKey]);
  }

}

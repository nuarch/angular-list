import { Pipe, PipeTransform } from '@angular/core';
import { Entity } from '../models/entity';
import { filter, some, get, toLower, includes } from 'lodash';

@Pipe({
  name: 'search',
})
export class SearchPipe implements PipeTransform {

  readonly defaultSortKey: string = 'code';

  transform<T extends Entity>(entity: T[], searchString: string, searchAttributes: string[]): T[] {
    return filter(entity, (e: T) => {
      return some(searchAttributes, (attr: string) => includes(toLower(get(e, attr)), toLower(searchString)));
    });
  }

}

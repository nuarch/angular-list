import { ReferenceData } from '@smart/reference-data-management-ui-library';
import { map } from 'lodash';
import { SortReferenceDataPipe } from './sort-reference-data.pipe';

describe('SortReferenceDataPipe', () => {
  const referenceData: ReferenceData[] = [
    new ReferenceData({
      id: '1',
      correlationId: {correlation: '1'},
      label: 'Z',
    }), new ReferenceData({
      id: '3',
      correlationId: {correlation: '3'},
      label: 'B',
    }), new ReferenceData({
      id: '2',
      correlationId: {correlation: '2'},
      label: 'A',
    }),
  ];
  const sortReferenceDataPipe: SortReferenceDataPipe = new SortReferenceDataPipe();

  it('create an instance', () => {
    expect(sortReferenceDataPipe).toBeTruthy();
  });

  it('sort by label', () => {
    const sortedReferenceData: ReferenceData[] = sortReferenceDataPipe.transform(referenceData);
    expect(map(sortedReferenceData, (refData: ReferenceData) => refData.id)).toEqual(['1', '3', '2']);
  });

  it('sort by label', () => {
    const sortedReferenceData: ReferenceData[] = sortReferenceDataPipe.transform(referenceData, 'correlationId.correlation');
    expect(map(sortedReferenceData, (refData: ReferenceData) => refData.id)).toEqual(['1', '2', '3']);
  });

});

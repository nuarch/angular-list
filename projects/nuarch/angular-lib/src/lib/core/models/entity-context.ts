import { Entity } from './entity';

export interface EntityContext {
  entity: Entity;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  formValue: any;
  mode: number;
}

export interface ColumnModel {
  label: string;
  value: string;
  sortable: boolean;
  flex: string;
  type?: string;
  referenceData?: string;
  defaultSort?: boolean;
  defaultDirection?: string;
  multiLine?: number;
  displayedValue?: Function;
}

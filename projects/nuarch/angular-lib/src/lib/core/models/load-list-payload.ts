import { Filter } from './filter';
import { PageInfo } from './page-info';
import { loadMethod } from '../constants/load-method';

export class LoadListPayload {
  pageInfo: PageInfo;
  searchString: string;
  filter: Filter;
  url: string;
  additionalOptions: {[key: string]: string}[];
  method?: loadMethod;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  body?: any;
  override?: boolean;
  overrideParams?: Filter;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(data?: any) {
    this.pageInfo = data.pageInfo;
    this.searchString = data.searchString;
    this.filter = data.filter;
    this.additionalOptions = data.additionalOptions;
    this.url = data.url;
    this.method = data.method;
    this.body = data.body;
    this.override = data.override;
    this.overrideParams = data.overrideParams;
  }
}

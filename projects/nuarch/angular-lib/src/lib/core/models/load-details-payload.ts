export interface LoadDetailsPayload {
  id?: string;
  url?: string;
  urlOverride?: boolean;
  removeAllEntries?: boolean;
}

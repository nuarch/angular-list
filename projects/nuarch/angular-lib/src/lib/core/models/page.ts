import { Entity } from './entity';
import { PageInfo } from './page-info';
import { ServerPageInfo } from './server-page-info';

export interface Page<T extends Entity> {
  pageInfo: PageInfo;
  entity: T[];
  serverPageInfo?: ServerPageInfo;
}

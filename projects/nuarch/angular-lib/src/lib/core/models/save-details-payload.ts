export interface SaveDetailsPayload<T> {
  url: string;
  entity: T;
  redirect?: boolean;
  redirectUrl?: string;
  message?: string;
}

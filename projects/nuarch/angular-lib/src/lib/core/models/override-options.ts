import { Filter } from './filter';

export interface OverrideOptions {
  override?: boolean;
  overrideParams?: Filter;
}

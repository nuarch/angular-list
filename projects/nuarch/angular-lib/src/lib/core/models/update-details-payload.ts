import { SaveDetailsPayload } from './save-details-payload';

export interface UpdateDetailsPayload<T> extends SaveDetailsPayload<T> {
  id: string;
  urlOverride?: boolean;
}

import { GenericReferenceData } from '@smart/reference-data-management-ui-library';

export interface ReferenceDataCollection {
  [type: string]: { [correlationId: string]: GenericReferenceData };
}

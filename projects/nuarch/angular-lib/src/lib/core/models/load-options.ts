import { loadMethod } from '../constants/load-method';

export interface LoadOptions {
  resetToDefaultMode?: boolean;
  method?: loadMethod;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  body?: any;
}

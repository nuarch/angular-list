import { GenericReferenceData } from '@smart/reference-data-management-ui-library';
import { ColumnModel } from './column-model';

export interface ViewModel {
  columns: ColumnModel[];
  url: string;
  showAdd: boolean;
  searchPlaceholder?: string;
  sectionName?: string;
  addButtonText?: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  referenceData?: { [type: string]: new(...args: any[]) => GenericReferenceData };
}

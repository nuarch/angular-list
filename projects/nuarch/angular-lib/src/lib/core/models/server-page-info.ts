export interface ServerPageInfo {
  first?: boolean;
  last?: boolean;
  number?: number;
  empty?: boolean;
  numberOfElements?: boolean;
  totalPages?: number;
  totalElements?: number;
}

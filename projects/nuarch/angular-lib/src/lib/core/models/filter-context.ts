import { FormGroup } from '@angular/forms';
import { Entity } from './entity';

export interface FilterContext {
  entity?: Entity;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  formValue?: any;
  form?: FormGroup;
}

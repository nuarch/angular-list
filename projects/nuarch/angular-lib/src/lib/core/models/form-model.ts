import { GenericReferenceData } from '@smart/reference-data-management-ui-library';
import { INuDynamicElementConfig } from '../../dynamic-forms/services/dynamic-forms.service';

export interface FormModel {
  addTemplate?: INuDynamicElementConfig[];
  editTemplate?: INuDynamicElementConfig[];
  readTemplate?: INuDynamicElementConfig[];
  sectionName: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  referenceData?: { [type: string]: new(...args: any[]) => GenericReferenceData };
  url?: string;
}

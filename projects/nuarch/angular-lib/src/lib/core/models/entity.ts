export interface Entity {
  id?: string;
  createdDate?: string;
  createdBy?: string;
  version?: string;
}

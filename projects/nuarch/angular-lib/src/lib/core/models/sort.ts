export const defaultSortBy: string = 'id';
export const defaultSortOrder: string = 'asc';

export class Sort {
  active: string = defaultSortBy;
  direction: string = defaultSortOrder;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(data?: any) {
    if (data) {
      this.active = data.sort || data.active;
      this.direction = data.dir || data.direction;
    }
  }

}

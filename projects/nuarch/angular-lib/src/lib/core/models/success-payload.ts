import { MassUpdateDetailsReponse } from './mass-update-details-response';

export const defaultSuccessMessage: string = 'Successfully Saved!';

export interface SuccessPayload {
  redirect: boolean;
  redirectUrl?: string;
  message: string;
  updatedEntityId?: string;
  massResponse?: MassUpdateDetailsReponse;
}

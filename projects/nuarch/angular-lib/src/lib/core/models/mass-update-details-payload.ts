export interface MassUpdateDetailsPayload<T> {
  url: string;
  entities: T[];
  message?: string;
  redirect?: boolean;
  redirectUrl?: string;
}

import { LoadDetailsPayload } from './load-details-payload';

export interface DeleteDetailsPayload extends LoadDetailsPayload {
}

import { Directive, Injectable, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

@Directive()
export abstract class AbstractComponent implements OnDestroy {

  protected componentDestroyed: Subject<void> = new Subject<void>();

  abstract afterOnDestroy(): void;

  ngOnDestroy(): void {
    this.componentDestroyed.next();
    this.componentDestroyed.complete();
    this.componentDestroyed.unsubscribe();
    this.afterOnDestroy();
  }
}

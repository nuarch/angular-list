import { ContentChild, Directive, EventEmitter, Injectable, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  assign,
  defaults,
  filter as _filter,
  find,
  flatMap,
  get,
  head,
  invoke,
  isEqual,
  isNil,
  some,
  split,
  values,
} from 'lodash';
import { combineLatest, Observable } from 'rxjs';
import { debounceTime, skip, take, takeUntil, filter, distinctUntilChanged } from 'rxjs/operators';
import { addUrl } from '../../constants/add-url';
import { searchDebounceTime } from '../../constants/debounce-time';
import { defaultPageInfo, defaultSortDirection } from '../../constants/default-page-info';
import { defaultPageNumber, defaultPageSize, defaultPageSizeOptions } from '../../constants/default-page-size';
import { editUrl } from '../../constants/edit-url';
import { Entity } from '../../models/entity';
import { Filter } from '../../models/filter';
import { FormModel } from '../../models/form-model';
import { LoadListPayload } from '../../models/load-list-payload';
import { Page } from '../../models/page';
import { Sort } from '../../models/sort';
import { PageInfo } from '../../models/page-info';
import { ReferenceData } from '../../models/reference-data';
import { ViewModel } from '../../models/view-model';
import { ReferenceDataService } from '../../services/reference-data.service';
import { AbstractComponent } from '../abstract-component/abstract-component';
import { ColumnModel } from '../../models/column-model';
import { PageEvent } from '@angular/material/paginator';
import { CoreService } from '../../services/core.service';
import { LoadOptions } from '../../models/load-options';
import { loadMethod } from '../../constants/load-method';
import { FilterComponent } from '../../../filter/filter.component';
import { OverrideOptions } from '../../models/override-options';

@Directive()
export abstract class AbstractList<T extends Entity> extends AbstractComponent implements OnInit {

  private _pageInfo: PageInfo = defaultPageInfo;
  private _searchString: string;
  private _filter: Filter;
  private _defaultFilter: Filter;

  @Input() showSearchControl: boolean = true;
  @Input() showSortControl: boolean = true;
  @Input() buttonTriggersSearch: boolean = false;
  @Input() loadDataOnInit: boolean = true;
  @Input() openDetails: boolean = true;
  @Input() viewModel: ViewModel;
  @Input() filterModel: FormModel;
  @Input() filterGroup: FormGroup;
  @Input() searchPipeAttributes: string[];
  @Input() itemSize: number;
  @Input() itemWidth: number;
  @Input() pagination: boolean = true;
  @Input() showAdd: boolean = true;
  @Input() waitForDefaultFilter: boolean = false;
  @Input() additionalOptions: {[key: string]: string}[];
  rootUrl: string;

  readonly pageSizeOptions: number[] = defaultPageSizeOptions;
  readonly defaultPageSize: number = defaultPageSize;

  referenceDataSeperator: string = '.';
  page: Page<T>;
  pageLoading: Observable<boolean>;
  listLoading: Observable<boolean>;
  searchControl: FormControl = new FormControl();
  referenceData: Observable<{ [type: string]: ReferenceData[] }>;
  readonly addModeUrl: string = addUrl;
  showAdvanceFilters: boolean = false;
  readonly descendingSortDirection: string = defaultSortDirection;
  readonly ascendingSortDirection: string = 'asc';
  sortBy: FormControl = new FormControl();
  cardSortColumns: { active: string, direction: string, label: string }[];
  noRecordsFound: Observable<boolean>;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @ViewChild('searchComp', {static: false}) searchComponent: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @ContentChild('headerTemplate', {static: false}) headerTemplate: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @ContentChild('cardTemplate', {static: false}) cardTemplate: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @ContentChild('bodyTemplate', {static: false}) bodyTemplate: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @ContentChild('rowTemplate', {static: false}) rowTemplate: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @ContentChild('filterTemplate', {static: false}) filterTemplate: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @ContentChild(FilterComponent, {static: false}) filterComponent: FilterComponent;
  @Output() select: EventEmitter<string> = new EventEmitter<string>();

  protected constructor(protected router: Router,
                        protected url: ActivatedRoute,
                        protected coreService: CoreService<T>,
                        protected referenceDataService: ReferenceDataService) {
    super();
    this.noRecordsFound = this.coreService.noRecordsFound;
  }

  abstract get pageInfo(): Observable<PageInfo>;

  abstract get searchString(): Observable<string>;

  abstract get filter(): Observable<Filter>;

  abstract get defaultFilter(): Observable<Filter>;

  get hasFilter(): boolean {
    return some(values(get(this.page, 'pageInfo.filter')), (value: string) => !isNil(value));
  }

  abstract get entityPage(): Observable<Page<T>>;

  abstract get loading(): Observable<boolean>;

  ngOnInit(): void {
    if (!!this.viewModel) {
      this.cardSortColumns = flatMap(
        _filter(this.viewModel.columns, (column: ColumnModel) => column.sortable), (column: ColumnModel) => {
          return [
            {active: column.value, direction: this.ascendingSortDirection, label: column.label},
            {active: column.value, direction: this.descendingSortDirection, label: column.label},
          ];
        });
      if (!!this.viewModel.referenceData) {
        this.referenceData = this.referenceDataService.getReferenceDataArray(this.viewModel.referenceData);
      }
    }

    this.filter
      .pipe(
        takeUntil(this.componentDestroyed),
        distinctUntilChanged(isEqual),
      )
      .subscribe((f: Filter) => {
        this._filter = f;
      });
    this.defaultFilter
      .pipe(
        takeUntil(this.componentDestroyed),
        distinctUntilChanged(isEqual),
        take(2),
      )
      .subscribe((f: Filter) => {
        this._defaultFilter = f;
      });
    this.pageInfo
      .pipe(
        distinctUntilChanged(isEqual),
        takeUntil(this.componentDestroyed),
      )
      .subscribe((pageInfo: PageInfo) => {
        this._pageInfo = pageInfo;
      });
    this.pageInfo
      .pipe(
        distinctUntilChanged(isEqual),
        take(1),
      )
      .subscribe((pageInfo: PageInfo) => {
        this._pageInfo = pageInfo;
        this.setSort();
      });

    this.pageLoading = this.loading.pipe(takeUntil(this.componentDestroyed), take(2));
    this.listLoading = this.loading.pipe(takeUntil(this.componentDestroyed), skip(2));
    this.entityPage.pipe(takeUntil(this.componentDestroyed)).subscribe((page: Page<T>) => {
      this.page = page;
    });

    this.searchString
      .pipe(
        distinctUntilChanged(isEqual),
        takeUntil(this.componentDestroyed),
      )
      .subscribe((searchTerm: string) => {
        this._searchString = searchTerm;
      });

    if (!this.buttonTriggersSearch) {
      this.searchControl.valueChanges.pipe(debounceTime(searchDebounceTime), takeUntil(this.componentDestroyed))
        .subscribe((searchTerm: string) => {
          this.search(searchTerm);
        });
    }

    this.dispatchResetList();

    if (this.loadDataOnInit && !this.waitForDefaultFilter) {
      this.loadData(new LoadListPayload(assign({}, {
        pageInfo: this.defaultInfo,
        filter: this._filter,
        searchString: this._searchString,
        url: this.viewModel.url,
      })));
    }

    if (this.waitForDefaultFilter) {
      combineLatest([
          this.coreService.filterReady,
          this.coreService.filter,
        ],
      ).pipe(takeUntil(this.componentDestroyed),
        filter(([filterReady, f]: [boolean, Filter]) => !!filterReady && !!f),
        take(1),
      ).subscribe(([filterReady, f]: [boolean, Filter]) => {
        this.loadData(new LoadListPayload(assign({}, {
          pageInfo: this.defaultInfo,
          filter: f,
          searchString: this._searchString,
          url: this.viewModel.url,
        })));
      });
    }
  }

  get invalidFilter(): boolean {
    return this.filterComponent && (!this.filterComponent.isValid || !this._filter);
  }

  setSort(): void {
    const defaultSortColumn: ColumnModel = find(this.viewModel.columns, (column: ColumnModel) => column.defaultSort);
    this.sortBy.setValue(find(this.cardSortColumns, (c: { active: string, direction: string, label: string }) => {
      return c.active === get(this._pageInfo, 'sort.active', get(defaultSortColumn, 'value')) &&
        c.direction === get(this._pageInfo, 'sort.direction', get(defaultSortColumn, 'defaultDirection', defaultSortDirection));
    }));
    this.sortBy.valueChanges.pipe(takeUntil(this.componentDestroyed)).subscribe((sort: { active: string, direction: string, label: string }) => {
      if (get(this._pageInfo, 'sort.direction') !== sort.direction || get(this._pageInfo, 'sort.active') !== sort.active) {
        this.sort(new Sort(sort));
      }
    });
  }

  get defaultInfo(): PageInfo {
    if (this._pageInfo) {
      return this._pageInfo;
    }
    const defaultSortColumn: ColumnModel =
      find(get(this.viewModel, 'columns'), (column: ColumnModel) => column.defaultSort);
    if (!!defaultSortColumn) {
      return assign({}, defaultPageInfo, {
        sort: assign({},
          this.formatReferenceSort({active: defaultSortColumn.value, direction: defaultSortColumn.defaultDirection || defaultSortDirection})),
      });
    }
    return defaultPageInfo;
  }

  sort(data: Sort): void {
    this.dispatchResetList();
    this.loadData(new LoadListPayload(assign({}, {
      pageInfo: assign({}, this._pageInfo, {page: defaultPageNumber, sort: assign({}, this.formatReferenceSort(data))}),
      filter: this._filter,
      searchString: this._searchString,
      url: this.viewModel.url,
    })));
  }

  formatReferenceSort(sortData: Sort): Sort {
    const isReferenceDataColumn: boolean =
      some(get(this.viewModel, 'columns'), (column: ColumnModel) => column.value === sortData.active && column.referenceData);
    return assign({}, sortData, {
      active: isReferenceDataColumn ?
        head(split(sortData.active, this.referenceDataSeperator)) : sortData.active,
    });
  }

  search(searchTerm: string): void {
    if (!this.searchPipeAttributes) {
      this.dispatchResetList();
      this.loadData(new LoadListPayload(assign({}, {
        pageInfo: assign({}, this._pageInfo, {page: defaultPageNumber}),
        filter: this._filter,
        searchString: searchTerm,
        url: this.viewModel.url,
      })));
    } else {
      this.coreService.dispatchSetSearch(searchTerm);
    }
  }

  applyFilter(f: Filter, options?: LoadOptions, overrideOptions?: OverrideOptions): void {
    this.showAdvanceFilters = false;
    const pageInfo: PageInfo = this._pageInfo ? {...this._pageInfo, page: defaultPageNumber} : {...this.defaultInfo, page: defaultPageNumber};
    this.dispatchResetList();
    this.loadData(new LoadListPayload(assign({}, {
      pageInfo: pageInfo,
      filter: defaults({}, f, get(options, 'resetToDefaultMode', false) ? this._defaultFilter : {}),
      searchString: this._searchString,
      url: this.viewModel.url,
      body: get(options, 'body'),
      method: get(options, 'method', loadMethod.get),
      override: get(overrideOptions, 'override'),
      overrideParams: get(overrideOptions, 'overrideParams'),
    })));
  }

  reloadList(): void {
    this.dispatchResetList();
    this.loadData(new LoadListPayload(assign({}, {
      pageInfo: this._pageInfo,
      filter: this._filter,
      searchString: this._searchString,
      url: this.viewModel.url,
    })));
  }

  cancelFilter(resetToDefaultMode?: boolean): void {
    this.showAdvanceFilters = false;
    this.dispatchResetList();
    invoke(this.filterTemplate, 'dynamicFormsComponent.dynamicFormGroup.reset');
    this.loadData(new LoadListPayload(assign({}, {
      pageInfo: this._pageInfo,
      filter: defaults({}, resetToDefaultMode ? this._defaultFilter : {}),
      searchString: this._searchString,
      url: this.viewModel.url,
    })));
  }

  paginate(event: PageEvent): void {
    this.dispatchResetList();
    this.loadData(new LoadListPayload(assign({}, {
      pageInfo: assign({}, this._pageInfo, {page: event.pageIndex, size: event.pageSize}),
      filter: this._filter,
      searchString: this._searchString,
      url: this.viewModel.url,
    })));
  }

  loadMore(): void {
    this.loadData(new LoadListPayload(assign({}, {
      pageInfo: this._pageInfo ? assign({}, this._pageInfo, {page: ++this._pageInfo.page}) : this.defaultInfo,
      filter: this._filter,
      searchString: this._searchString,
      url: this.viewModel.url,
    })));
  }

  loadData(payload: LoadListPayload): void {
    this.dispatchLoadList(assign({}, payload, {additionalOptions: this.additionalOptions}));
  }

  navigate(id: string): void {
    if (id === addUrl) {
      this.router.navigate([id], {relativeTo: this.url});
    } else if (this.openDetails) {
      this.router.navigate([id, editUrl], {relativeTo: this.url});
    } else {
      this.select.emit(id);
    }
  }

  clearSearch(): void {
    this.dispatchResetList();
    this.searchControl.setValue(undefined, {emitEvent: true});
  }

  abstract dispatchLoadList(payload: LoadListPayload): void;

  abstract dispatchResetList(): void;

  abstract dispatchSetFilter(filter: Filter): void;

  abstract dispatchSetSearch(searchString: string): void;

  abstract dispatchClearSearch(): void;

  afterOnDestroy(): void {
    // afterOnDestroy
  }
}

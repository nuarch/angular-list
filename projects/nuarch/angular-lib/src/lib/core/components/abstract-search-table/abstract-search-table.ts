import { AfterViewInit, Directive, EventEmitter, Injectable, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort, Sort } from '@angular/material/sort';
import { find, get, isEqual, split, head, toLower, includes } from 'lodash';
import moment from 'moment';
import { ColumnModel } from '../../models/column-model';
import { Page } from '../../models/page';
import { ReferenceData } from '../../models/reference-data';
import { Entity } from '../../models/entity';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AbstractComponent } from '../../../core/components/abstract-component/abstract-component';

@Directive()
export abstract class AbstractSearchTable extends AbstractComponent implements AfterViewInit, OnInit {

  @Output() sort: EventEmitter<Sort> = new EventEmitter<Sort>();
  @Output() load: EventEmitter<void> = new EventEmitter<void>();
  @Output() select: EventEmitter<string> = new EventEmitter<string>();
  @Input() referenceData: { [type: string]: ReferenceData[] };
  @Input() itemSize: number;
  @Input() itemWidth: number = 100;
  @Input() columns: ColumnModel[];
  @Input() page: Observable<Page<Entity>>;
  readonly dateFormat: string = 'L';
  entityPage: Page<Entity>;

  get: Function = get;

  readonly referenceDataDisplayAttr: string = 'description';

  readonly referenceDataSeperator: string = '.';

  @ViewChild(MatSort, {static: false}) matSort: MatSort;

  ngOnInit(): void {
    this.page.pipe(takeUntil(this.componentDestroyed)).subscribe((entityPage: Page<Entity>) => {
      this.entityPage = entityPage;
    });
  }

  ngAfterViewInit(): void {
    if (!!this.matSort) {
      this.matSort.disableClear = true;
    }
  }

  get active(): string {
    const activeSort: string = get(this.entityPage, 'pageInfo.sort.active');
    const column: ColumnModel = find(this.columns, (c: ColumnModel) => c.value === activeSort ||
      head(split(c.value, this.referenceDataSeperator)) === activeSort );
    return get(column, 'value');
  }

  sortData(sort: Sort): void {
    this.sort.emit(sort);
  }

  loadData(): void {
    this.load.emit();
  }

  getCellValue(row: Entity, column: ColumnModel): string {
    const value: string = get(row, column.value);

    if (column.referenceData) {
      const displayedValue: Function = get(column, 'displayedValue');
      const columnReferenceData: ReferenceData = find(get(this.referenceData, column.referenceData), (refData: ReferenceData) => {
        return isEqual(get(refData, 'correlationId'), value) || isEqual(get(refData, 'correlation'), value);
      });

      if (displayedValue) {
        return displayedValue(columnReferenceData);
      }

      return get(columnReferenceData, this.referenceDataDisplayAttr);
    }

    if (column.type === 'date') {
      return moment(value).format(this.dateFormat);
    }

    if (column.multiLine) {
      const end: number = column.multiLine > value.length ? value.length : column.multiLine;
      return `${value.substring(0, end)}...`;
    }

    return value;
  }

  selectRecord(id: string): void {
    this.select.emit(id);
  }

  afterOnDestroy(): void {
    // afterOnDestroy
  }
}

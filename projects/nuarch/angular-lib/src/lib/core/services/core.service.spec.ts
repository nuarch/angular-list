import { HttpClientTestingModule } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { anything, capture, instance, mock, reset, verify } from 'ts-mockito';

import { CoreActionTypes } from '../actions/core.action';
import { Entity } from '../models/entity';
import * as fromList from '../reducers/root-reducer';
import { CoreService } from './core.service';

describe('CoreService', () => {
  const mockStore: Store<fromList.State> = mock<Store<fromList.State>>(Store);
  const store: Store<fromList.State> = instance(mockStore);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        CoreService,
        {
          provide: Store,
          useValue: store,
        },
        {
          provide: 'environment',
          useValue: {},
        },
      ],
    });
    reset(mockStore);
  });

  it('should be created', inject([CoreService], (service: CoreService<Entity>) => {
    expect(service).toBeTruthy();
  }));

  it('should dispatchLoadList', inject([CoreService], (lawsListService: CoreService<Entity>) => {
    lawsListService.dispatchLoadList(undefined);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const [firstArg]: any = capture(mockStore.dispatch).first();
    verify(mockStore.dispatch(anything())).once();
    expect(firstArg).toBeTruthy();
    expect(firstArg.type).toBe(CoreActionTypes.LoadList);
  }));

  it('should dispatchResetList', inject([CoreService], (lawsListService: CoreService<Entity>) => {
    lawsListService.dispatchResetList();
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const [firstArg]: any = capture(mockStore.dispatch).first();
    verify(mockStore.dispatch(anything())).once();
    expect(firstArg).toBeTruthy();
    expect(firstArg.type).toBe(CoreActionTypes.ResetList);
  }));

});

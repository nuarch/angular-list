import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, inject, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { Action, GenericReferenceData, ReferenceData, ReferenceDataManagementUiLibraryService } from '@smart/reference-data-management-ui-library';
import { of } from 'rxjs';

import { ReferenceDataService } from './reference-data.service';

describe('Reference Data Service', () => {
  let referenceDataManagementUiLibraryService: ReferenceDataManagementUiLibraryService<GenericReferenceData>;

  beforeEach(
    () => {
      TestBed.configureTestingModule(
        {
          imports: [HttpClientTestingModule],
          providers: [
            ReferenceDataService,
            ReferenceDataManagementUiLibraryService,
            {
              provide: Store,
              useValue: {},
            },
          ],
        },
      );
      referenceDataManagementUiLibraryService = TestBed.get(ReferenceDataManagementUiLibraryService);
    },
  );

  it('should be created', inject([ReferenceDataService], (referenceDataService: ReferenceDataService) => {
    expect(referenceDataService).toBeTruthy();
  }));

  it('getReferenceData - should return ReferenceDataSelection', async(
    inject([ReferenceDataService], (referenceDataService: ReferenceDataService) => {
      spyOnProperty(referenceDataManagementUiLibraryService, 'referenceData', 'get')
        .and.returnValues(of({
        'actions': {
          'test': new Action(
            {
              correlationId: {'correlation': 'TEST'},
              description: 'test',
              code: 'TEST',
            }),
        },
      }));
      referenceDataService.getReferenceData('actions').subscribe((data: ReferenceData[]) => {
        expect(data[0].correlationId.correlation).toBe('TEST');
        expect(data[0].code).toBe('TEST');
      });
    })));

});

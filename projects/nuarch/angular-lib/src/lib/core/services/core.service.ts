import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { each, get, includes, isDate, isUndefined, keys, omitBy, toString, head } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  ClearFilterAction,
  ClearSearchAction,
  DeleteDetailsAction,
  LoadDetailsAction,
  LoadDetailsSuccessAction,
  LoadListAction,
  ReadyFilterAction,
  ResetAllAction,
  ResetDetailsAction,
  ResetListAction,
  SaveDetailsAction,
  SelectAllListItemsAction,
  SelectListItemAction,
  SetDefaultFilterAction,
  SetFilterAction,
  SetSearchAction,
  UnselectAllListItemsAction,
  UnselectListItemAction,
  UpdateDetailsAction,
  UpdateListItemsAction,
  UpdatePageInfoAction,
} from '../actions/core.action';
import { Entity } from '../models/entity';
import { Filter } from '../models/filter';
import { LoadListPayload } from '../models/load-list-payload';
import { LoadDetailsPayload } from '../models/load-details-payload';
import { Page } from '../models/page';
import { PageInfo } from '../models/page-info';
import { SaveDetailsPayload } from '../models/save-details-payload';
import { UpdateDetailsPayload } from '../models/update-details-payload';
import { DeleteDetailsPayload } from '../models/delete-details-payload';
import * as fromCore from '../reducers/root-reducer';
import { MassUpdateDetailsPayload } from '../models/mass-update-details-payload';
import { requestDateFormat } from '../constants/request-date-format';
import moment from 'moment';
import { loadMethod } from '../constants/load-method';
import { MassUpdateDetailsReponse } from '../models/mass-update-details-response';

@Injectable()
export class CoreService<T extends Entity> {

  // page
  listLoading: Observable<boolean>;
  detailsLoading: Observable<boolean>;
  entities: Observable<Entity[]>;
  selectedEntityId: Observable<string>;
  updatedEntityId: Observable<string>;
  selectedEntity: Observable<Entity>;
  pageInfo: Observable<PageInfo>;
  filter: Observable<Filter>;
  defaultFilter: Observable<Filter>;
  page: Observable<Page<T>>;
  searchString: Observable<string>;
  errors: Observable<string[]>;
  selectedListItems: Observable<string[]>;
  filterReady: Observable<boolean>;
  noRecordsFound: Observable<boolean>;
  saveSuccess: Observable<boolean>;
  massResponse: Observable<MassUpdateDetailsReponse>;

  readonly filterAll: string = 'ALL';

  constructor(private http: HttpClient,
              private store: Store<fromCore.State>,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              @Inject('environment') private environment: any) {

    // page
    this.listLoading = store.pipe(select(fromCore.getListLoading));
    this.detailsLoading = store.pipe(select(fromCore.getDetailsLoading));
    this.entities = store.pipe(select(fromCore.getAllList));
    this.selectedEntityId = store.pipe(select(fromCore.getSelectedEntityId));
    this.updatedEntityId = store.pipe(select(fromCore.getUpdatedEntityId));
    this.selectedEntity = store.pipe(select(fromCore.getSelectedEntity));
    this.pageInfo = store.pipe(select(fromCore.getPageInfo));
    this.filterReady = store.pipe(select(fromCore.getFilterReady));
    this.defaultFilter = store.pipe(select(fromCore.getDefaultFilter));
    this.filter = store.pipe(select(fromCore.getFilter));
    this.searchString = store.pipe(select(fromCore.getSearch));
    this.page = store.pipe(select(fromCore.getPage));
    this.errors = store.pipe(select(fromCore.getErrors));
    this.selectedListItems = store.pipe(select(fromCore.getSelectedListItems));
    this.noRecordsFound = store.pipe(select(fromCore.getNoRecordsFound));
    this.saveSuccess = store.pipe(select(fromCore.getSaveSuccess));
    this.massResponse = store.pipe(select(fromCore.getMassResponse));
  }

  dispatchSelectListItem(id: string): void {
    this.store.dispatch(new SelectListItemAction(id));
  }

  dispatchUnselectListItem(id: string): void {
    this.store.dispatch(new UnselectListItemAction(id));
  }

  dispatchSelectAllListItems(): void {
    this.store.dispatch(new SelectAllListItemsAction());
  }

  dispatchUnselectAllListItems(): void {
    this.store.dispatch(new UnselectAllListItemsAction());
  }

  dispatchResetList(): void {
    this.store.dispatch(new ResetListAction());
  }

  dispatchLoadList(payload: LoadListPayload): void {
    this.store.dispatch(new LoadListAction(payload));
  }

  dispatchLoadDetails(payload: LoadDetailsPayload): void {
    this.store.dispatch(new LoadDetailsAction(payload));
  }

  dispatchLoadDetailsSuccess(entity: Entity): void {
    this.store.dispatch(new LoadDetailsSuccessAction(entity));
  }

  dispatchSaveDetails(payload: SaveDetailsPayload<T>): void {
    this.store.dispatch(new SaveDetailsAction(payload));
  }

  dispatchUpdateDetails(payload: UpdateDetailsPayload<T>): void {
    this.store.dispatch(new UpdateDetailsAction(payload));
  }

  dispatchMassUpdateDetails(payload: MassUpdateDetailsPayload<T>): void {
    this.store.dispatch(new UpdateListItemsAction(payload));
  }

  dispatchDeleteDetails(payload: DeleteDetailsPayload): void {
    this.store.dispatch(new DeleteDetailsAction(payload));
  }

  dispatchUpdatePageInfo(pageInfo: PageInfo): void {
    this.store.dispatch(new UpdatePageInfoAction(pageInfo));
  }

  dispatchSetDefaultFilter(filter: Filter): void {
    this.store.dispatch(new SetDefaultFilterAction(filter));
  }

  dispatchFilterReady(): void {
    this.store.dispatch(new ReadyFilterAction());
  }

  dispatchSetFilter(filter: Filter): void {
    this.store.dispatch(new SetFilterAction(filter));
  }

  dispatchSetSearch(searchString: string): void {
    this.store.dispatch(new SetSearchAction(searchString));
  }

  dispatchClearSearch(): void {
    this.store.dispatch(new ClearSearchAction());
  }

  dispatchClearFilter(): void {
    this.store.dispatch(new ClearFilterAction());
  }

  dispatchResetDetails(): void {
    this.store.dispatch(new ResetDetailsAction());
  }

  dispatchResetAll(): void {
    this.store.dispatch(new ResetAllAction());
  }

  loadListParams(payload: LoadListPayload): HttpParams {
    let params: HttpParams = new HttpParams();
    if (!!payload) {
      if (!!payload.pageInfo) {
        if (!!payload.pageInfo.size) {
          params = params.set('page', toString(payload.pageInfo.page)).set('size', toString(payload.pageInfo.size));
        }
        if (!!payload.pageInfo.sort) {
          params = params.set('sort', `${payload.pageInfo.sort.active},${payload.pageInfo.sort.direction}`);
        }
      }

      if (!!payload.searchString) {
        params = params.set('search', payload.searchString);
      }

      if (!!payload.override) {

        each(keys(omitBy(payload.overrideParams, isUndefined)), (key: string) => {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          const val: any = get(payload.filter, key);
          if (!!val && !includes(val, this.filterAll) && !includes(val.correlation, this.filterAll)) {
            params = params.set(key, this.getFilterValue(val));
          }
        });

      } else {

        if (!!payload.additionalOptions) {
          each(payload.additionalOptions, (value: { [key: string]: string }) => {
            const key: string = head(keys(value));
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const val: any = get(value, key);
            if (!!val && !includes(val, this.filterAll) && !includes(val.correlation, this.filterAll)) {
              params = params.set(key, this.getFilterValue(val));
            }
          });
        }

        each(keys(omitBy(payload.filter, isUndefined)), (key: string) => {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          const val: any = get(payload.filter, key);
          if (!!val && !includes(val, this.filterAll) && !includes(val.correlation, this.filterAll)) {
            params = params.set(key, this.getFilterValue(val));
          }
        });
      }

    }
    return params;
  }

  loadList(payload: LoadListPayload): Observable<Page<T>> {
    const params: HttpParams = this.loadListParams(payload);

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let res: Observable<any>;

    if (payload.method === loadMethod.post) {
      res = this.http.post(payload.url, payload.body, {params});
    } else {
      res = this.http.get(payload.url, {params});
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return res.pipe(map((data: any) => {
      return {
        entity: data.content || data,
        // needs to improve BE model so we dont have to do this
        pageInfo: payload.pageInfo,
        // back-end model
        serverPageInfo: {
          first: data.first,
          last: data.last,
          number: data.number,
          empty: data.empty,
          numberOfElements: data.numberOfElements,
          totalPages: data.totalPages,
          totalElements: data.totalElements,
        },
      };
    }));
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getFilterValue(value: any): string {
    if (moment.isMoment(value) || isDate(value)) {
      return moment(value).format(requestDateFormat);
    }
    const referenceData: string = get(value, 'correlationId.correlation');
    if (!!referenceData) {
      return referenceData;
    }

    const referenceDataId: string = get(value, 'correlation');
    if (!!referenceDataId) {
      return referenceDataId;
    }

    return value;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  loadDetails(payload: LoadDetailsPayload): Observable<any> {
    const url: string = payload.urlOverride ? `${payload.url}` : `${payload.url}/${payload.id}`;
    return this.http.get(url);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  saveDetails(payload: SaveDetailsPayload<T>): Observable<any> {
    return this.http.post(`${payload.url}`, payload.entity);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  updateDetails(payload: UpdateDetailsPayload<T>): Observable<any> {
    const url: string = payload.urlOverride ? `${payload.url}` : `${payload.url}/${payload.id}`;
    return this.http.put(url, payload.entity);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  deleteDetails(payload: DeleteDetailsPayload): Observable<any> {
    return this.http.delete(`${payload.url}/${payload.id}`);
  }

  massUpdateDetails(payload: MassUpdateDetailsPayload<T>): Observable<unknown> {
    return this.http.put(payload.url, payload.entities);
  }
}

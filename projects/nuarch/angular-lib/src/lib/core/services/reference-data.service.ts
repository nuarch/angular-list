import { Injectable } from '@angular/core';
import {
  GenericReferenceData,
  LoadReferenceDataItemPayload,
  ReferenceDataManagementUiLibraryService,
  TypedReferenceData,
} from '@smart/reference-data-management-ui-library';
import { every, get, keyBy, keys, map as _map, mapValues, toArray } from 'lodash';
import moment from 'moment'
import { combineLatest, Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ReferenceData } from '../models/reference-data';
import { ReferenceDataCollection } from '../models/reference-data-collection';
import { SortReferenceDataPipe } from '../pipes/sort-reference-data.pipe';

@Injectable()
export class ReferenceDataService {

  readonly referenceDataRoute: string = '../refdata/api/items/date/';
  referenceDataSortingPipe: SortReferenceDataPipe = new SortReferenceDataPipe();

  constructor(private referenceDataManagementUiLibraryService: ReferenceDataManagementUiLibraryService<GenericReferenceData>) {
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public dispatchLoadReferenceData(metadata: { [type: string]: new(...args: any[]) => GenericReferenceData },
                                   date?: moment.Moment): void {
    this.referenceDataManagementUiLibraryService.dispatchLoadReferenceData(
      _map(keys(metadata), (type: string) => {
        let url: string = `${this.referenceDataRoute}${type}`;
        return new LoadReferenceDataItemPayload(url, type, get(metadata, type), (date ? date : moment()).format(moment.HTML5_FMT.DATE));
      }),
    );
  }

  getReferenceData<T extends GenericReferenceData>(type: string, sortKey?: string): Observable<T[]> {
    return this.referenceDataManagementUiLibraryService.referenceData.pipe(
      map((data: { [type: string]: { [correlationId: string]: T } }) =>
        this.referenceDataSortingPipe.transform(toArray(get(data, type)), sortKey)),
    );
  }

  getCollection<T extends GenericReferenceData>(type: string, sortKey?: string): Observable<{ [key: string]: T }> {
    return this.referenceDataManagementUiLibraryService.referenceData.pipe(
      map((data: { [type: string]: { [correlationId: string]: T } }) => get(data, type)),
    );
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getReferenceDataCollection(metadata: { [type: string]: new(...args: any[]) => GenericReferenceData }): Observable<ReferenceDataCollection> {
    return this.getLoadedTypedReferenceData(metadata)
      .pipe(
        map((data: TypedReferenceData<GenericReferenceData>[]) => {
          return mapValues(keyBy(data, 'type'), (d: TypedReferenceData<GenericReferenceData>) => {
            return keyBy(d.referenceData, 'correlationId.correlation');
          });
        }),
      );
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getReferenceDataArray(metadata: { [type: string]: new(...args: any[]) => GenericReferenceData }): Observable<{ [type: string]: ReferenceData[] }> {
    return this.getLoadedTypedReferenceData(metadata)
      .pipe(
        map((data: TypedReferenceData<GenericReferenceData>[]) => {
          return mapValues(keyBy(data, 'type'), (d: TypedReferenceData<GenericReferenceData>) => {
            return this.referenceDataSortingPipe.transform(d.referenceData);
          });
        }),
      );
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getLoadedTypedReferenceData(metadata: { [type: string]: new(...args: any[]) => GenericReferenceData }):
    Observable<TypedReferenceData<GenericReferenceData>[]> {
    return combineLatest(
      _map(keys(metadata), (type: string) => this.referenceDataManagementUiLibraryService.getTypedReferenceData(type)),
    )
      .pipe(
        filter((data: TypedReferenceData<GenericReferenceData>[]) => {
          return every(data, (d: TypedReferenceData<GenericReferenceData>) => get(d, 'loaded'));
        }),
      );
  }
}

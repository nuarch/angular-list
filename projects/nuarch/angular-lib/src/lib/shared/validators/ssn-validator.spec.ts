import { FormControl } from '@angular/forms';
import { ssnValidator } from './ssn-validator';

describe('SSN validator', () => {
  let control: FormControl;
  const hint: string = 'e.g., 555-55-5555';

  beforeEach(() => {
    control = new FormControl();
    control.setValidators(ssnValidator);
  });

  it('should not validate when control value is empty', () => {
    control.updateValueAndValidity();
    expect(control.hasError('invalidFormat')).toBe(false);
  });

  it('should return undefined on ssn value of format xxx-xx-xxxx', () => {
    control.updateValueAndValidity();
    expect(control.hasError('invalidFormat')).toBe(false);
  });

  it('should return a validation error on other values', () => {
    const values: string[] = [
      ' ',
      '1',
      '123-12-12345',
      '123-12123',
      '123 12 1234',
      'xxx-xx-xxxx',
    ];
    for (let value of values) {
      control.setValue(value);
      control.updateValueAndValidity();
      expect(control.hasError('invalidFormat')).toBe(true);
      expect(control.errors.invalidFormat).toEqual({hint});
    }
  });
});

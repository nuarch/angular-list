import { AbstractControl } from '@angular/forms';

import { phoneRegex } from '../constants/phone-regex';

export function phoneValidator(control: AbstractControl): { [key: string]: { hint: string } } {
  const isValid: boolean = !control.value || phoneRegex.test(control.value);
  return isValid ? undefined : {invalidFormat: {hint: 'e.g., 555/555-5555, (555) 555-5555'}};
}

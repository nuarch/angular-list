import { AbstractControl, ValidatorFn } from '@angular/forms';
import { find, get } from 'lodash';
import { BehaviorSubject } from 'rxjs';
import { ReferenceData } from '../../core/models/reference-data';

export function autocompleteValidator(selections: BehaviorSubject<ReferenceData[] |
  { value: string, label: string, displayText: string }[]>): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } => {
    const isValid: boolean = !control.value
      || !!find(selections.value, control.value)
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      || !!find(selections.value, (item: any) => {
        return item.value === get(control, 'value.value', get(control, 'value'));
      });
    
    return isValid ? undefined : {invalidSelection: true};
  };
}

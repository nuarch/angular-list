import { FormControl } from '@angular/forms';
import { alphanumericValidator } from './alphanumeric-validator';

describe('AlphaNumeric validator', () => {

  let control: FormControl;
  const hint: string = 'Only numbers and letters';

  beforeEach(() => {
    control = new FormControl();
    control.setValidators(alphanumericValidator);
  });

  it('should not validate when control value is empty', () => {
    control.updateValueAndValidity();
    expect(control.hasError('invalidFormat')).toBe(false);
  });

  it('should return undefined on element format with all numbers', () => {
    control.setValue('12345');
    control.updateValueAndValidity();
    expect(control.hasError('invalidFormat')).toBe(false);
  });

  it('should return undefined on element format with all letters', () => {
    control.setValue('aaaa');
    control.updateValueAndValidity();
    expect(control.hasError('invalidFormat')).toBe(false);
  });

  it('should return undefined on element format with numbers and letters', () => {
    control.setValue('123abc');
    control.updateValueAndValidity();
    expect(control.hasError('invalidFormat')).toBe(false);
  });

  it('should return a validation error on other values', () => {
    const values: string[] = [
      'a!@#)',
      '5$%^',
      '?><:}',
    ];
    for (let value of values) {
      control.setValue(value);
      control.updateValueAndValidity();
      expect(control.hasError('invalidFormat')).toBe(true);
      expect(control.errors.invalidFormat).toEqual({hint});
    }
  });
});

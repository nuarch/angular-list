import { FormControl } from '@angular/forms';

import { zipCodeValidator } from './zip-code-validator';

describe('Zip Code validator', () => {
  let control: FormControl;
  const hint: string = 'e.g., 12345, 12345-1234';

  beforeEach(() => {
    control = new FormControl();
    control.setValidators(zipCodeValidator);
  });

  it('should not validate when control value is empty', () => {
    control.updateValueAndValidity();
    expect(control.hasError('invalidFormat')).toBe(false);
  });

  it('should return undefined on zip code of format xxxxx', () => {
    control.setValue('12345');
    control.updateValueAndValidity();
    expect(control.hasError('invalidFormat')).toBe(false);
  });

  it('should return a validation error on other values', () => {
    const values: string[] = [
      ' ',
      '123',
      'xxxxx',
    ];
    for (let value of values) {
      control.setValue(value);
      control.updateValueAndValidity();
      expect(control.hasError('invalidFormat')).toBe(true);
      expect(control.errors.invalidFormat).toEqual({hint});
    }
  });
});

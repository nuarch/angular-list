import { AbstractControl } from '@angular/forms';

import { ssnRegex } from '../constants/ssn-regex';

export function ssnValidator(control: AbstractControl): { [key: string]: { hint: string } } {
  const isValid: boolean = !control.value || ssnRegex.test(control.value);
  return isValid ? undefined : {invalidFormat: {hint: 'e.g., 555-55-5555'}};
}

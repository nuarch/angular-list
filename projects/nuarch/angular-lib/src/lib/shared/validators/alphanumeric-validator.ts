import { AbstractControl } from '@angular/forms';

export function alphanumericValidator (control: AbstractControl): { [key: string]: { hint: string } } {
  const regExp: RegExp = /^[A-Za-z0-9]+$/;
  const isValid: boolean = !control.value || regExp.test(control.value);
  return isValid ? undefined : { invalidFormat: { hint: 'Only numbers and letters' }};
}

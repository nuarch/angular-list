import { AbstractControl } from '@angular/forms';

export function zipCodeValidator(control: AbstractControl): { [key: string]: { hint: string } } {
  const zipCodeRegex: RegExp = /^\d{5}(-\d{4})?$/;
  const isValid: boolean = !control.value || zipCodeRegex.test(control.value);
  return isValid ? undefined : {invalidFormat: {hint: 'e.g., 12345, 12345-1234'}};
}

import { FormControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { autocompleteValidator } from './autocomplete-validator';

describe('Autocomplete validator', () => {
  let control: FormControl;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const selections: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([
    {label: 'Permanent Appointment', value: {correlation: 'A15-DEM'}},
    {label: 'Alternate Title Change', value: {correlation: 'C60-DTA'}},
    {label: 'Alternate Title Change', value: {correlation: 'B51-DTA'}},
  ]);

  beforeEach(() => {
    control = new FormControl();
    control.setValidators(autocompleteValidator(selections));
  });

  it('should not validate when control value is empty', () => {
    control.updateValueAndValidity();
    expect(control.hasError('invalidSelection')).toBe(false);
  });

  it('should return undefined on control value is in the selections', () => {
    control.setValue({label: 'Permanent Appointment', value: {correlation: 'A15-DEM'}});
    control.updateValueAndValidity();
    expect(control.hasError('invalidSelection')).toBe(false);
  });

  it('should return a validation error on other values', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const values: any[] = [
      {correlation: 'A15-DEM'},
      {label: 'Not Found', value: {correlation: '404'}},
      ' ',
      'A15-DEM',
      'Permanent Appointment',
    ];
    for (let value of values) {
      control.setValue(value);
      control.updateValueAndValidity();
      expect(control.hasError('invalidSelection')).toBe(true);
    }
  });
});

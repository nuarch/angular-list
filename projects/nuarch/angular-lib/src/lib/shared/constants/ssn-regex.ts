export const ssnRegex: RegExp = /^([0-9]{3})[\-]?([0-9]{2})[\-]?([0-9]{4})$/;

export const ssnLength: number = 9;

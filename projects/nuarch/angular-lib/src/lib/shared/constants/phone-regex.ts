export const phoneRegex: RegExp = /^\(?([0-9]{3})\)?[-./ ]?([0-9]{3})[-. ]?([0-9]{4})$/;

export const phoneLength: number = 10;

import { ChangeDetectorRef } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';

import { Observable, Subject } from 'rxjs';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Constructor<T> = new (...args: any[]) => T;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const noop: any = () => {
  // empty method
};

export interface IControlValueAccessor extends ControlValueAccessor {
  /* eslint-disable-next-line */
  value: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  valueChanges: Observable<any>;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  onChange: (_: any) => any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  onTouched: () => any;
}

export interface IHasChangeDetectorRef {
  _changeDetectorRef: ChangeDetectorRef;
}

/** Mixin to augment a component with ngModel support. */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function mixinControlValueAccessor<T extends Constructor<IHasChangeDetectorRef>>(base: T, initialValue?: any):
  Constructor<IControlValueAccessor> & T {
  return class extends base {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private _subjectValueChanges: Subject<any>;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private _value: any = initialValue instanceof Array ? Object.assign([], initialValue) : initialValue;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    valueChanges: Observable<any>;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    onChange = (_: any) => noop;
    onTouched = () => noop;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    constructor(...args: any[]) {
      super(...args);
      /* eslint-disable-next-line */
      this._subjectValueChanges = new Subject<any>();
      this.valueChanges = this._subjectValueChanges.asObservable();
    }

    /* eslint-disable-next-line */
    get value(): any {
      return this._value;
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    set value(v: any) {
      if (v !== this._value) {
        this._value = v;
        this.onChange(v);
        this._changeDetectorRef.markForCheck();
        this._subjectValueChanges.next(v);
      }
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    writeValue(value: any): void {
      this.value = value;
      this._changeDetectorRef.markForCheck();
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    registerOnChange(fn: any): void {
      this.onChange = fn;
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    registerOnTouched(fn: any): void {
      this.onTouched = fn;
    }
  };
}

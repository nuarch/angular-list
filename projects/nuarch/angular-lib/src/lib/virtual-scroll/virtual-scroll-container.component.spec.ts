import { async, ComponentFixture, TestBed, } from '@angular/core/testing';
import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { NuVirtualScrollContainerComponent } from './virtual-scroll-container.component';
import { VirtualScrollModule } from './virtual-scroll.module';
import { MatListModule } from '@angular/material/list';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

@Component({
  template: `
    <mat-list>
      <nuarch-virtual-scroll-container [style.height.px]="height" [data]="data" (bottom)="myBottom()">
        <ng-template let-row="row" let-last="last" nuVirtualScrollRow>
          <mat-list-item>
            <h4 matLine>{{row}}</h4>
          </mat-list-item>
          <mat-divider *ngIf="!last"></mat-divider>
        </ng-template>
      </nuarch-virtual-scroll-container>
    </mat-list>`,
})
class TestBasicVirtualScrollComponent {
  height: number = 200;
  data: string[] = [
    'Pizza',
    'Burger',
    'Tacos',
    'Sushi',
    'Wings',
    'Salad',
    'Fries',
    'Nuggets',
    'Quesadilla',
    'Steak',
    'Hot Dog',
    'Torta',
    'Rice',
    'Sandwich',
    'Tuna',
    'Milk',
    'Soda',
    'Tea',
  ];
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  myBottom: any = () => ({});
}

describe('Component: VirtualScrollContainer', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        MatListModule,
        VirtualScrollModule,
      ],
      declarations: [
        TestBasicVirtualScrollComponent,
      ],
    });
    TestBed.compileComponents();
  }));

  it('should render only what fits the viewport', (done: DoneFn) => {
    let fixture: ComponentFixture<TestBasicVirtualScrollComponent> = TestBed.createComponent(TestBasicVirtualScrollComponent);
    let component: TestBasicVirtualScrollComponent = fixture.debugElement.componentInstance;
    let virtualScrollComponent: DebugElement = fixture.debugElement.query(By.directive(NuVirtualScrollContainerComponent));

    component.height = 200;
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(virtualScrollComponent.componentInstance.fromRow).toBe(0);
        expect(virtualScrollComponent.componentInstance.virtualData.length).toBe(8);
        component.height = 400;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(virtualScrollComponent.componentInstance.fromRow).toBe(0);
          expect(virtualScrollComponent.componentInstance.virtualData.length).toBe(12);
          component.height = 300;
          fixture.detectChanges();
          fixture.whenStable().then(() => {
            expect(virtualScrollComponent.componentInstance.fromRow).toBe(0);
            expect(virtualScrollComponent.componentInstance.virtualData.length).toBe(10);
            done();
          });
        });
      });
    });
  });

  // ignored for now, requires @sandesh to look into this.
  xit('should render rows and scroll to 2th row', (done: DoneFn) => {
    let fixture: ComponentFixture<TestBasicVirtualScrollComponent> = TestBed.createComponent(TestBasicVirtualScrollComponent);
    let component: TestBasicVirtualScrollComponent = fixture.debugElement.componentInstance;
    let virtualScrollComponent: DebugElement = fixture.debugElement.query(By.directive(NuVirtualScrollContainerComponent));

    component.height = 100;
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(virtualScrollComponent.componentInstance.fromRow).toBe(0, 'fromRow should be 0');
        expect(virtualScrollComponent.componentInstance.virtualData.length).toBe(6, 'virtualDataLength should be 6');
        fixture.detectChanges();
        virtualScrollComponent.componentInstance.scrollTo(2);
        expect(virtualScrollComponent.nativeElement.scrollTop).toBe(virtualScrollComponent.componentInstance.rowHeight * 2,
        'scrollTop should be rowHeight * 2');
        done();
      });
    });
  });

  it('should render rows, clear them and render them again', (done: DoneFn) => {
    let fixture: ComponentFixture<TestBasicVirtualScrollComponent> = TestBed.createComponent(TestBasicVirtualScrollComponent);
    let component: TestBasicVirtualScrollComponent = fixture.debugElement.componentInstance;
    let virtualScrollComponent: DebugElement = fixture.debugElement.query(By.directive(NuVirtualScrollContainerComponent));

    component.height = 100;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let data: any[] = component.data;
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(virtualScrollComponent.componentInstance.fromRow).toBe(0);
        expect(virtualScrollComponent.componentInstance.virtualData.length).toBe(6);
        component.data = [];
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(virtualScrollComponent.componentInstance.fromRow).toBe(0);
          expect(virtualScrollComponent.componentInstance.virtualData.length).toBe(0);
          component.data = data;
          fixture.detectChanges();
          fixture.whenStable().then(() => {
            expect(virtualScrollComponent.componentInstance.fromRow).toBe(0);
            expect(virtualScrollComponent.componentInstance.virtualData.length).toBe(6);
            done();
          });
        });
      });
    });
  });

  it('should emit bottom event', (done: DoneFn) => {
    let fixture: ComponentFixture<TestBasicVirtualScrollComponent> = TestBed.createComponent(TestBasicVirtualScrollComponent);
    let component: TestBasicVirtualScrollComponent = fixture.debugElement.componentInstance;
    let virtualScrollComponent: DebugElement = fixture.debugElement.query(By.directive(NuVirtualScrollContainerComponent));
    let eventSpy: jasmine.Spy = spyOn(component, 'myBottom');

    component.height = 100;
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        virtualScrollComponent.componentInstance.scrollToEnd();

        fixture.detectChanges();
        setTimeout(() => {
          expect(eventSpy.calls.count()).toBe(1);
          done();
        }, 500);
      });
    });
  });
});

import { NgModule, Type } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NuVirtualScrollRowDirective } from './virtual-scroll-row.directive';
import { NuVirtualScrollContainerComponent } from './virtual-scroll-container.component';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const NU_VIRTUAL_SCROLL: Type<any>[] = [
  NuVirtualScrollRowDirective,
  NuVirtualScrollContainerComponent,
];

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    NU_VIRTUAL_SCROLL,
  ],
  exports: [
    NU_VIRTUAL_SCROLL,
  ],
})
export class VirtualScrollModule {

}

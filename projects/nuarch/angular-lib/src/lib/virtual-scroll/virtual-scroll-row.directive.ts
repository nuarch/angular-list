import { Directive, TemplateRef, ViewContainerRef } from '@angular/core';
import { TemplatePortalDirective } from '@angular/cdk/portal';

// eslint-disable-next-line @angular-eslint/directive-selector
@Directive({selector: '[nuVirtualScrollRow]'})
export class NuVirtualScrollRowDirective extends TemplatePortalDirective {

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(templateRef: TemplateRef<any>,
              viewContainerRef: ViewContainerRef) {
    super(templateRef, viewContainerRef);
  }

}

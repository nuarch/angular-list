import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { CoreModule } from '../core/core.module';
import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';

@NgModule({
    declarations: [
        SearchComponent,
    ],
    imports: [
        CommonModule,
        SearchRoutingModule,
        MatIconModule,
        MatButtonModule,
        FlexLayoutModule,
        MatInputModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        CoreModule.forRoot({}),
    ],
    exports: [
        SearchComponent,
    ]
})
export class SearchModule {

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public static forRoot(environment: any): ModuleWithProviders<SearchModule> {

    return {
      ngModule: SearchModule,
      providers: [
        {
          provide: 'environment',
          useValue: environment,
        },
      ],
    };
  }
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { AbstractComponent } from '../core/components/abstract-component/abstract-component';
import { searchDebounceTime } from '../core/constants/debounce-time';
import { Entity } from '../core/models/entity';
import { CoreService } from '../core/services/core.service';

@Component({
  selector: 'nuarch-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent extends AbstractComponent implements OnInit {

  searchControl: FormControl = new FormControl();
  @Input() searchPlaceholder: string = 'Search';
  @Input() searchButton: boolean = false;
  @Input() searchString: string;
  @Output() search: EventEmitter<string> = new EventEmitter<string>();

  public constructor(private coreService: CoreService<Entity>) {
    super();
  }

  ngOnInit(): void {
    this.searchControl.setValue(this.searchString);
    this.searchControl.updateValueAndValidity();
    if (!this.searchButton) {
      this.searchControl.valueChanges.pipe(debounceTime(searchDebounceTime), takeUntil(this.componentDestroyed))
        .subscribe((searchTerm: string) => {
          this.search.emit(searchTerm);
        });
    }
  }

  doSearch(): void {
    this.coreService.dispatchSetSearch(this.searchControl.value);
    this.search.emit(this.searchControl.value);
  }

  clearSearch(): void {
    this.searchControl.setValue(undefined);
    this.searchControl.updateValueAndValidity();
    this.coreService.dispatchClearSearch();
    this.search.emit(undefined);
  }

  afterOnDestroy(): void {
    // afterOnDestroy
  }

}

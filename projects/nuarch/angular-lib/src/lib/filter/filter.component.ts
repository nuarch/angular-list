import {
  ChangeDetectorRef,
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { get, invoke, isEqual, reduce, assign } from 'lodash';
import { AbstractComponent } from '../core/components/abstract-component/abstract-component';
import { Entity } from '../core/models/entity';
import { Filter } from '../core/models/filter';
import { FilterContext } from '../core/models/filter-context';
import { FormModel } from '../core/models/form-model';
import { ReferenceData } from '../core/models/reference-data';
import { ReferenceDataService } from '../core/services/reference-data.service';
import { NuDynamicFormsComponent } from '../dynamic-forms/dynamic-forms.component';
import { INuDynamicElementConfig, NuDynamicFormsService } from '../dynamic-forms/services/dynamic-forms.service';
import { combineLatest, EMPTY, Observable } from 'rxjs';
import { CoreService } from '../core/services/core.service';
import moment from 'moment';
import { distinctUntilChanged, map, take, takeUntil, takeWhile } from 'rxjs/operators';

@Component({
  selector: 'nuarch-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent extends AbstractComponent implements OnInit {

  @Input() filterModel: FormModel;
  @Input() filterEntity: Filter;
  @Input() filterGroup: FormGroup;
  @Input() showTitle: boolean = true;
  @Input() refreshOnFilterEntityChange: boolean = false;
  @Output() apply: EventEmitter<Filter> = new EventEmitter<Filter>();
  @Output() cancel: EventEmitter<void> = new EventEmitter<void>();
  @Output() init: EventEmitter<void> = new EventEmitter<void>();
  elements: INuDynamicElementConfig[];
  defaultFilter: Filter;
  dateFormat: string = 'MM/dd/yyyy';
  filter: Filter;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @ContentChild('actionsTemplate', {static: false}) actionsTemplate: any;
  @ViewChild(NuDynamicFormsComponent, {static: false}) dynamicFormsComponent: NuDynamicFormsComponent;

  public constructor(private router: Router,
                     private route: ActivatedRoute,
                     private referenceDataService: ReferenceDataService,
                     private dynamicFormService: NuDynamicFormsService,
                     private coreService: CoreService<Filter>,
                     private cdRef: ChangeDetectorRef,
                     public dialog: MatDialog) {
    super();
  }

  ngOnInit(): void {
    this.coreService.defaultFilter.pipe(takeUntil(this.componentDestroyed)).subscribe((defaultFilter: Filter) => {
      this.defaultFilter = defaultFilter;
    });
    combineLatest([
        this.referenceData,
        this.filterData,
      ],
    ).pipe(
      takeUntil(this.componentDestroyed),
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      takeWhile((val: any, index: number) => this.refreshOnFilterEntityChange || (!this.refreshOnFilterEntityChange && index < 1)),
      distinctUntilChanged(isEqual),
      map((data: [{ [type: string]: ReferenceData[] }, Filter], index: number) => {
        return [data[0], data[1], index];
      }))
      .subscribe(([referenceData, filterEntity, index]: [{ [type: string]: ReferenceData[] }, Filter, number]) => {
        this.filter = filterEntity;
        this.initElements(filterEntity, referenceData, index === 0);
      });
  }

  get referenceData(): Observable<{ [type: string]: ReferenceData[] }> {
    if (get(this.filterModel, 'referenceData')) {
      return this.referenceDataService.getReferenceDataArray(this.filterModel.referenceData);
    }
    return EMPTY;
  }

  get filterData(): Observable<Filter> {
    return this.coreService.filter;
  }

  initialized(): void {
    this.coreService.dispatchFilterReady();
    this.init.emit();
  }

  initElements(filterEntity: Filter, referenceData?: { [type: string]: ReferenceData[] }, firstLoad?: boolean): void {
    const controls: INuDynamicElementConfig[] = get(this.filterModel, 'addTemplate', get(this.filterModel, 'editTemplate'));
    if (!this.filterGroup) {
      this.elements = this.dynamicFormService.mapFormData(filterEntity, controls, referenceData);
    } else {
      this.elements = controls.slice();
    }
    this.cdRef.detectChanges();
    if (firstLoad && this.isValid) {
      this.coreService.dispatchSetFilter(assign({},
        this.formValue(invoke(this.dynamicFormsComponent, 'dynamicFormGroup.getRawValue')), this.filter));
      if (!this.defaultFilter) {
        this.coreService.dispatchSetDefaultFilter(this.formValue(invoke(this.dynamicFormsComponent, 'dynamicFormGroup.getRawValue')));
      }
    }
  }

  get filterContext(): FilterContext {
    return {
      entity: this.filterEntity,
      formValue: this.formValue(invoke(this.dynamicFormsComponent, 'dynamicFormGroup.getRawValue')),
      form: get(this.dynamicFormsComponent, 'dynamicForm'),
    };
  }

  formValue(rawValue: { [fieldName: string]: string | number | boolean | ReferenceData }): Entity {
    return reduce(rawValue,
      (extractedValue: { [controlName: string]: string | number | boolean | ReferenceData },
       controlValue: string | number | boolean | ReferenceData,
       key: string) => {
        extractedValue[key] = get(controlValue, 'value', get(controlValue, 'correlationId', controlValue));
        return extractedValue;
      }, {}) as Entity;
  }

  get isValid(): boolean {
    return get(this.dynamicFormsComponent, 'dynamicFormGroup.valid');
  }

  save(): void {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const data: any = this.formValue(invoke(this.dynamicFormsComponent, 'dynamicFormGroup.getRawValue'));
    this.apply.emit(data);
  }

  close(): void {
    invoke(this.dynamicFormsComponent, 'dynamicFormGroup.reset');
    this.cancel.emit();
  }

  afterOnDestroy(): void {
    // afterOnDestroy
  }

}

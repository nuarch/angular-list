import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CoreModule } from '../core/core.module';
import { DynamicFormsModule } from '../dynamic-forms/dynamic-forms.module';
import { FilterRoutingModule } from './filter-routing.module';
import { FilterComponent } from './filter.component';

@NgModule({
    declarations: [
        FilterComponent,
    ],
    imports: [
        CommonModule,
        FilterRoutingModule,
        DynamicFormsModule,
        MatIconModule,
        MatCardModule,
        MatButtonModule,
        MatDialogModule,
        FlexLayoutModule,
        MatProgressSpinnerModule,
        CoreModule.forRoot({}),
    ],
    exports: [
        FilterComponent,
    ]
})
export class FilterModule {

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public static forRoot(environment: any): ModuleWithProviders<FilterModule> {

    return {
      ngModule: FilterModule,
      providers: [
        {
          provide: 'environment',
          useValue: environment,
        },
      ],
    };
  }
}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY } from 'rxjs/index';
import { anything, instance, mock, when } from 'ts-mockito';
import { Entity } from '../core/models/entity';
import { CoreService } from '../core/services/core.service';
import { ReferenceDataService } from '../core/services/reference-data.service';

import { FilterComponent } from './filter.component';

xdescribe('FilterComponent', () => {
  let component: FilterComponent;
  let fixture: ComponentFixture<FilterComponent>;

  const mockActivatedRoute: ActivatedRoute = mock(ActivatedRoute);
  when(mockActivatedRoute.paramMap).thenReturn(EMPTY);
  const mockReferenceDataService: ReferenceDataService = mock(ReferenceDataService);
  when(mockReferenceDataService.getReferenceDataArray(anything())).thenReturn(EMPTY);
  const mockCoreService: CoreService<Entity> = mock(CoreService);
  when(mockCoreService.selectedEntity).thenReturn(EMPTY);
  const mockRouter: Router = mock(Router);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        imports: [
          NoopAnimationsModule,
          MatCardModule,
          MatIconModule,
          FormsModule,
          ReactiveFormsModule,
        ],
        declarations: [
          FilterComponent,
          //  NuDynamicFormsErrorTemplateDirective,
        ],
        providers: [
          {
            provide: CoreService,
            useFactory: () => instance(mockCoreService),
          },
          {
            provide: ActivatedRoute,
            useFactory: () => instance(mockActivatedRoute),
          },
          {
            provide: Router,
            useFactory: () => instance(mockRouter),
          },
          {
            provide: ReferenceDataService,
            useFactory: () => instance(mockReferenceDataService),
          },
        ],
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterComponent);
    component = fixture.componentInstance;
    component.filterModel = {addTemplate: undefined, editTemplate: undefined, sectionName: undefined, referenceData: undefined};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

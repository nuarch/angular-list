import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { CoreModule } from '../core/core.module';
import { FilterModule } from '../filter/filter.module';
import { SearchModule } from '../search/search.module';
import { VirtualScrollModule } from '../virtual-scroll/virtual-scroll.module';
import { ListCardComponent } from './components/list-card/list-card.component';
import { ListMultiRowComponent } from './components/list-multi-row/list-multi-row.component';
import { ListTableComponent } from './components/list-table/list-table.component';
import { ListRoutingModule } from './list-routing.module';
import { ListComponent } from './list.component';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  imports: [
    CommonModule,
    MatSortModule,
    MatTableModule,
    MatCardModule,
    FlexLayoutModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatFormFieldModule,
    MatSelectModule,
    MatSlideToggleModule,
    FormsModule,
    MatInputModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    MatButtonModule,
    CoreModule.forRoot({}),
    ListRoutingModule,
    VirtualScrollModule,    
    CdkTableModule,
    SearchModule,
    FilterModule,
  ],
  declarations: [
    ListTableComponent,
    ListComponent,
    ListMultiRowComponent,
    ListCardComponent,
  ],
  exports: [
    ListTableComponent,
    ListComponent,
    ListMultiRowComponent,
    ListCardComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ListModule {

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public static forRoot(environment: any): ModuleWithProviders<ListModule> {

    return {
      ngModule: ListModule,
      providers: [
        {
          provide: 'environment',
          useValue: environment,
        },
      ],
    };
  }
}

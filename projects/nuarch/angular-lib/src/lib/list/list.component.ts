import { Component, Input, TrackByFunction } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { head, split } from 'lodash';
import { Observable } from 'rxjs';
import { AbstractList } from '../core/components/abstract-list/abstract-list';
import { Entity } from '../core/models/entity';
import { Filter } from '../core/models/filter';
import { LoadListPayload } from '../core/models/load-list-payload';
import { Page } from '../core/models/page';
import { PageInfo } from '../core/models/page-info';
import { CoreService } from '../core/services/core.service';
import { ReferenceDataService } from '../core/services/reference-data.service';

@Component({
  selector: 'nuarch-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent extends AbstractList<Entity> {

  constructor(protected coreService: CoreService<Entity>,
              protected router: Router,
              protected referenceDataService: ReferenceDataService,
              protected route: ActivatedRoute) {
    super(router, route, coreService, referenceDataService);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @Input() trackBy: TrackByFunction<any> = (index: number, item: any) => {
    return item;
  }

  get entityPage(): Observable<Page<Entity>> {
    return this.coreService.page;
  }

  get filter(): Observable<Filter> {
    return this.coreService.filter;
  }

  get defaultFilter(): Observable<Filter> {
    return this.coreService.defaultFilter;
  }

  get pageInfo(): Observable<PageInfo> {
    return this.coreService.pageInfo;
  }

  get searchString(): Observable<string> {
    return this.coreService.searchString;
  }

  get loading(): Observable<boolean> {
    return this.coreService.listLoading;
  }

  get selectedListItems(): Observable<string[]> {
    return this.coreService.selectedListItems;
  }

  dispatchSetFilter(f: Filter): void {
    this.coreService.dispatchSetFilter(f);
  }

  dispatchSetSearch(searchString: string): void {
    this.coreService.dispatchSetSearch(searchString);
  }

  dispatchClearSearch(): void {
    this.coreService.dispatchClearSearch();
  }

  dispatchLoadList(payload: LoadListPayload): void {
    if (!!this.viewModel) {
      this.coreService.dispatchLoadList(payload);
    }
  }

  dispatchResetList(): void {
    this.coreService.dispatchResetList();
  }

  dispatchSelectListItem(id: string): void {
    this.coreService.dispatchSelectListItem(id);
  }

  dispatchUnselectListItem(id: string): void {
    this.coreService.dispatchUnselectListItem(id);
  }

  dispatchSelectAllListItems(): void {
    this.coreService.dispatchSelectAllListItems();
  }

  dispatchUnselectAllListItems(): void {
    this.coreService.dispatchUnselectAllListItems();
  }

}

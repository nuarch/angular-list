import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, EMPTY, of } from 'rxjs';
import { anything, instance, mock, when } from 'ts-mockito';
import { defaultFilter } from '../core/constants/default-filter';
import { defaultPageInfo } from '../core/constants/default-page-info';
import { configureTestSuite } from '../core/functions/configure-test-suite';
import { Entity } from '../core/models/entity';
import { Page } from '../core/models/page';
import { CoreService } from '../core/services/core.service';
import { ReferenceDataService } from '../core/services/reference-data.service';
import { ListComponent } from './list.component';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;

  const lawsPageSubject: BehaviorSubject<Page<Entity>> =
    new BehaviorSubject<Page<Entity>>({pageInfo: defaultPageInfo, entity: [], serverPageInfo: {totalElements: 5}});
  const searchSubject: BehaviorSubject<string> = new BehaviorSubject<string>('');

  const mockCoreService: CoreService<Entity> = mock(CoreService);
  when(mockCoreService.page).thenReturn(lawsPageSubject);
  when(mockCoreService.pageInfo).thenReturn(of(defaultPageInfo));
  when(mockCoreService.listLoading).thenReturn(of(false));
  when(mockCoreService.detailsLoading).thenReturn(of(false));
  when(mockCoreService.filter).thenReturn(of(defaultFilter));
  when(mockCoreService.defaultFilter).thenReturn(of(defaultFilter));
  when(mockCoreService.searchString).thenReturn(searchSubject);
  const mockReferenceDataService: ReferenceDataService = mock(ReferenceDataService);
  when(mockReferenceDataService.getReferenceDataArray(anything())).thenReturn(EMPTY);

  const mockActivatedRoute: ActivatedRoute = mock(ActivatedRoute);

  const mockRouter: Router = mock(Router);
  when(mockRouter.events).thenReturn(EMPTY);

  configureTestSuite();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  beforeAll((done: any) => (async () => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        MatCardModule,
        RouterTestingModule,            
        MatInputModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        NoopAnimationsModule,
      ],
      declarations: [
        ListComponent,
      ],
      providers: [
        {
          provide: CoreService,
          useFactory: () => instance(mockCoreService),
        },
        {
          provide: ActivatedRoute,
          useFactory: () => instance(mockActivatedRoute),
        },
        {
          provide: Router,
          useFactory: () => instance(mockRouter),
        },
        {
          provide: ReferenceDataService,
          useFactory: () => instance(mockReferenceDataService),
        },
      ],
    });

    await TestBed.compileComponents();
  })().then(done).catch(done.fail));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    component.viewModel = {columns: undefined, url: undefined, showAdd: undefined};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init search control', () => {
    expect(component.search).toBeDefined();
  });

  it('should fill in search bar with search query', () => {
    searchSubject.next('0001234');
    component.searchString.subscribe((value: string) => expect(value).toEqual('0001234'));
  });
});

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatSortModule } from '@angular/material/sort';
import { InfiniteScrollingDirective } from '../../../core/directives/infinite-scrolling.directive';

import { ListMultiRowComponent } from './list-multi-row.component';
import { EMPTY } from 'rxjs';

describe('ListMultiRowComponent', () => {
  let component: ListMultiRowComponent;
  let fixture: ComponentFixture<ListMultiRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        imports: [
          MatSortModule,
          FlexLayoutModule,
        ],
        declarations: [
          ListMultiRowComponent,
          InfiniteScrollingDirective,
        ],
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMultiRowComponent);
    component = fixture.componentInstance;
    component.page = EMPTY;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

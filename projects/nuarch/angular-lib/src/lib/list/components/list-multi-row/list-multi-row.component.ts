import { Component, Input, TrackByFunction } from '@angular/core';
import { AbstractSearchTable } from '../../../core/components/abstract-search-table/abstract-search-table';

@Component({
  selector: 'nuarch-list-multi-row',
  templateUrl: './list-multi-row.component.html',
  styleUrls: ['./list-multi-row.component.scss'],
})
export class ListMultiRowComponent extends AbstractSearchTable {

  @Input() loading: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @Input() rowTemplate: any;
  @Input() noRecordsFound: boolean = false;
  @Input() invalidFilter: boolean = false;
  @Input() pagination: boolean = false;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @Input() trackBy: TrackByFunction<any> = (index: number, item: any) => {
    return item;
  }

}

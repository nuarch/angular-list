import { AfterViewInit, ChangeDetectorRef, Component, Input, TrackByFunction } from '@angular/core';
import { Observable } from 'rxjs/index';
import { AbstractSearchTable } from '../../../core/components/abstract-search-table/abstract-search-table';
import { Entity } from '../../../core/models/entity';
import { takeUntil } from 'rxjs/operators';
import { filter, get, includes, some, toLower } from 'lodash';
import { Page } from '../../../core/models/page';

@Component({
  selector: 'nuarch-list-card',
  templateUrl: './list-card.component.html',
  styleUrls: ['./list-card.component.scss'],
})
export class ListCardComponent extends AbstractSearchTable implements AfterViewInit {

  _searchString: string;
  optionsLoading: boolean = false;
  filteredItems: Entity[];

  @Input() loading: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @Input() cardTemplate: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @Input() bodyTemplate: any;
  @Input() pagination: boolean = false;
  @Input() searchPipeAttributes: string[];

  @Input() set searchString(_searchString: Observable<string>) {
    _searchString.pipe(takeUntil(this.componentDestroyed)).subscribe((st: string) => {
      this.optionsLoading = true;
      this._searchString = st;
      setTimeout(() => {
        this.filteredItems = this.getFilteredItems();
        this.optionsLoading = false;
        this.cdRef.detectChanges();
      }, 0);
    });
  }

  @Input() noRecordsFound: boolean = false;
  @Input() invalidFilter: boolean = false;

  public constructor(private cdRef: ChangeDetectorRef) {
    super();
  }

  ngAfterViewInit(): void {
    this.page.pipe(takeUntil(this.componentDestroyed)).subscribe((e: Page<Entity>) => {
      this.filteredItems = e.entity;
    });
  }

  getFilteredItems(): Entity[] {
    const items: Entity[] = filter(get(this.entityPage, 'entity'), (e: Entity) => {
      return some(this.searchPipeAttributes, (attr: string) => includes(toLower(get(e, attr)),
        !!this._searchString ? toLower(this._searchString) : ''));
    });
    return items;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @Input() trackBy: TrackByFunction<any> = (index: number, item: any) => {
    return item;
  }

}

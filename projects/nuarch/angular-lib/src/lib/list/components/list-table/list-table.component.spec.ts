import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatOptionModule } from '@angular/material/core';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { filter, get, head } from 'lodash';
import { configureTestSuite } from '../../../core/functions/configure-test-suite';
import { VirtualScrollModule } from '../../../virtual-scroll/virtual-scroll.module';
import { ListTableComponent } from './list-table.component';
import { EMPTY } from 'rxjs';

describe('ListTableComponent', () => {
  let component: ListTableComponent;
  let fixture: ComponentFixture<ListTableComponent>;

  configureTestSuite();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  beforeAll((done: any) => (async () => {
    TestBed.configureTestingModule({
      imports: [
        MatCardModule,
        MatIconModule,
        RouterTestingModule,
        MatOptionModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        MatTableModule,
        MatDividerModule,
        MatSortModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        NoopAnimationsModule,
        VirtualScrollModule,
      ],
      declarations: [
        ListTableComponent,
      ],
    });

    await TestBed.compileComponents();
  })().then(done).catch(done.fail));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTableComponent);
    component = fixture.componentInstance;
    component.page = EMPTY;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  xit('should have <p> with "banner works!"', async(() => {
    const de: DebugElement = fixture.debugElement;
    let buttons: DebugElement[] = de.queryAll(By.css('button'));
    let button: DebugElement = head(filter(buttons, (b: DebugElement) => get(b, 'nativeElement.textContent') === '1'));
    expect(button).toBeDefined();
    expect(button.nativeElement.textContent).toEqual('1');
    button.nativeElement.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(button.nativeElement.textContent).toEqual('1');
      buttons = de.queryAll(By.css('button'));
      button = head(filter(buttons, (b: DebugElement) => get(b, 'nativeElement.textContent') === '2'));
      button.nativeElement.click();
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(button.nativeElement.textContent).toEqual('2');
      });
    });
  }));

});

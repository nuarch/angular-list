import { Component, Input } from '@angular/core';
import { get, map } from 'lodash';
import { AbstractSearchTable } from '../../../core/components/abstract-search-table/abstract-search-table';

@Component({
  selector: 'nuarch-list-table',
  templateUrl: './list-table.component.html',
  styleUrls: ['./list-table.component.scss'],
})
export class ListTableComponent extends AbstractSearchTable {

  @Input() loading: boolean;
  @Input() pagination: boolean = false;
  @Input() noRecordsFound: boolean = false;
  @Input() invalidFilter: boolean = false;

  map: Function = map;
  get: Function = get;

}

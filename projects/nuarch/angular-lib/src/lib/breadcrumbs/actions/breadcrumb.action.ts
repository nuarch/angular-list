import { Action } from '@ngrx/store';
import { Breadcrumb } from '../models/breadcrumb';

export enum BreadcrumbActionTypes {
  UpdateBreadcrumbs = '[Breadcrumbs] Update',
}

export class UpdateBreadcrumbsAction implements Action {
  readonly type: string = <string> BreadcrumbActionTypes.UpdateBreadcrumbs;

  constructor(public payload: Breadcrumb[]) {
  }
}

export type BreadcrumbActions = UpdateBreadcrumbsAction;

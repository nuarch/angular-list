import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbComponent } from './breadcrumb.component';
import { BreadcrumbService } from './services/breadcrumb.service';
import { BreadcrumbRoutingModule } from './breadcrumb-routing.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from './reducers/root-reducer';

@NgModule({
  declarations: [
    BreadcrumbComponent,
  ],
  imports: [
    CommonModule,
    BreadcrumbRoutingModule,
    StoreModule.forFeature('breadcrumb', reducers),
  ],
  providers: [
    BreadcrumbService,
  ],
  exports: [
    BreadcrumbComponent,
  ],
})
export class BreadcrumbsModule {

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public static forRoot(environment: any): ModuleWithProviders<BreadcrumbsModule> {

    return {
      ngModule: BreadcrumbsModule,
      providers: [
        BreadcrumbService,
        {
          provide: 'environment',
          useValue: environment,
        },
      ],
    };
  }
}

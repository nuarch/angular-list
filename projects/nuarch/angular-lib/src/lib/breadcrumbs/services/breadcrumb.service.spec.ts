import { HttpClientTestingModule } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { instance, mock, reset } from 'ts-mockito';
import { BreadcrumbService } from './breadcrumb.service';
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockStore } from '@ngrx/store/testing';
import * as fromBreadcrumb from '../reducers/root-reducer';
import { Store } from '@ngrx/store';
import { CoreModule } from '../../core/core.module';
import { EMPTY } from 'rxjs';

describe('BreadcrumbService', () => {

  const mockStore: Store<fromBreadcrumb.State> = mock<Store<fromBreadcrumb.State>>();
  const store: Store<fromBreadcrumb.State> = instance(mockStore);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
      ],
      providers: [
        BreadcrumbService,
        {
          provide: Store,
          useValue: store,
        },
      ],
    });

    reset(mockStore);
  });

  it('should be created', inject([BreadcrumbService], (breadcrumbService: BreadcrumbService) => {
    expect(breadcrumbService).toBeTruthy();
  }));
});

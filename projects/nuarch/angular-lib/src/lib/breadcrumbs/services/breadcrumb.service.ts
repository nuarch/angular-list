import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Breadcrumb } from '../models/breadcrumb';
import { UpdateBreadcrumbsAction } from '../actions/breadcrumb.action';
import { HttpClient } from '@angular/common/http';
import * as fromBreadcrumb from '../reducers/root-reducer';
import { ActivatedRoute, PRIMARY_OUTLET, Router, UrlSegment } from '@angular/router';
import { ROUTE_DATA_BREADCRUMB } from '../constants/breadcrumb';
import { uniqBy, startsWith, split, head, invoke, get } from 'lodash';

@Injectable()
export class BreadcrumbService {

  breadcrumbs: Observable<Breadcrumb[]>;
  _breadcrumbs: Breadcrumb[];

  constructor(private http: HttpClient,
              private store: Store<fromBreadcrumb.State>,
              private router: Router,
              private route: ActivatedRoute) {
    this.breadcrumbs = store.pipe(select(fromBreadcrumb.getBreadcrumbs));
    if (!!this.breadcrumbs) {
      this.breadcrumbs.subscribe((breadcrumbs: Breadcrumb[]) => {
        this._breadcrumbs = breadcrumbs;
      });
    }
  }

  dispatchUpdateBreadcrumbs(payload: Breadcrumb[]): void {
    this.store.dispatch(new UpdateBreadcrumbsAction(payload));
  }

  buildBreadCrumb(route: ActivatedRoute, url: string = '',
                  breadcrumbs: Breadcrumb[] = []): Breadcrumb[] {
    let children: ActivatedRoute[] = route.children;

    if (children.length === 0) {
      return uniqBy(breadcrumbs, (breadcrumb: Breadcrumb) => breadcrumb.label);
    }

    const preserveBreadcrumb: boolean = get(invoke(this.router, 'getCurrentNavigation'), 'extras.state.preserveBreadcrumbs');

    if (preserveBreadcrumb) {
      breadcrumbs = this._breadcrumbs.slice();
    }

    for (let child of children) {

      if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {
        return this.buildBreadCrumb(child, url, breadcrumbs);
      }

      let routeURL: string = child.snapshot.url.map((segment: UrlSegment) => segment.path).join('/');

      url += `/${routeURL}`.replace('//', '/');

      const data: string = child.snapshot.data[ROUTE_DATA_BREADCRUMB];

      let breadcrumb: Breadcrumb = {
        label: startsWith(data, ':') ? head(split(routeURL, '/')) : data,
        url: url,
      };
      breadcrumbs.push(breadcrumb);
      return this.buildBreadCrumb(child, url, breadcrumbs);
    }
    return breadcrumbs;
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BreadcrumbComponent } from './breadcrumb.component';
import { instance, mock } from 'ts-mockito';
import { RouterTestingModule } from '@angular/router/testing';
import { assign } from 'lodash';
import { BreadcrumbService } from './services/breadcrumb.service';
import { configureTestSuite } from '../core/functions/configure-test-suite';

describe('BreadcrumbComponent', () => {
  let component: BreadcrumbComponent;
  let fixture: ComponentFixture<BreadcrumbComponent>;

  let mockBreadcrumbService: BreadcrumbService = mock(BreadcrumbService);
  let navigationService: BreadcrumbService = instance(mockBreadcrumbService);

  configureTestSuite();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  beforeAll((done: any) => (async () => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      declarations: [
        BreadcrumbComponent,
      ],
      providers: [
        {
          provide: BreadcrumbService,
          useValue: navigationService,
        },
      ],
    });

    await TestBed.compileComponents();
  })().then(done).catch(done.fail));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreadcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});

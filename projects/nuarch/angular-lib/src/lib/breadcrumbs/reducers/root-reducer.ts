import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import * as fromBreadcrumb from './breadcrumb.reducer';

export interface BreadcrumbState {
  breadcrumb: fromBreadcrumb.LocalState;
}

export interface State {
  breadcrumb: BreadcrumbState;
}

/* eslint-disable @typescript-eslint/no-explicit-any */
export const reducers: any = {
  breadcrumb: fromBreadcrumb.reducer,
};

/* eslint-disable @typescript-eslint/no-explicit-any */
export const getBreadcrumbState: MemoizedSelector<any, any> = createFeatureSelector<BreadcrumbState>('breadcrumb');

export const getBreadcrumbEntitiesState: MemoizedSelector<any, any> =
  createSelector(getBreadcrumbState, (state: BreadcrumbState) => state.breadcrumb);

/* eslint-disable @typescript-eslint/no-explicit-any */
export const getBreadcrumbs: MemoizedSelector<any, any> = createSelector(
  getBreadcrumbEntitiesState, fromBreadcrumb.getBreadcrumb,
);

import { assign } from 'lodash';
import { BreadcrumbActions, BreadcrumbActionTypes } from '../actions/breadcrumb.action';
import { Breadcrumb } from '../models/breadcrumb';

export interface LocalState {
  breadcrumbs: Breadcrumb[];
}

export const initialState: LocalState = {
  breadcrumbs: undefined,
};

export function reducer(state: LocalState = initialState, action: BreadcrumbActions): LocalState {
  switch (action.type) {
    case BreadcrumbActionTypes.UpdateBreadcrumbs: {
      return assign({}, state, {
        breadcrumbs: action.payload,
      });
    }
    default: {
      return state;
    }
  }
}

/* eslint-disable @typescript-eslint/no-explicit-any */
export const getBreadcrumb: any = (state: LocalState) => state.breadcrumbs;

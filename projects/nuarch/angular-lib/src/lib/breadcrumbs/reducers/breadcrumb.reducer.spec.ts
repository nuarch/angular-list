import { BreadcrumbActions, UpdateBreadcrumbsAction } from '../actions/breadcrumb.action';
import { Breadcrumb } from '../models/breadcrumb';
import { initialState, LocalState, reducer } from './breadcrumb.reducer';

describe('Breadcrumb Reducer', () => {
  it('should UpdateBreadcrumbs', () => {
    const breadcrumbs: Breadcrumb[] = [
      { label: 'l', url: 'u' },
    ];
    const action: UpdateBreadcrumbsAction = new UpdateBreadcrumbsAction(breadcrumbs);
    const updatedState: LocalState = reducer(initialState, action);
    expect(updatedState).toEqual({
      breadcrumbs: breadcrumbs,
    });
  });
});

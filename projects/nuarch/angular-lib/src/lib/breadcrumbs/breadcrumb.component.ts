import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Breadcrumb } from './models/breadcrumb';
import { Observable } from 'rxjs';
import { BreadcrumbService } from './services/breadcrumb.service';

@Component({
  selector: 'nuarch-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BreadcrumbComponent {

  breadcrumbs: Observable<Breadcrumb[]>;

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbs = this.breadcrumbService.breadcrumbs;
  }

}

import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CoreModule } from '../core/core.module';
import { DynamicFormsModule } from '../dynamic-forms/dynamic-forms.module';
import { CancelDialogComponent } from './components/cancel-dialog/cancel-dialog.component';
import { DetailsRoutingModule } from './details-routing.module';
import { DetailsComponent } from './details.component';

@NgModule({
    declarations: [
        DetailsComponent,
        CancelDialogComponent,
    ],
    imports: [
        CommonModule,
        DetailsRoutingModule,
        DynamicFormsModule,
        MatIconModule,
        MatCardModule,
        MatButtonModule,
        MatDialogModule,
        FlexLayoutModule,
        MatProgressSpinnerModule,
        CoreModule.forRoot({}),
    ],
    exports: [
        DetailsComponent,
        CancelDialogComponent,
    ]
})
export class DetailsModule {

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public static forRoot(environment: any): ModuleWithProviders<DetailsModule> {

    return {
      ngModule: DetailsModule,
      providers: [
        {
          provide: 'environment',
          useValue: environment,
        },
      ],
    };
  }
}

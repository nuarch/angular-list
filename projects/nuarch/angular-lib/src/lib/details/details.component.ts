import {
  ChangeDetectorRef,
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, ParamMap, Router, UrlSegment } from '@angular/router';
import { get, invoke, keys, reduce, last } from 'lodash';
import { combineLatest, Observable, of } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { AbstractComponent } from '../core/components/abstract-component/abstract-component';
import { addUrl } from '../core/constants/add-url';
import { editUrl } from '../core/constants/edit-url';
import { modes } from '../core/constants/modes';
import { Entity } from '../core/models/entity';
import { EntityContext } from '../core/models/entity-context';
import { FormModel } from '../core/models/form-model';
import { ReferenceData } from '../core/models/reference-data';
import { CoreService } from '../core/services/core.service';
import { ReferenceDataService } from '../core/services/reference-data.service';
import { NuDynamicFormsComponent } from '../dynamic-forms/dynamic-forms.component';
import { INuDynamicElementConfig, NuDynamicFormsService } from '../dynamic-forms/services/dynamic-forms.service';
import { CancelDialogComponent } from './components/cancel-dialog/cancel-dialog.component';
import { cancelFromEditUrl } from '../core/constants/cancel-from-edit-url';
import { cancelFromAddOrViewUrl } from '../core/constants/cancel-from-add-or-view-url';

@Component({
  selector: 'nuarch-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent extends AbstractComponent implements OnInit {

  private id: string;
  loading: Observable<boolean>;
  readonly addModeUrl: string = addUrl;
  readonly editModeUrl: string = editUrl;
  @Input() formModel: FormModel;
  @Input() formControls: FormGroup;
  @Input() cancelFromEditUrl: string = cancelFromEditUrl;
  @Input() cancelFromAddOrViewUrl: string = cancelFromAddOrViewUrl;
  @Input() redirectUrl: string;
  @Output() init: EventEmitter<void> = new EventEmitter<void>();
  mode: number = modes.READ;
  elements: INuDynamicElementConfig[];
  entity: Entity;
  @ViewChild(NuDynamicFormsComponent, {static: false}) dynamicFormsComponent: NuDynamicFormsComponent;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @ContentChild('additionalActionsTemplate', {static: false}) additionalActionsTemplate: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  @ContentChild('actionsTemplate', {static: false}) actionsTemplate: any;

  public constructor(private router: Router,
                     private route: ActivatedRoute,
                     private coreService: CoreService<Entity>,
                     private referenceDataService: ReferenceDataService,
                     private dynamicFormService: NuDynamicFormsService,
                     private cdRef: ChangeDetectorRef,
                     public dialog: MatDialog) {
    super();
    this.loading = this.coreService.detailsLoading;
  }

  get addMode(): boolean {
    return this.mode === modes.ADD;
  }

  get readMode(): boolean {
    return this.mode === modes.READ;
  }

  get editMode(): boolean {
    return this.mode === modes.EDIT;
  }

  get cancelUrl(): string {
    if (this.addMode || this.readMode) {
      return this.cancelFromAddOrViewUrl;
    }

    return this.cancelFromEditUrl;
  }

  get redirectAfterSaveUrl(): string {
    if (this.redirectUrl) {
      return this.redirectUrl;
    }

    return this.cancelUrl;
  }

  get entityContext(): EntityContext {
    return {
      entity: this.entity,
      formValue: this.formValue(invoke(this.dynamicFormsComponent, 'dynamicForm.getRawValue')),
      mode: this.mode,
    };
  }

  ngOnInit(): void {
    combineLatest([
        this.route.paramMap,
        this.route.url,
      ],
    ).pipe(
      takeUntil(this.componentDestroyed),
      filter(([params, url]: [ParamMap, UrlSegment[]]) => !!params && !!url),
    )
      .subscribe(([params, url]: [ParamMap, UrlSegment[]]) => {
        this.id = params.get('id');
        if (!this.id) {
          this.id = get(last(url), 'path');
        }
        this.mode = modes.READ;

        if (this.id === this.addModeUrl) {
          this.mode = modes.ADD;
        }
        if (params.get('mode') === this.editModeUrl) {
          this.mode = modes.EDIT;
        }
        if (this.addMode) {
          this.coreService.dispatchResetDetails();
        } else if (this.formModel) {
          this.coreService.dispatchLoadDetails({id: this.id, url: this.formModel.url, removeAllEntries: true});
        }
      });

    combineLatest([
        this.coreService.selectedEntity,
        this.formModel.referenceData ? this.referenceDataService.getReferenceDataArray(this.formModel.referenceData) : of(undefined),
      ],
    )
      .pipe(takeUntil(this.componentDestroyed),
        filter(([selectedEntity, referenceData]: [Entity, { [type: string]: ReferenceData[] }]) =>
          (this.addMode || (!this.addMode && !!selectedEntity)) &&
          (!this.formModel.referenceData || (!!this.formModel.referenceData && !!keys(referenceData).length))),
      )
      .subscribe(([selectedEntity, referenceData]: [Entity, { [type: string]: ReferenceData[] }]) => {
        this.entity = selectedEntity;
        let controls: INuDynamicElementConfig[];
        switch (this.mode) {
          case modes.ADD:
            controls = get(this.formModel, 'addTemplate');
            break;
          case modes.EDIT:
            controls = get(this.formModel, 'editTemplate');
            break;
          case modes.READ:
            controls = get(this.formModel, 'readTemplate');
            break;
          default:
            controls = get(this.formModel, 'readTemplate');
            break;
        }
        if (!this.formControls) {
          this.elements = this.dynamicFormService.mapFormData(selectedEntity, controls, referenceData);
        } else {
          this.elements = controls.slice();
        }
        this.cdRef.detectChanges();
      });

  }

  initialized(): void {
    this.init.emit();
  }

  formValue(rawValue: { [fieldName: string]: string | number | boolean | ReferenceData }): Entity {
    return reduce(rawValue,
      (extractedValue: { [controlName: string]: string | number | boolean | ReferenceData },
       controlValue: string | number | boolean | ReferenceData,
       key: string) => {
        extractedValue[key] = get(controlValue, 'value', controlValue);
        return extractedValue;
      }, {}) as Entity;
  }

  save(): void {
    invoke(this.dynamicFormsComponent, 'dynamicForm.markAllAsTouched');
    invoke(this.dynamicFormsComponent, 'updateValueAndValidity');

    if (get(this.dynamicFormsComponent, 'valid')) {
      if (this.addMode) {
        this.coreService.dispatchSaveDetails({
          url: this.formModel.url,
          entity: this.formValue(invoke(this.dynamicFormsComponent, 'dynamicForm.getRawValue')),
          redirect: true,
          redirectUrl: this.redirectAfterSaveUrl,
        });
      } else {
        this.coreService.dispatchUpdateDetails({
          id: this.id,
          url: this.formModel.url,
          urlOverride: false,
          entity: this.formValue(invoke(this.dynamicFormsComponent, 'dynamicForm.getRawValue')),
          redirect: true,
          redirectUrl: this.redirectAfterSaveUrl,
        });

      }
    }
  }

  cancel(): void {
    if (get(this.dynamicFormsComponent, 'dynamicFormGroup.dirty', false) === true) {
      const dialogRef: MatDialogRef<CancelDialogComponent> = this.dialog.open(CancelDialogComponent, {
        panelClass: 'cancel-dialog', disableClose: true, });
      dialogRef.afterClosed().pipe(takeUntil(this.componentDestroyed)).subscribe((result: boolean) => {
        if (result) {
          this.router.navigate([this.cancelUrl], {relativeTo: this.route});
        }
      });
    } else {
      this.router.navigate([this.cancelUrl], {relativeTo: this.route});
    }
  }

  afterOnDestroy(): void {
    // afterOnDestroy
  }

}

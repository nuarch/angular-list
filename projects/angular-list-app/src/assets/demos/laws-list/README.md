# nuarch-list
List with model driven layout, state management and advance filters.

## API Summary

#### Inputs

+ buttonTriggersSearch: boolean
  + false - search will be initiated as soon as start typing.
  + true - search will be initiated when you click search button.

+ openDetails: boolean
  + true - will activate the :id route on click of a row.

+ viewModel: ViewModel
  + Rows and column will be laid out depending on the configuration.

+ filterModel: FormModel
  + Advanced filter will be displayed using filterModel configuration. Dynamic form is used for the lay out.

+ filterGroup: FormGroup
  + This can be used in combination with filterModel. You can construct a formGroup and pass to the laid out model.

#### Properties (Read only)
+ entityPage: Observable<Page<Entity>>
  + Getter property for Page.
+ filter: Observable<Filter>
  + Getter property for filter.
+ pageInfo: Observable<PageInfo>
  + Getter property for pageInfo.
+ searchString: Observable<string>
  + Getter property for search string.
+ loading: Observable<boolean>
  + Getter property to check reference data and content are loaded.

## Setup

Import the [@nuarch/angular-lib] in your NgModule:

```typescript
import { CoreModule, ListModule } from '@covalent/dynamic-forms';
@NgModule({
  imports: [
    CoreModule.forRoot({}),
    ListModule,
    ...
  ],
  ...
})
export class MyModule {}
```

## Usage

`nuarch-list` draws a view using the configuration provided. Loads the data in the store. Infinite scrolling in built. On sort, searh, filter or scroll to the bottom, automatically makes an api call using the url provided, loads the data in the store and refreshes the list.

Pass a viewModel.

```typescript
import { ViewModel } from '@nuarch/angular-lib';

export const viewModel: ViewModel = {
  columns: [
    {
      label: 'ID',
      value: 'id',
      sortable: true,
      flex: '5',
    },
    {
      label: 'Law Section',
      value: 'lawSection',
      sortable: true,
      flex: '7.5',
    },
  ],
  searchPlaceholder: 'Start typing to search by law section or abbreviated description',
  sectionName: 'Laws',
  route: 'lawmgmt/api/list.json',
  addButtonText: 'Add a Law',
  showAdd: true,
};

```

NOTE: For custom types, you need to implement a `[control]` property and use it in your underlying element.

Example for HTML usage:

```html
<nuarch-list [viewModel]="viewModel" [openDetails]="false" (select)="navigate($event)"></nuarch-list>
```

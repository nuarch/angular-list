import { inject, TestBed } from '@angular/core/testing';
import { GlobalReferenceService } from './global-reference.service';

describe('GlobalReferenceService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GlobalReferenceService],
    });
  });

  it('should be created', inject([GlobalReferenceService], (service: GlobalReferenceService) => {
    expect(service).toBeTruthy();
  }));
});

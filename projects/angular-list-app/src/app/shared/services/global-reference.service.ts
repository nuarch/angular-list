import { Injectable } from '@angular/core';

@Injectable()
export class GlobalReferenceService {

  get nativeWindow(): Window {
    return window;
  }

  encodeURI(uri: string): string {
    return encodeURI(uri);
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { appRoutes } from '../../constants/app-routes';
import { User } from '../../modules/user/models/user';
import { PermissionsService } from '../../modules/user/services/permissions.service';
import { UserService } from '../../modules/user/services/user.service';

@Component({
  selector: 'nuarch-redirect-component',
  templateUrl: './home-redirect.component.html',
  styleUrls: ['./home-redirect.component.scss'],
})
export class HomeRedirectComponent implements OnInit {

  constructor(private router: Router,
              private userService: UserService,
              private permissionsService: PermissionsService) {
  }

  ngOnInit(): void {
    this.userService.user
    .pipe(take(1))
    .subscribe((user: User) => {
      let permissions: string[] = this.permissionsService.permissions(user);
      this.redirect(permissions);
    });
  }

  redirect(permissions: string[]): void {
    this.router.navigate([appRoutes.laws]);
  }

}

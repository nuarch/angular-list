import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { anything, capture, instance, mock, when } from 'ts-mockito';

import { appRoutes } from '../../constants/app-routes';
import { CAN_READ_NOVAS_DATA } from '../../constants/security-matrix';
import { User } from '../../modules/user/models/user';
import { PermissionsService } from '../../modules/user/services/permissions.service';
import { UserService } from '../../modules/user/services/user.service';
import { HomeRedirectComponent } from './home-redirect.component';

describe('RedirectComponentComponent', () => {
  let component: HomeRedirectComponent;
  let fixture: ComponentFixture<HomeRedirectComponent>;

  const user: User = new User({ username: 'test', authoritiesAsStrings: ['ROLE_DSNYSMT_EIS_ADMIN'] });
  const mockUserService: UserService = mock(UserService);
  when(mockUserService.user).thenReturn(of(user));

  const mockRouter: Router = mock(Router);
  const router: Router = instance(mockRouter);

  const mockPermissionsService: PermissionsService = mock(PermissionsService);
  when(mockPermissionsService.permissions(anything())).thenReturn([CAN_READ_NOVAS_DATA]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [HomeRedirectComponent],
      imports: [
        MatProgressSpinnerModule,
      ],
      providers: [
        { provide: UserService, useFactory: () => instance(mockUserService) },
        { provide: Router, useValue: router },
        { provide: PermissionsService, useFactory: () => instance(mockPermissionsService) },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeRedirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();

  });

  it('should redirect to personnel', () => {
    const permissions: string[] = [CAN_READ_NOVAS_DATA];
    component.redirect(permissions);
    const [arg] = capture(mockRouter.navigate).last();
    expect(arg[0]).toEqual(appRoutes.laws);
  });
});

import { Component, Input, OnInit, HostBinding } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { slideInUpAnimation } from '../../app.animations';

@Component({
  selector: 'nu-readme-loader',
  styleUrls: ['./readme-loader.component.scss'],
  templateUrl: './readme-loader.component.html',
  animations: [slideInUpAnimation],
})
export class NuReadmeLoaderComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation: boolean = true;
  @HostBinding('class.nu-route-animation') classAnimation: boolean = true;

  @Input() resourceUrl: string;

  constructor(private _route: ActivatedRoute) {}

  ngOnInit(): void {
    if (this.resourceUrl) {
      return;
    }

    this.resourceUrl = `assets/demos/${this._route.snapshot.data.componentName}/README.md`;
  }
}

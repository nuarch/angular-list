import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-content-details',
  templateUrl: './content-details.component.html',
  styleUrls: ['./content-details.component.scss'],
})
export class ContentDetailsComponent implements OnInit {
  componentName: string;
  componentDescription: string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  navLinks: any = [
    {
      name: 'Overview',
      route: 'overview',
      alwaysShow: true,
    },
    {
      name: 'Api',
      route: 'api',
      alwaysShow: true,
    },
    {
      name: 'Examples',
      route: 'examples',
      alwaysShow: false,
    }
  ];

  constructor(private _route: ActivatedRoute) {}

  ngOnInit(): void {
    this.componentName = this._route.snapshot.data.componentName;
    this.componentDescription = this._route.snapshot.data.componentDescription;
  }
}

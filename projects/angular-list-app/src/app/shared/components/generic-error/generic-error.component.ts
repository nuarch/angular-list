import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, UrlSegment } from '@angular/router';
import { forEach } from 'lodash';
import { takeUntil } from 'rxjs/operators';
import { AbstractComponent } from '../abstract-component/abstract-component';

@Component({
  selector: 'nuarch-generic-error',
  templateUrl: './generic-error.component.html',
  styleUrls: ['./generic-error.component.scss'],
})
export class GenericErrorComponent extends AbstractComponent implements OnInit {

  readonly networkErrorTitle: string = 'Network Error';
  readonly networkErrorMessage: string = 'Network Error! Please try again later. If problem persists, please contact the help desk for support at';

  readonly accessDeniedErrorTitle: string = '403 - Access Denied';
  readonly accessDeniedErrorMessage: string =
    'You are unable to view this page due to your current role. Please contact the help desk for support at';

  readonly genericErrorTitle: string = 'An error occurs';
  readonly genericErrorMessage: string = 'An error occurs, please contact the help desk for support at';

  readonly error: string = 'error';
  readonly deny: string = 'deny';

  message: string;
  title: string;

  constructor(private route: ActivatedRoute,
              private router: Router) {
    super();
  }

  ngOnInit(): void {
    this.route.url
    .pipe(takeUntil(this.componentDestroyed))
    .subscribe((urlSegments: UrlSegment[]) => {
      forEach(urlSegments, (segment: UrlSegment) => {
        if (segment.path === this.error) {
          this.message = this.networkErrorMessage;
          this.title = this.networkErrorTitle;
        } else if (segment.path === this.deny) {
          this.message = this.accessDeniedErrorMessage;
          this.title = this.accessDeniedErrorTitle;
        } else {
          this.message = this.genericErrorMessage;
          this.title = this.genericErrorTitle;
        }
      });
    });
  }

  toHomepage(): void {
    this.router.navigate(['/']);
  }

  afterOnDestroy(): void {
    // after on destroy
  }

}

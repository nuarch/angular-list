import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { capture, instance, mock } from 'ts-mockito';

import { GenericErrorComponent } from './generic-error.component';
import { configureTestSuite } from '../../functions/configure-test-suite';

describe('GenericErrorComponent', () => {
  let component: GenericErrorComponent;
  let fixture: ComponentFixture<GenericErrorComponent>;
  let mockRouter: Router = mock(Router);
  let router: Router = instance(mockRouter);

  configureTestSuite();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  beforeAll((done: any) => (async () => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatCardModule,
      ],
      declarations: [GenericErrorComponent],
    });

    await TestBed.compileComponents();
  })().then(done).catch(done.fail));

  it('should create', () => {
    fixture = TestBed.createComponent(GenericErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should navigate to homepage', () => {
    TestBed.overrideProvider(ActivatedRoute, { useValue: { url: of([{ path: 'error' }]) } })
    .overrideProvider(Router, { useValue: router })
    .compileComponents();

    fixture = TestBed.createComponent(GenericErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.toHomepage();
    const [arg] = capture(mockRouter.navigate).last();
    expect(arg[0]).toEqual('/');
  });

  it('should handel net work error', async(() => {
    TestBed.overrideProvider(ActivatedRoute, {
      useValue: {
        url: of([{ path: 'error' }]),
      },
    })
    .compileComponents();
    fixture = TestBed.createComponent(GenericErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component.message).toBe(component.networkErrorMessage);
    expect(component.title).toBe(component.networkErrorTitle);
  }));

  it('should handel access denied error', async(() => {
    TestBed.overrideProvider(ActivatedRoute, {
      useValue: {
        url: of([{ path: 'deny' }]),
      },
    })
    .compileComponents();
    fixture = TestBed.createComponent(GenericErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component.message).toBe(component.accessDeniedErrorMessage);
    expect(component.title).toBe(component.accessDeniedErrorTitle);
  }));

  it('should handel other error', async(() => {
    TestBed.overrideProvider(ActivatedRoute, {
      useValue: {
        url: of([{ path: 'other' }]),
      },
    })
    .compileComponents();
    fixture = TestBed.createComponent(GenericErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component.message).toBe(component.genericErrorMessage);
    expect(component.title).toBe(component.genericErrorTitle);
  }));

});

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NavigationLayoutComponent } from './navigation-layout.component';
import { MatDividerModule } from '@angular/material/divider';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgxPermissionsAllowStubDirective } from 'ngx-permissions';

import { configureTestSuite } from '../../functions/configure-test-suite';
import { UserService } from '../../modules/user/services/user.service';

describe('NavigationLayoutComponent', () => {
  let component: NavigationLayoutComponent;
  let fixture: ComponentFixture<NavigationLayoutComponent>;

  configureTestSuite();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  beforeAll((done: any) => (async () => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        RouterTestingModule,
        MatDividerModule,
        MatSidenavModule,
        MatIconModule,
        MatToolbarModule,
        MatSidenavModule,
        MatListModule,
        FlexLayoutModule,
        NoopAnimationsModule,
      ],
      declarations: [
        NavigationLayoutComponent,
        NgxPermissionsAllowStubDirective,
      ],
      providers: [
        {
          provide: UserService,
          useValue: {
            // eslint-disable-next-line no-empty, no-empty-function, @typescript-eslint/no-empty-function
            logout: () => {
            },
          },
        },
      ],
    });

    await TestBed.compileComponents();
  })().then(done).catch(done.fail));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationLayoutComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

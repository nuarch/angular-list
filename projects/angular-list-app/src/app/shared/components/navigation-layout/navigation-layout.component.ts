import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { UserService } from '../../modules/user/services/user.service';

@Component({
  selector: 'nuarch-navigation-layout',
  templateUrl: './navigation-layout.component.html',
  styleUrls: ['./navigation-layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigationLayoutComponent {

  showText: boolean = false;

  @ViewChild(MatSidenav, { static: false }) sidenav: MatSidenav;

  constructor(private userService: UserService) {
  }

  logout(): void {
    this.userService.logout();
  }

  home(): void {
    this.userService.home();
  }

  toggleText(): void {
    this.showText = !this.showText;
  }

}

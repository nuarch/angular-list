import { MatSnackBar } from '@angular/material/typings';
import { Router } from '@angular/router';
import { get, join } from 'lodash';

import { ErrorCode } from '../constants/error-code';
import { errorRoute } from '../constants/error-route';
import { mapServerErrorMessages } from './map-server-error-messages';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function handleLoadFail(error: any, snackbar: MatSnackBar, router: Router): void {
  if (get(error, 'errorCode') === ErrorCode.ServerError) {
    snackbar.open(join(mapServerErrorMessages(error), ','), 'close', { panelClass: ['warn-snackbar'] });
  } else {
    router.navigate([errorRoute]);
  }
}

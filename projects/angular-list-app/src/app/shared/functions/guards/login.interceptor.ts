import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalReferenceService } from '../../services/global-reference.service';
import * as _ from 'lodash';
import { EMPTY, Observable, of, throwError } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class LoginInterceptor implements HttpInterceptor {

  constructor(private globalReference: GlobalReferenceService) {
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      switchMap((response: any) => {
        if (response instanceof HttpErrorResponse) {
          let message: string = _.get(response, 'message', '');
          let url: string = _.get(response, 'url', '');

          if (url.indexOf('login') > -1 || message.indexOf('<html>') > -1) {
            let href: string = _.get(this.globalReference.nativeWindow.location, 'href', '');
            if (href.indexOf('localhost') > -1) {
              this.globalReference.nativeWindow.location.href = '/login';
            } else {
              this.globalReference.nativeWindow.location.reload();
            }

            return EMPTY;
          }

          try {
            if (response) {
              if (_.isObject(response.error)) {
                return throwError(response.error);
              }
            }

            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            let json: any = JSON.parse(_.get(response, 'error', '{}'));
            return throwError(json);

          } catch (e) {
            return EMPTY;
          }
        }
        return of(response);
      }),
    );

  }
}

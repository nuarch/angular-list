import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { User } from '../../modules/user/models/user';
import { UserService } from '../../modules/user/services/user.service';
import { switchMap, take } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private userService: UserService,
              private router: Router) {
  }

  isLoggedIn(): Observable<boolean> {
    return this.userService.getUser().pipe(
      take(1),
      switchMap((user: User) => {
        if (!user || !user.loggedIn) {
          return this.userService.handleError();
        }
        return of(true);
      }),
    );
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.isLoggedIn();
  }

}

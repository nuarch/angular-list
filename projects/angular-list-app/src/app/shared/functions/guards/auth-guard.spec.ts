import { async, TestBed } from '@angular/core/testing';
import { AuthGuard } from './auth-guard';
import { UserService } from '../../modules/user/services/user.service';
import { instance, mock } from 'ts-mockito/lib/ts-mockito';
import { Router } from '@angular/router';
import { User } from '../../modules/user/models/user';
import { of } from 'rxjs';

xdescribe('AuthGuard', () => {
  let authGuard: AuthGuard;
  let userService: UserService;

  const mockRouter: Router = mock(Router);
  const router: Router = instance(mockRouter);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthGuard,
        {
          provide: Router,
          useValue: router,
        },
        {
          provide: UserService,
          useValue: {
            handleError: () => of(false),
          },
        },
      ],
    });
    authGuard = TestBed.get(AuthGuard);
    userService = TestBed.get(UserService);
  }));

  it('isLoggedIn: false', () => {
    userService.user = of(new User({}));
    authGuard.isLoggedIn().subscribe((isLoggedIn: boolean) => {
      expect(isLoggedIn).toBeFalsy();
    });
  });

  it('isLoggedIn: false', () => {
    userService.user = of(undefined);
    authGuard.isLoggedIn().subscribe((isLoggedIn: boolean) => {
      expect(isLoggedIn).toBeFalsy();
    });
  });

});

export const defaultSortBy: string = 'id';
export const defaultSortOrder: string = 'asc';

export class Sort {
  sort: string = defaultSortBy;
  dir: string = defaultSortOrder;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(data?: any) {
    if (data) {
      this.sort = data.sort;
      this.dir = data.dir;
    }
  }

}

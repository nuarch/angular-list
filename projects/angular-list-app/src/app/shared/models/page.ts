import { PageInfo } from './page-info';

export interface Page<T> {
  pageInfo: PageInfo,
  laws: T[],
}

import { Sort } from './sort';

export interface PageInfo {
  page: number;
  size: number;
  sort?: Sort;
  search?: string;
}

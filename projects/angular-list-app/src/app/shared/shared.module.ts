import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { RouterModule } from '@angular/router';
import { NgxPermissionsModule } from 'ngx-permissions';
import { NovasMaterialModule } from './modules/material.module';

import { GlobalReferenceService } from './services/global-reference.service';
import { AuthGuard } from './functions/guards/auth-guard';
import { NavigationLayoutComponent } from './components/navigation-layout/navigation-layout.component';
import { HomeRedirectComponent } from './components/home-redirect/home-redirect.component';
import { GenericErrorComponent } from './components/generic-error/generic-error.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DemoComponent } from './components/demo-tools/demo.component';
import { ContentDetailsComponent } from './components/component-details/content-details.component';
import { NuReadmeLoaderComponent } from './components/readme-loader/readme-loader.component';
import { CovalentFlavoredMarkdownModule } from '@covalent/flavored-markdown';
import { CovalentHighlightModule } from '@covalent/highlight';
import { CovalentMarkdownModule } from '@covalent/markdown';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const NUARCH_MODULES: any[] = [];

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const COMPONENTS: any[] = [
  NavigationLayoutComponent,
  HomeRedirectComponent,

];

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const PIPES: any[] = [];

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    NUARCH_MODULES,
    NovasMaterialModule,
    RouterModule,
    NgxPermissionsModule.forChild(),
    FontAwesomeModule,
    CovalentFlavoredMarkdownModule,
    CovalentHighlightModule,
    CovalentMarkdownModule,
  ],
  exports: [
    FlexLayoutModule,
    NUARCH_MODULES,
    NgxPermissionsModule,
    COMPONENTS,
    PIPES,
    NovasMaterialModule,
    FontAwesomeModule,
    DemoComponent,
    ContentDetailsComponent,
    NuReadmeLoaderComponent,
  ],
  declarations: [
    COMPONENTS,
    PIPES,
    GenericErrorComponent,
    DemoComponent,
    ContentDetailsComponent,
    NuReadmeLoaderComponent,
  ],
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [
        GlobalReferenceService,
        AuthGuard,
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
      ],
    };
  }

  static forChild(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
      ],
    };
  }
}

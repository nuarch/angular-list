// ladp roles

export const ADMINS: string = 'ROLE_ADMINS';
export const DSNYSMT_ADMINS: string = 'ROLE_DSNYSMT_ADMINS';

export const LDAP_ROLES_WITH_RULES: string[] = [ADMINS, DSNYSMT_ADMINS];

// app roles
export const NOVAS_ADMIN: string = 'NOVAS_ADMIN';
export const NOVAS_WRITER: string = 'NOVAS_WRITER';
export const NOVAS_READER: string = 'NOVAS_READER';

export const ROLES: { [ROLE: string]: string[] } = {
  [NOVAS_ADMIN]: [ADMINS, DSNYSMT_ADMINS, NOVAS_ADMIN],
  [NOVAS_WRITER]: [NOVAS_WRITER],
  [NOVAS_READER]: [NOVAS_READER],
};

// permissions

// core permissions
export const CAN_EDIT_NOVAS_DATA: string = 'canEditNovasData';
export const CAN_READ_NOVAS_DATA: string = 'canReadNovasData';

// matrix
export const RULES: { [ROLE: string]: string[] } = {
  [NOVAS_ADMIN]: [CAN_EDIT_NOVAS_DATA, CAN_READ_NOVAS_DATA],
};

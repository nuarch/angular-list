export const tooltipPosition: string = 'above';
export const historyTooltip: string = 'Open Historical Records';
export const futureTooltip: string = 'Open Future Records';
export const pageSizeOptions: number[] = [5, 10, 20, 40, 100];
export const submitButtonLabel: string = 'Apply Filters';
export const cancelButtonLabel: string = 'Back to Search';
export const FILTER: string = 'filter';
export const sectionTitle: string = 'Advance Filters';

import { PageInfo } from '../models/page-info';
import { defaultPageSize } from './default-page-size';

export const defaultPageInfo: PageInfo = {
  page: 0,
  size: defaultPageSize,
  sort: undefined,
};

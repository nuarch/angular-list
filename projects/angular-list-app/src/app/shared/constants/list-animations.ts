import { animate, AnimationTriggerMetadata, AUTO_STYLE, state, style, transition, trigger } from '@angular/animations';

export const listAnimations: {
  readonly slideOutSearch: AnimationTriggerMetadata;
} = {
  slideOutSearch: trigger('inputState', [
    state('0', style({
      width: '0%',
      margin: '0px',
    })),
    state('1', style({
      width: '100%',
      margin: AUTO_STYLE,
    })),
    transition('0 => 1', animate('200ms ease-in')),
    transition('1 => 0', animate('200ms ease-out')),
  ]),
};

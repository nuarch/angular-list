import { Action } from '@ngrx/store';
import { User } from '../models/user';
import { keys, map } from 'lodash';

export enum UserActionTypes {

  LoadUser = '[User] Load',
  LoadUserSuccess = '[User] Load Success',
  LoadUserFail = '[User] Load Fail',
  DenyUserAccess = '[User] Deny Access',
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const successActionTypes: any[] = actionTypesByNameContains('Success');

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const failActionTypes: any[] = actionTypesByNameContains('Fail');

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function actionTypesByNameContains(name: string): any[] {
  let actionTypes: string[] = keys(UserActionTypes);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return map(actionTypes.filter((k: string) => k.indexOf(name) > -1), (s: keyof typeof UserActionTypes) => UserActionTypes[s]);
}

export class LoadUserAction implements Action {
  readonly type: string = UserActionTypes.LoadUser;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public payload?: any) {

  }
}

export class LoadUserSuccessAction implements Action {
  readonly type: string = UserActionTypes.LoadUserSuccess;

  constructor(public payload: User) {
  }
}

export class LoadUserFailAction implements Action {
  readonly type: string = UserActionTypes.LoadUserFail;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public payload?: any) {

  }
}

export class DenyUserAccessAction implements Action {
  readonly type: string = UserActionTypes.DenyUserAccess;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public payload?: any) {

  }
}

export type UserActions = LoadUserAction
  | LoadUserSuccessAction
  | LoadUserFailAction
  | DenyUserAccessAction;

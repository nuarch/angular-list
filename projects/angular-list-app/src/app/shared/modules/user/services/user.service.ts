import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { GlobalReferenceService } from '../../../services/global-reference.service';
import { User } from '../models/user';
import * as fromUser from '../reducers/index';

@Injectable()
export class UserService {

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  readonly routes: any = {
    user: '/me',
    deny: '/deny',
    logout: '/logout',
    static: {
      user: 'data/user.json',
    },
  };
  user: Observable<User>;

  constructor(private http: HttpClient,
              private store: Store<fromUser.State>,
              private router: Router,
              private globalReference: GlobalReferenceService) {
    this.user = store.pipe(select(fromUser.getSelectedUser));
  }

  public getUser(): Observable<User> {
    // TODO change back when using edge
    // return this.http.get(`${this.routes.user}`, { params: undefined })
    // .pipe(
    // eslint-disable-next-line 
    //   map((data: any) => {
    //     return new User(data.user);
    //   }),
    // );

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let fake: any = {
      user: {
        username: 'vgellerman',
        authorizedLocations: [ 'ENFCW', 'CTU', 'SIBO', 'BKS14', 'HQLCU', 'BKS13', 'BKS15', 'EPU', 'ENFTR', 'BKS10', 'BKS12',
          'BKS11', 'BKLCU', 'QE12', 'ACCO', 'QE11', 'QE07B', 'QE14', 'COMM', 'QE13', 'QE07A', 'BKS18', 'FIAT', 'QE10', 'QE11A', 'SILCU',
          'QNLCU', 'MNLCU', 'MN04A', 'FKLLB', 'BKN02', 'BKN01', 'ENFSI', 'ENFBX', 'OMD', 'DOT', 'BKN08', 'BKN09', 'BKN04', 'BKS1549',
          'BKN03', 'BKN05', 'BX06A', 'EXPQN', 'PICA', 'BKSBO', 'MTSNS', 'QE11W', 'ENFBK', 'QE08', 'QE12A', 'MN03A', 'BXBO', 'QE07',
          'MN03', 'QE13J', 'MN04', 'MN05', 'BX03A', 'MN06', 'QEBO', 'MN01', 'MN02', 'DPR', 'MN07', 'MN08', 'BKS15KN', 'MN09', 'MTS59',
          'WARE', 'FKILL', 'SPLQN', 'TRCKM', 'BXLCU', 'ENFQN', 'QW05A', 'OTHER', 'SITS', 'BKS07', 'QE13W', 'BKS06', 'ENFHQ', 'BKS06A',
          'PIU', 'QW01', 'MN11A', 'SWMHQ', 'QW09', 'MNBO', 'FDCO', 'QW06', 'QW04', 'QW05', 'SAFTY', 'QW02', 'QW03', 'TRNG', 'SPLSI', 'SPLBX',
          'MEDIC', 'BOO', 'MN10', 'MN11', 'MN12', 'EXPMN', 'SPLBN', 'MTSBK', 'REAL', 'MTS91', 'BX09', 'QSLCU', 'BX08', 'OAU', 'BX07', 'BX06',
          'BX05', 'BX04', 'BX03', 'WMENG', 'BX02', 'BX01', 'L444', 'BKN17', 'BKN16', 'PASSG', 'GPSSUP', 'QWBO', 'LEGAL', 'BBMBO', 'SPAGN',
          'SI01J', 'MTSHA', 'BMEBO', 'SBAT', 'EXPSI', 'PMD', 'EXPBX', 'BX12', 'BX11', 'L831', 'BX10', 'BIT', 'MAINO', 'DEP', 'ENFMN', 'SI01',
          'COLL', 'SWMREC', 'BKNBO', 'SI01A', 'OCO', 'CLEAN', 'OEDM', 'DVO', 'SI02', 'SI03', 'XPMGT' ],
        authoritiesAsStrings: [ 'ROLE_DOITT_VPN_USERS', 'ROLE_DSNYSMT_ADMINS', 'ROLE_DSNYSMT_DEV', 'ROLE_DSNY_CCEWIN_USERS',
          'ROLE_DSNYSMT_TASKS_LEAD', 'ROLE_DSNYSMT_RANCHER_SI_DEV', 'ROLE_DSNYSMT_CORE_LEAD', 'ROLE_DSNYSMT_RANCHER_SI' ],
        enabled: true,
        authorities: [ {
          authority: 'ROLE_DOITT_VPN_USERS',
        }, {
          authority: 'ROLE_DSNYSMT_ADMINS',
        }, {
          authority: 'ROLE_DSNYSMT_DEV',
        }, {
          authority: 'ROLE_DSNYSMT_TASKS_LEAD',
        }, {
          authority: 'ROLE_DSNYSMT_CORE_LEAD',
        }, {
          authority: 'ROLE_DSNYSMT_RANCHER_SI',
        }, {
          authority: 'ROLE_DSNYSMT_RANCHER_SI_DEV',
        }, {
          authority: 'ROLE_DSNY_CCEWIN_USERS',
        } ],
        accountNonLocked: true,
        credentialsNonExpired: true,
        n: 'cn=vgellerman,ou=USERS,dc=DSNY,o=nycnet',
        accountNonExpired: true,
        admin: true,
        hq: false,
        gpsadmin: false,
        emsadmin: false,
        allBoros: false,
        boro: false,
        district: false,
        name: 'vgellerman',
      },
    };

    let user: User = new User(fake.user);
    return of(user);
  }

  public handleError(): Observable<boolean> {
    this.globalReference.nativeWindow.location.href = '/login';
    return of(false);
  }

  public home(): void {
    this.globalReference.nativeWindow.location.href = '/';
  }

  public logout(): void {
    this.globalReference.nativeWindow.location.href = '/login?logout';
  }

}

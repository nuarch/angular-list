import { inject, TestBed } from '@angular/core/testing';

import { PermissionsService } from './permissions.service';
import { User } from '../models/user';

describe('PermissionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PermissionsService,
      ],
    });
  });

  it('should be created', inject([PermissionsService], (service: PermissionsService) => {
    expect(service).toBeTruthy();
  }));

  it('should return empty', inject([PermissionsService], (service: PermissionsService) => {
    expect(service.permissions(new User({ authoritiesAsStrings: [''] }))).toEqual([]);
  }));

  // xit('should return permissions', inject([PermissionsService], (service: PermissionsService) => {
  //   expect(service.permissions(new User({authoritiesAsStrings: [ROLE_ADMINS]}))).toEqual([CAN_EDIT_PERSONNEL_DATA, CAN_EDIT_JOB_DATA]);
  // }));
});

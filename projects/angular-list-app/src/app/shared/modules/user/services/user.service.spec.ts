import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { instance, mock, reset } from 'ts-mockito';

import { GlobalReferenceService } from '../../../services/global-reference.service';
import { User } from '../models/user';

import * as fromUser from '../reducers/index';
import { UserService } from './user.service';

describe('User Service', () => {
  let mockStore: Store<fromUser.State> = mock(Store);
  let store: Store<fromUser.State> = instance(mockStore);

  const mockRouter: Router = mock(Router);
  const router: Router = instance(mockRouter);

  const mockGlobalReferenceService: GlobalReferenceService = mock(GlobalReferenceService);
  const globalReference: GlobalReferenceService = instance(mockGlobalReferenceService);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        UserService,
        { provide: Store, useValue: store },
        { provide: Router, useValue: router },
        { provide: GlobalReferenceService, useValue: globalReference },
      ],
    });
    reset(mockStore);
  });

  it('should be created', inject([UserService], (service: UserService) => {
    expect(service).toBeTruthy();
  }));

  // it('should get user', inject([UserService, HttpTestingController], (service: UserService, httpMock: HttpTestingController) => {
  //   service.getUser()
  //   .subscribe((user: User) => {
  //     expect(user).toBeDefined();
  //     expect(user).toEqual(new User({ username: 'John' }));
  //   });

  // eslint-disable-next-line
  //   const testReq: TestRequest = httpMock.expectOne((req: any) => true);
  //   testReq.flush({ user: { username: 'John' } });
  //   httpMock.verify();
  // }));

});

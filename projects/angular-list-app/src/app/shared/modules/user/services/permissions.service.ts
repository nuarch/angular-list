import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { flatMap, get, intersection, isEmpty, keys, omitBy } from 'lodash';
import { LDAP_ROLES_WITH_RULES, ROLES, RULES } from '../../../constants/security-matrix';

@Injectable()
export class PermissionsService {

  permissions(user: User): string[] {
    return flatMap(this.roles(user), (role: string) => get(RULES, role));
  }

  ldapRoles(user: User): string[] {
    return intersection(LDAP_ROLES_WITH_RULES, user.authorities);
  }

  roles(user: User): string[] {
    return keys(omitBy(ROLES, (roles: string[]) => isEmpty(intersection(roles, this.ldapRoles(user)))));
  }

}

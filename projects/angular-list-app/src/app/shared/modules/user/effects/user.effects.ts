import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { LoadUserAction, LoadUserFailAction, LoadUserSuccessAction, UserActionTypes, } from '../actions/user.actions';
import { User } from '../models/user';
import { UserService } from '../services/user.service';

@Injectable()
export class UserEffects {

  @Effect()
  loadUser$: Observable<Action> = this.actions$.pipe(
    ofType<LoadUserAction>(UserActionTypes.LoadUser),
    startWith(new LoadUserAction(undefined)),
    switchMap(() => {
      return this.userService.getUser()
      .pipe(
        map((user: User) => new LoadUserSuccessAction(user)),
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        catchError((err: any) => of(new LoadUserFailAction(err))),
      );
    }),
  );

  constructor(private actions$: Actions,
              private userService: UserService) {

  }
}

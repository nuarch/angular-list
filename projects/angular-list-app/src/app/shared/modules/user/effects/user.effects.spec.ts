import { CommonModule } from '@angular/common';
import { TestBed } from '@angular/core/testing';
import { EMPTY, Observable, of } from 'rxjs';
import { instance, mock, reset, when } from 'ts-mockito/lib/ts-mockito';
import { Router } from '@angular/router';
import moment from 'moment';
import { Actions } from '@ngrx/effects';
import { LoadUserSuccessAction } from '../actions/user.actions';
import { User } from '../models/user';
import { UserEffects } from './user.effects';
import { UserService } from '../services/user.service';
import { Action } from '@ngrx/store';
import { get } from 'lodash';

export class TestActions extends Actions {
  /* eslint-disable @typescript-eslint/no-explicit-any */
  source: any;

  constructor() {
    super(EMPTY);
  }

  /* eslint-disable @typescript-eslint/no-explicit-any */
  set stream(source: Observable<any>) {
    this.source = source;
  }
}

export function getActions(): TestActions {
  return new TestActions();
}

describe('Effects: UserEffects', () => {
  let effects: UserEffects;
  let actions: TestActions;

  const mockUserService: UserService = mock(UserService);
  const userService: UserService = instance(mockUserService);

  const mockRouter: Router = mock(Router);
  const router: Router = instance(mockRouter);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
      ],
      declarations: [],
      providers: [
        UserEffects,
        { provide: UserService, useValue: userService },
        { provide: Router, useValue: router },
        { provide: Actions, useFactory: getActions },
      ],
    });

    effects = TestBed.get(UserEffects);
    actions = TestBed.get(Actions);
    reset(mockRouter);
  });

  it('loadUser - should return a LoadUsersSuccess action, on success', () => {
    const user: User = new User({ username: 'a' });
    const outcome: LoadUserSuccessAction = new LoadUserSuccessAction(user);
    when(mockUserService.getUser()).thenReturn(of(user));
    effects.loadUser$.subscribe((data: Action) => {
      expect(get(data, 'payload.userName')).toBeTruthy();
    });
  });

});

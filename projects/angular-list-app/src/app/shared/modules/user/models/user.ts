import { get, isEmpty } from 'lodash';

export class User {
  userName: string;
  authorities: string[];
  workforceId: string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(user: any) {
    this.userName = user.username;
    this.workforceId = user.workforceId;
    this.authorities = get(user, 'authoritiesAsStrings', get(user, 'authorities'));
  }

  get loggedIn(): boolean {
    return (this.userName && !isEmpty(this.authorities));
  }

  static testUser(): User {
    return new User({
      username: 'test',
      workforceId: '12345',
      authorities: ['DSNYSMT_ADMINS'],
    });
  }

}

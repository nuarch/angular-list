import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserService } from './services/user.service';
import { UserEffects } from './effects/user.effects';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { reducers } from './reducers';
import { PermissionsService } from './services/permissions.service';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('user', reducers),
    EffectsModule.forFeature([
      UserEffects,
    ]),
  ],
  providers: [
    UserService,
    PermissionsService,
  ],
})

export class UserModule {
}

/* eslint-disable */
import { EntitySelectors } from '@ngrx/entity/src/models';
import { ActionReducerMap, createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';

import { User } from '../models/user';
import * as fromUser from './user.reducer';

export interface UserState {
  user: fromUser.LocalState;
}

export interface State {
  user: UserState;
}

export const reducers: ActionReducerMap<UserState> = {
  user: fromUser.reducer,
};

export const getUserState: MemoizedSelector<any, any> = createFeatureSelector<UserState>('user');

export const getUserEntitiesState: MemoizedSelector<any, any> = createSelector(getUserState, (state: UserState) => state.user);

export const getSelectedUserId: MemoizedSelector<any, any> = createSelector(getUserEntitiesState, fromUser.getSelectedUserId);
export const getLoadingUser: MemoizedSelector<any, any> = createSelector(getUserEntitiesState, fromUser.getLoadingUser);
export const getUserErrorMessages: MemoizedSelector<any, any> = createSelector(getUserEntitiesState, fromUser.getUserErrorMessages);

export const {
  selectIds: getUserIds,
  selectEntities: getUserEntities,
  selectAll: getAllUser,
}: EntitySelectors<User, State> = fromUser.adapter.getSelectors(getUserEntitiesState);

export const getSelectedUser: MemoizedSelector<any, any> = createSelector(
  getUserEntities,
  getSelectedUserId,
  (userEntities, selectedUserId) => {
    return selectedUserId && userEntities[selectedUserId];
  },
);

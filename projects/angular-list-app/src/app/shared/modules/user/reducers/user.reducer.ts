import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { assign, get } from 'lodash';
import * as userActions from '../actions/user.actions';
import { User } from '../models/user';
import { mapServerErrorMessages } from '../../../functions/map-server-error-messages';

export interface LocalState extends EntityState<User> {
  selectedUserId: string;
  loadingUser: boolean;
  errorMessages: string[];
}

export const adapter: EntityAdapter<User> = createEntityAdapter<User>({
  selectId: (user: User) => user.userName,
  sortComparer: false,
});

export const initialState: LocalState = adapter.getInitialState({
  selectedUserId: undefined,
  loadingUser: false,
  errorMessages: undefined,
});

export function reducer(state: LocalState = initialState, action: userActions.UserActions): LocalState {
  switch (action.type) {
    case userActions.UserActionTypes.LoadUser: {
      return assign({}, state, {
        loadingUser: true,
      });
    }
    case userActions.UserActionTypes.LoadUserSuccess: {
      const actOnState: LocalState = adapter.removeAll(state);
      return adapter.addOne(action.payload, {
        ...actOnState,
        selectedUserId: action.payload.userName,
        loadingUser: false,
        errorMessages: undefined,
      });
    }
    case userActions.UserActionTypes.LoadUserFail: {
      const existingErrors: string[] = get(state, 'errorMessages', []);
      const actOnState: LocalState = adapter.removeAll(state);

      // TODO remove this, temporary
      return adapter.addOne(User.testUser(),
        {
          ...actOnState,
          selectedUserId: action.payload.userName,
          loadingUser: false,
          errorMessages: [
              ...existingErrors,
              ...mapServerErrorMessages(action.payload.error),
            ],
        });
    }
    default: {
      return state;
    }
  }
}

/* eslint-disable @typescript-eslint/no-explicit-any */
export const getSelectedUserId: any = (state: LocalState) => state.selectedUserId;
export const getLoadingUser: any = (state: LocalState) => state.loadingUser;
export const getUserErrorMessages: any = (state: LocalState) => state.errorMessages;

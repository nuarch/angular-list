import { createEntityAdapter, EntityAdapter } from '@ngrx/entity';

import { LoadUserAction, LoadUserFailAction, LoadUserSuccessAction, } from '../actions/user.actions';
import { User } from '../models/user';
import { LocalState, reducer } from './user.reducer';

describe('User Reducer', () => {
  let adapter: EntityAdapter<User>;
  const personnelId: string = '00034561';

  beforeEach(() => {
    adapter = createEntityAdapter(createEntityAdapter<User>({
      selectId: (user: User) => user.userName,
      sortComparer: false,
    }));
  });

  it('should let you get the initial state', () => {
    const initialState: LocalState = adapter.getInitialState({
      selectedUserId: undefined,
      loadingUser: false,
      errorMessages: undefined,
    });
    expect(initialState).toEqual({
      ids: [],
      entities: {},
      selectedUserId: undefined,
      loadingUser: false,
      errorMessages: undefined,
    });
  });

  it('should Load User', () => {
    const initialState: LocalState = adapter.getInitialState({
      selectedUserId: undefined,
      loadingUser: false,
      errorMessages: undefined,
    });

    const action: LoadUserAction = new LoadUserAction();

    const updatedState: LocalState = reducer(initialState, action);

    expect(updatedState).toEqual({
      ids: [],
      entities: {},
      selectedUserId: undefined,
      loadingUser: true,
      errorMessages: undefined,
    });
  });

  it('should LoadUserSuccess', () => {
    const initialState: LocalState = adapter.getInitialState({
      selectedUserId: undefined,
      loadingUser: false,
      errorMessages: undefined,
    });

    const action: LoadUserSuccessAction = new LoadUserSuccessAction(new User({ username: 'John' }));

    const updatedState: LocalState = reducer(initialState, action);

    expect(updatedState).toEqual({
      ids: ['John'],
      entities: {
        John: new User({ username: 'John' }),
      },
      selectedUserId: 'John',
      loadingUser: false,
      errorMessages: undefined,
    });
  });

  // TODo add back in
  xit('should LoadUserFail', () => {
    const initialState: LocalState = adapter.getInitialState({
      selectedUserId: undefined,
      loadingUser: false,
      errorMessages: undefined,
    });

    const action: LoadUserFailAction = new LoadUserFailAction({ error: { message: 'error' } });

    const updatedState: LocalState = reducer(initialState, action);

    expect(updatedState).toEqual({
      ids: [],
      entities: {},
      selectedUserId: undefined,
      loadingUser: false,
      errorMessages: ['error'],
    });
  });
});

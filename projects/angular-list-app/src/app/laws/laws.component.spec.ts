import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawsComponent } from './laws.component';
import { RouterTestingModule } from '@angular/router/testing';
import { instance, mock } from 'ts-mockito';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReferenceDataService } from '@nuarch/angular-lib';

describe('LawsComponent', () => {
  let component: LawsComponent;
  let fixture: ComponentFixture<LawsComponent>;

  const mockReferenceDataService: ReferenceDataService = mock(ReferenceDataService);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        RouterTestingModule,
      ],
      declarations: [
        LawsComponent,
      ],
      providers: [
        {
          provide: ReferenceDataService,
          useFactory: () => instance(mockReferenceDataService),
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

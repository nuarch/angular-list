import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LawsDetailsComponent } from './components/laws-details/laws-details.component';
import { AuthGuard } from '../shared/functions/guards/auth-guard';
import { LawsComponent } from './laws.component';
import { LawsListModule } from './components/laws-list/laws-list.module';

export const routes: Routes = [
  {
    path: '',
    component: LawsComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'add',
        component: LawsDetailsComponent,
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Add',
        },
      },
      {
        path: ':id/:mode',
        component: LawsDetailsComponent,
        canActivate: [AuthGuard],
        data: {
          breadcrumb: ':id',
        },
      },
      {
        path: ':id',
        component: LawsDetailsComponent,
        canActivate: [AuthGuard],
        data: {
          breadcrumb: ':id',
        },
      },
      {
        path: '**',
        loadChildren: () => LawsListModule,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LawsRoutingModule {

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LawsListComponent } from './components/laws-list/laws-list.component';
import { LawsDetailsComponent } from './components/laws-details/laws-details.component';
import { LawsRoutingModule } from './laws-routing.module';
import { CoreModule, ListModule, DetailsModule } from '@nuarch/angular-lib';
import { SharedModule } from '../shared/shared.module';
import { LawsComponent } from './laws.component';
import { LawsListModule } from './components/laws-list/laws-list.module';

@NgModule({
  imports: [
    CommonModule,
    ListModule,
    DetailsModule,
    SharedModule,
    LawsListModule,
    LawsRoutingModule,
  ],
  declarations: [
    LawsDetailsComponent,
    LawsComponent,
  ],
})
export class LawsModule {
}

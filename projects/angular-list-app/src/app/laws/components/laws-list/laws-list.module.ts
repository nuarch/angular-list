import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule, ListModule, FilterModule } from '@nuarch/angular-lib';
import { SharedModule } from '../../../shared/shared.module';
import { LawsListComponent } from './laws-list.component';
import { LawsListRoutingModule } from './laws-list-routing.module';
import { LawsListOverviewComponent } from './components/laws-list-overview/laws-list-overview.component';
import { LawsListFilterFilterGroupComponent } from './components/demos/laws-list-filter-filter-group/laws-list-filter-filter-group.component';
import { LawsListFilterFilterModelComponent } from './components/demos/laws-list-filter-filter-model/laws-list-filter-filter-model.component';
// eslint-disable-next-line max-len
import { LawsListFilterFilterTemplateComponent } from './components/demos/laws-list-filter-filter-template/laws-list-filter-filter-template.component';
import { LawsListViewModelComponent } from './components/demos/laws-list-view-model/laws-list-view-model.component';
import { LawsListRowTemplateComponent } from './components/demos/laws-list-row-template/laws-list-row-template.component';
import { MatCardModule } from '@angular/material/card';
import { LawsListCardsComponent } from './components/demos/laws-list-cards/laws-list-cards.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { LawsListHeaderWithFilterComponent } from './components/demos/laws-list-header-with-filter/laws-list-header-with-filter.component';
import { LawsListHeaderTemplateWithFilterTemplateComponent } from
    './components/demos/laws-list-header-template-with-filter-template/laws-list-header-template-with-filter-template.component';
import { LawListRouteService } from './services/law-list-route.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,
    ListModule,
    FilterModule,
    LawsListRoutingModule,
    MatCardModule,
    ScrollingModule,
  ],
  declarations: [
    LawsListComponent,
    LawsListOverviewComponent,
    LawsListFilterFilterGroupComponent,
    LawsListFilterFilterModelComponent,
    LawsListFilterFilterTemplateComponent,
    LawsListViewModelComponent,
    LawsListRowTemplateComponent,
    LawsListCardsComponent,
    LawsListHeaderWithFilterComponent,
    LawsListHeaderTemplateWithFilterTemplateComponent,
  ],
  providers: [
    LawListRouteService
  ]
})
export class LawsListModule {
}

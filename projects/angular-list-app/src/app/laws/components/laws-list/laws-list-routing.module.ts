import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LawsListOverviewComponent } from './components/laws-list-overview/laws-list-overview.component';
import { LawsListComponent } from './laws-list.component';
import { ContentDetailsComponent } from '../../../shared/components/component-details/content-details.component';
import { NuReadmeLoaderComponent } from '../../../shared/components/readme-loader/readme-loader.component';

export const routes: Routes = [
  {
    path: '',
    component: ContentDetailsComponent,
    data: {
      componentName: 'Nuarch List',
      // eslint-disable-next-line max-len
      componentDescription: 'Nuarch List draws a view using the configuration provided. Loads the data in the store. Infinite scrolling in built. On sort, searh, filter or scroll to the bottom, automatically makes an api call using the url provided, loads the data in the store and refreshes the list.',
    },
    children: [
      {
        path: 'overview',
        component: LawsListOverviewComponent,
      },
      {
        path: 'api',
        component: NuReadmeLoaderComponent,
        data: {
          componentName: 'laws-list',
        },
      },
      {
        path: 'examples',
        component: LawsListComponent,
      },
      {
        path: '',
        redirectTo: 'overview',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LawsListRoutingModule {

}

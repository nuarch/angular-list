import { Component, TrackByFunction } from '@angular/core';
import { formModel } from '../../../../../models/form-model';
import { viewModel } from '../../../../../models/view-model';
import { get, find, isEqual } from 'lodash';
import { ColumnModel, ReferenceData, ViewModel, FormModel } from '@nuarch/angular-lib';
import { faEdit } from '@fortawesome/free-regular-svg-icons';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { LawListRouteService } from '../../../services/law-list-route.service';

@Component({
  selector: 'nuarch-laws-list-filter-filter-model',
  templateUrl: './laws-list-filter-filter-model.component.html',
  styleUrls: ['./laws-list-filter-filter-model.component.scss'],
})
export class LawsListFilterFilterModelComponent {

  viewModel: ViewModel = viewModel;
  filterModel: FormModel = formModel;
  get: Function = get;
  faEdit: IconDefinition = faEdit;

  constructor(private lawListRedirectService: LawListRouteService) {
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  trackBy: TrackByFunction<string> = (index: number, item: any) => {
    return !!item && item.id;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getReferenceDataValue(row: any, column: ColumnModel, referenceData: ReferenceData): string {
    const value: string = get(row, column.value);
    if (column.referenceData) {
      return get(find(get(referenceData, column.referenceData), (refData: ReferenceData) => {
        return isEqual(get(refData, 'correlationId'), value) || isEqual(get(refData, 'correlation'), value);
      }), 'description');
    }

    return undefined;
  }

  selectRecord($event: MouseEvent, id: number) {
    this.lawListRedirectService.redirectToEdit(id);
  }
}

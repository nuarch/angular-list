import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawsListHeaderTemplateWithFilterTemplateComponent } from './laws-list-header-template-with-filter-template.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CoreService, Entity } from '@nuarch/angular-lib';
import { instance, mock } from 'ts-mockito';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('LawsListHeaderTemplateWithFilterTemplateComponent', () => {
  let component: LawsListHeaderTemplateWithFilterTemplateComponent;
  let fixture: ComponentFixture<LawsListHeaderTemplateWithFilterTemplateComponent>;
  const mockCoreService: CoreService<Entity> = mock(CoreService);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      declarations: [
        LawsListHeaderTemplateWithFilterTemplateComponent,
      ],
      providers: [
        {
          provide: CoreService,
          useFactory: () => instance(mockCoreService),
        },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawsListHeaderTemplateWithFilterTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

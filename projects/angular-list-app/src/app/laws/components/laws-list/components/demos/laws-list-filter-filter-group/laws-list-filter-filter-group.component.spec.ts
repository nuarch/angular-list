import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawsListFilterFilterGroupComponent } from './laws-list-filter-filter-group.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('LawsListFilterFilterGroupComponent', () => {
  let component: LawsListFilterFilterGroupComponent;
  let fixture: ComponentFixture<LawsListFilterFilterGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [LawsListFilterFilterGroupComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawsListFilterFilterGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

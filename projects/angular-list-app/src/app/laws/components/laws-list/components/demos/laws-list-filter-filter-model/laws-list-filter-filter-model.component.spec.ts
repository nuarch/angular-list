import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawsListFilterFilterModelComponent } from './laws-list-filter-filter-model.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';

describe('LawsListFilterFilterModelComponent', () => {
  let component: LawsListFilterFilterModelComponent;
  let fixture: ComponentFixture<LawsListFilterFilterModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FlexLayoutModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [LawsListFilterFilterModelComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawsListFilterFilterModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

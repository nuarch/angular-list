import { Component, OnInit } from '@angular/core';
import { ViewModel } from '@nuarch/angular-lib';
import { viewModel } from '../../../../models/view-model';

@Component({
  selector: 'nuarch-laws-list-overview',
  templateUrl: './laws-list-overview.component.html',
  styleUrls: ['./laws-list-overview.component.scss'],
})
export class LawsListOverviewComponent {
  viewModel: ViewModel = viewModel;
}

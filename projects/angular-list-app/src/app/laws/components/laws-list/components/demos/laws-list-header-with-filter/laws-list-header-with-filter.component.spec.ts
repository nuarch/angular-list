import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawsListHeaderWithFilterComponent } from './laws-list-header-with-filter.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { instance, mock } from 'ts-mockito';
import { CoreService, Entity } from '@nuarch/angular-lib';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('LawsListHeaderWithFilterComponent', () => {
  let component: LawsListHeaderWithFilterComponent;
  let fixture: ComponentFixture<LawsListHeaderWithFilterComponent>;
  const mockCoreService: CoreService<Entity> = mock(CoreService);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      declarations: [
        LawsListHeaderWithFilterComponent,
      ],
      providers: [
        {
          provide: CoreService,
          useFactory: () => instance(mockCoreService),
        },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawsListHeaderWithFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

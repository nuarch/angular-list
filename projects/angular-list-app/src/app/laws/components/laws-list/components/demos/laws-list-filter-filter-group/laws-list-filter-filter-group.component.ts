import { Component, OnInit, TrackByFunction } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { formModel } from '../../../../../models/form-model';
import { viewModel } from '../../../../../models/view-model';
import { get, find, isEqual } from 'lodash';
import { ColumnModel, ReferenceData, ViewModel, FormModel } from '@nuarch/angular-lib';

@Component({
  selector: 'nuarch-laws-list-filter-filter-group',
  templateUrl: './laws-list-filter-filter-group.component.html',
  styleUrls: ['./laws-list-filter-filter-group.component.scss'],
})
export class LawsListFilterFilterGroupComponent {
  viewModel: ViewModel = viewModel;
  filterModel: FormModel = formModel;
  filterGroup: FormGroup;

  get: Function = get;

  constructor(private formBuilder: FormBuilder) {
    this.filterGroup = this.formBuilder.group({
      employeeStatus: ['Active'],
      title: ['Chief II'],
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  trackBy: TrackByFunction<string> = (index: number, item: any) => {
    return !!item && item.id;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getReferenceDataValue(row: any, column: ColumnModel, referenceData: ReferenceData): string {
    const value: string = get(row, column.value);
    if (column.referenceData) {
      return get(find(get(referenceData, column.referenceData), (refData: ReferenceData) => {
        return isEqual(get(refData, 'correlationId'), value) || isEqual(get(refData, 'correlation'), value);
      }), 'description');
    }

    return undefined;
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawsListFilterFilterTemplateComponent } from './laws-list-filter-filter-template.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { instance, mock } from 'ts-mockito';
import { Router } from '@angular/router';

describe('LawsListFilterFilterTemplateComponent', () => {
  let component: LawsListFilterFilterTemplateComponent;
  let fixture: ComponentFixture<LawsListFilterFilterTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule,
        MatSelectModule,
        MatFormFieldModule,
        MatButtonModule,
      ],
      declarations: [
        LawsListFilterFilterTemplateComponent,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawsListFilterFilterTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

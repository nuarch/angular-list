import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawsListOverviewComponent } from './laws-list-overview.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('LawsListOverviewComponent', () => {
  let component: LawsListOverviewComponent;
  let fixture: ComponentFixture<LawsListOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [ LawsListOverviewComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawsListOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

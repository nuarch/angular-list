import { Component, TrackByFunction } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ViewModel } from '@nuarch/angular-lib';
import { faEdit } from '@fortawesome/free-regular-svg-icons';
import { viewModel } from '../../../../../models/view-model';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { get } from 'lodash';
import { LawListRouteService } from '../../../services/law-list-route.service';

@Component({
  selector: 'nuarch-laws-list-filter-filter-template',
  templateUrl: './laws-list-filter-filter-template.component.html',
  styleUrls: ['./laws-list-filter-filter-template.component.scss'],
})
export class LawsListFilterFilterTemplateComponent {

  filterGroup: FormGroup;
  viewModel: ViewModel = viewModel;
  get: Function = get;
  faEdit: IconDefinition = faEdit;

  constructor(private formBuilder: FormBuilder, private lawListRedirectService: LawListRouteService) {
    this.filterGroup = this.formBuilder.group({
      employeeStatus: ['Active'],
      title: ['Chief II'],
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  trackBy: TrackByFunction<string> = (index: number, item: any) => {
    return !!item && item.id;
  }

  selectRecord(id: number): void {
    // select record
    this.lawListRedirectService.redirectToEdit(id);
  }

}

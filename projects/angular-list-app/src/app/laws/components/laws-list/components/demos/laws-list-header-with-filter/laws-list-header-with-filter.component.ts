import { Component, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { filterModel } from '../../../../../models/filter-model';
import {
  CoreService,
  Entity,
  Filter,
  FilterComponent,
  FilterContext,
  FormModel,
  ListComponent,
  ViewModel,
} from '@nuarch/angular-lib';
import { viewModel } from '../../../../../models/view-model';
import { assign, get, invoke } from 'lodash';

@Component({
  selector: 'nuarch-laws-list-header-with-filter',
  templateUrl: './laws-list-header-with-filter.component.html',
  styleUrls: ['./laws-list-header-with-filter.component.scss'],
})
export class LawsListHeaderWithFilterComponent {

  filter: Observable<Filter>;
  viewModel: ViewModel = viewModel;
  filterModel: FormModel = filterModel;

  @ViewChild(ListComponent, {static: false}) listComponent: ListComponent;
  @ViewChild(FilterComponent, {static: false}) filterComponent: FilterComponent;

  constructor(private coreService: CoreService<Entity>) {
    this.filter = this.coreService.filter;
  }

  save(context: FilterContext): void {
    this.listComponent.applyFilter(assign({}, context.formValue, {legalPrefixId: get(context.formValue, 'legalPrefixId.correlation')}));
  }

  clear(): void {
    invoke(this.filterComponent, 'dynamicFormsComponent.dynamicFormGroup.reset');
    this.listComponent.cancelFilter();
  }

}

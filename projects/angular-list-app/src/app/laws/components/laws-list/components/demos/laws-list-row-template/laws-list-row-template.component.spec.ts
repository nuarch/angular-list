import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawsListRowTemplateComponent } from './laws-list-row-template.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';

describe('LawsListRowTemplateComponent', () => {
  let component: LawsListRowTemplateComponent;
  let fixture: ComponentFixture<LawsListRowTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FlexLayoutModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [
        LawsListRowTemplateComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawsListRowTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, TrackByFunction } from '@angular/core';
import { ViewModel } from '@nuarch/angular-lib';
import { viewModel } from '../../../../../models/view-model';
import { faEdit } from '@fortawesome/free-regular-svg-icons';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { formModel } from '../../../../../models/form-model';
import { FormModel } from '@nuarch/angular-lib';
import { LawListRouteService } from '../../../services/law-list-route.service';

@Component({
  selector: 'nuarch-laws-list-cards',
  templateUrl: './laws-list-cards.component.html',
  styleUrls: ['./laws-list-cards.component.scss'],
})
export class LawsListCardsComponent {

  viewModel: ViewModel = viewModel;
  faEdit: IconDefinition = faEdit;
  filterModel: FormModel = formModel;

  constructor(private lawListRedirectService: LawListRouteService) { }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  trackBy: TrackByFunction<string> = (index: number, item: any) => {
    return !!item && item.id;
  }

  selectRecord(id: number): void {
    // select record
    this.lawListRedirectService.redirectToEdit(id);
  }

}

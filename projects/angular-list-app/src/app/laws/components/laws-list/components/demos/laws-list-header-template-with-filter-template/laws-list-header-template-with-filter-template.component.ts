import { Component, ContentChild, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CoreService, Entity, Filter, FormModel, ViewModel, ListComponent, FilterComponent } from '@nuarch/angular-lib';
import { filterModel } from '../../../../../models/filter-model';
import { Observable } from 'rxjs';
import { viewModel } from '../../../../../models/view-model';
import { FilterContext } from '@nuarch/angular-lib';
import { get, assign, invoke } from 'lodash';

@Component({
  selector: 'nuarch-laws-list-header-template-with-filter-template',
  templateUrl: './laws-list-header-template-with-filter-template.component.html',
  styleUrls: ['./laws-list-header-template-with-filter-template.component.scss'],
})
export class LawsListHeaderTemplateWithFilterTemplateComponent {

  filter: Observable<Filter>;
  viewModel: ViewModel = viewModel;
  filterGroup: FormGroup;
  filterModel: FormModel = filterModel;
  @ViewChild(ListComponent, {static: false}) listComponent: ListComponent;
  @ViewChild(FilterComponent, {static: false}) filterComponent: FilterComponent;

  constructor(private coreService: CoreService<Entity>,
              private formBuilder: FormBuilder) {
    this.filter = this.coreService.filter;
    this.filterGroup = this.formBuilder.group({
      employeeStatus: ['Active'],
      title: ['Chief II'],
    });
  }

  save(): void {
    this.listComponent.applyFilter(this.filterGroup.getRawValue());
  }

  clear(): void {
    invoke(this.filterGroup, 'reset');
    this.listComponent.cancelFilter();
  }

}

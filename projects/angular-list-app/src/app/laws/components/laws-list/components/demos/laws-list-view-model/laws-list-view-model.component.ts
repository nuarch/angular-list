import { Component, OnInit } from '@angular/core';
import { ViewModel } from '@nuarch/angular-lib';
import { viewModel } from '../../../../../models/view-model';

@Component({
  selector: 'nuarch-laws-list-view-model',
  templateUrl: './laws-list-view-model.component.html',
  styleUrls: ['./laws-list-view-model.component.scss'],
})
export class LawsListViewModelComponent {

  viewModel: ViewModel = viewModel;

}

import { Component, TrackByFunction } from '@angular/core';
import { ColumnModel, ReferenceData, ViewModel } from '@nuarch/angular-lib';
import { viewModel } from '../../../../../models/view-model';
import { get, find, isEqual } from 'lodash';
import { faEdit } from '@fortawesome/free-regular-svg-icons';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { LawListRouteService } from '../../../services/law-list-route.service';

@Component({
  selector: 'nuarch-laws-list-row-template',
  templateUrl: './laws-list-row-template.component.html',
  styleUrls: ['./laws-list-row-template.component.scss'],
})
export class LawsListRowTemplateComponent {

  viewModel: ViewModel = viewModel;
  get: Function = get;
  faEdit: IconDefinition = faEdit;

  constructor(private lawListRedirectService: LawListRouteService) { }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  trackBy: TrackByFunction<string> = (index: number, item: any) => {
    return !!item && item.id;
  }

  selectRecord(id: number): void {
    // select record
    this.lawListRedirectService.redirectToEdit(id);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  getReferenceDataValue(row: any, column: ColumnModel, referenceData: ReferenceData): string {
    const value: string = get(row, column.value);
    if (column.referenceData) {
      return get(find(get(referenceData, column.referenceData), (refData: ReferenceData) => {
        return isEqual(get(refData, 'correlationId'), value) || isEqual(get(refData, 'correlation'), value);
      }), 'description') as string;
    }

    return undefined;
  }
}

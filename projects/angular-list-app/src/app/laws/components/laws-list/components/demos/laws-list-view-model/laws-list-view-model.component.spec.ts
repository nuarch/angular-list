import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawsListViewModelComponent } from './laws-list-view-model.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('LawsListViewModelComponent', () => {
  let component: LawsListViewModelComponent;
  let fixture: ComponentFixture<LawsListViewModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [
        LawsListViewModelComponent,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawsListViewModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawsListCardsComponent } from './laws-list-cards.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CoreModule } from '@nuarch/angular-lib';

describe('LawsListCardsComponent', () => {
  let component: LawsListCardsComponent;
  let fixture: ComponentFixture<LawsListCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [
        LawsListCardsComponent,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawsListCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

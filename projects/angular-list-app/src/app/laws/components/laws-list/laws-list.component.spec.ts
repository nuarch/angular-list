import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawsListComponent } from './laws-list.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ActivatedRoute, Router } from '@angular/router';
import { instance, mock } from 'ts-mockito';
import { RouterTestingModule } from '@angular/router/testing';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { ReferenceDataService } from '@nuarch/angular-lib';

describe('LawsListComponent', () => {
  let component: LawsListComponent;
  let fixture: ComponentFixture<LawsListComponent>;

  const mockActivatedRoute: ActivatedRoute = mock(ActivatedRoute);

  const mockRouter: Router = mock(Router);
  const mockReferenceDataService: ReferenceDataService = mock(ReferenceDataService);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        MatCardModule,
        RouterTestingModule,
        MatProgressSpinnerModule,
        MatInputModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        NoopAnimationsModule,
      ],
      declarations: [
        LawsListComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useFactory: () => instance(mockActivatedRoute),
        },
        {
          provide: Router,
          useFactory: () => instance(mockRouter),
        },
        {
          provide: ReferenceDataService,
          useFactory: () => instance(mockReferenceDataService),
        },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { lawsReferenceMetadata } from '../../models/laws-reference-metadata';
import { ReferenceDataService } from '@nuarch/angular-lib';

@Component({
  selector: 'nuarch-laws-list',
  templateUrl: './laws-list.component.html',
  styleUrls: ['./laws-list.component.scss'],
})
export class LawsListComponent implements OnInit {

  selected: AbstractControl = new FormControl(7);

  constructor(private referenceDataService: ReferenceDataService) {
  }

  ngOnInit(): void {
    this.referenceDataService.dispatchLoadReferenceData(lawsReferenceMetadata);
  }

}

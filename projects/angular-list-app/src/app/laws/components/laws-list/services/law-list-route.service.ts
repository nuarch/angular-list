import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { appRoutes } from '../../../../shared/constants/app-routes';

@Injectable()
export class LawListRouteService {

  constructor(private router: Router) {
  }

  redirectToEdit(id: number): void {
    this.router.navigate([`${appRoutes.laws}/${id}/edit`]);
  }

  redirectToAdd(): void {
    this.router.navigate([`${appRoutes.laws}/add`]);
  }

}

import { inject, TestBed } from '@angular/core/testing';
import { LawListRouteService } from './law-list-route.service';

describe('LawListRouteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LawListRouteService],
    });
  });

  it('should be created', inject([LawListRouteService], (service: LawListRouteService) => {
    expect(service).toBeTruthy();
  }));
});

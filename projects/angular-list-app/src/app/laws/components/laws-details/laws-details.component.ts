import { Component, ViewChild } from '@angular/core';
import { FormModel, NuDynamicFormsComponent } from '@nuarch/angular-lib';
import { formModel } from '../../models/form-model';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'nuarch-laws-details',
  templateUrl: './laws-details.component.html',
  styleUrls: ['./laws-details.component.scss'],
})
export class LawsDetailsComponent {

  @ViewChild(NuDynamicFormsComponent, { static: false }) private nuDynamicFormsComponent: NuDynamicFormsComponent;
  formModel: FormModel = formModel;
  formGroup: FormGroup;

  constructor( private formBuilder: FormBuilder ) {
    this.formGroup = this.formBuilder.group({
      id: [''],
      lawSection: ['test'],
      abbreviatedLaw: [''],
      lawDescription: [''],
      legalPrefixId: [''],
    });
  }

}

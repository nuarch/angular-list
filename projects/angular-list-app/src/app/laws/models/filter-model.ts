import { FormModel } from '@nuarch/angular-lib';
import { lawsReferenceMetadata } from './laws-reference-metadata';
import { NuDynamicElement, NuDynamicType } from '@nuarch/angular-lib';

export const filterModel: FormModel = {
  addTemplate: [
    {
      label: 'Law Section',
      name: 'lawSection',
      type: NuDynamicElement.Input,
      required: true,
      flex: 33,
      appearance: 'outline',
    },
    {
      label: 'Abbreviated Law',
      name: 'abbreviatedLaw',
      type: NuDynamicElement.Input,
      required: true,
      flex: 33,
      appearance: 'outline',
    },
    {
      label: 'Legal Prefix',
      name: 'legalPrefixId',
      type: NuDynamicElement.Autocomplete,
      referenceData: 'legalPrefixes',
      flex: 33,
      appearance: 'outline',
    },
  ],
  editTemplate: [
    {
      label: 'Law Section',
      name: 'lawSection',
      type: NuDynamicElement.Input,
      required: true,
      flex: 33,
      appearance: 'outline',
    },
    {
      label: 'Abbreviated Law',
      name: 'abbreviatedLaw',
      type: NuDynamicElement.Input,
      required: true,
      flex: 33,
      appearance: 'outline',
    },
    {
      label: 'Legal Prefix',
      name: 'legalPrefixId',
      type: NuDynamicElement.Autocomplete,
      referenceData: 'legalPrefixes',
      flex: 33,
      appearance: 'outline',
    },
  ],
  referenceData: lawsReferenceMetadata,
  sectionName: '',
  url: '',
};

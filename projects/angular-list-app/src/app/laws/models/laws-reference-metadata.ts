import {
  GenericReferenceData,
  ReferenceData,
} from '@smart/reference-data-management-ui-library';

export const legalPrefixes: string = 'legalPrefixes';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const lawsReferenceMetadata: { [type: string]: new(...args: any[]) => GenericReferenceData } = {
  [legalPrefixes]: ReferenceData,
};

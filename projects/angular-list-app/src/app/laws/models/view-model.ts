import { ViewModel } from '@nuarch/angular-lib';

export const viewModel: ViewModel = {
  columns: [
    {
      label: 'ID',
      value: 'id',
      sortable: true,
      flex: '5',
    },
    {
      label: 'Law Section',
      value: 'lawSection',
      sortable: true,
      flex: '7.5',
    },
    {
      label: 'Legal Prefix',
      value: 'legalPrefixId.correlation',
      sortable: false,
      flex: '10',
      type: 'rd-legalPrefixes',
    },
    {
      label: 'Abbreviated Law',
      value: 'abbreviatedLaw',
      sortable: true,
      flex: '12.5',
    },
    {
      label: 'Law Description',
      value: 'lawDescription',
      sortable: true,
      flex: '47.5',
    },
    {
      label: 'Created Date',
      value: 'createdDate',
      sortable: true,
      flex: '10',
      type: 'date',
    },
    {
      label: 'Action',
      value: 'action',
      sortable: false,
      flex: '7.5',
    },
  ],
  searchPlaceholder: 'Start typing to search by law section or abbreviated description',
  sectionName: 'Laws',
  url: 'lawmgmt/api/list.json',
  addButtonText: 'Add a Law',
  showAdd: true,
};

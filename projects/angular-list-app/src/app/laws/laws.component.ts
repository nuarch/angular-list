import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ReferenceDataService } from '@nuarch/angular-lib';
import { lawsReferenceMetadata } from './models/laws-reference-metadata';

@Component({
  selector: 'nuarch-laws',
  templateUrl: './laws.component.html',
  styleUrls: ['./laws.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LawsComponent implements OnInit {

  constructor(private referenceDataService: ReferenceDataService) { }

  ngOnInit(): void {
    this.referenceDataService.dispatchLoadReferenceData(lawsReferenceMetadata);
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { configureTestSuite } from './shared/functions/configure-test-suite';
import { PermissionsService } from './shared/modules/user/services/permissions.service';
import { instance, mock } from 'ts-mockito';
import { NgxPermissionsService } from 'ngx-permissions';
import { UserService } from './shared/modules/user/services/user.service';
import { EMPTY } from 'rxjs/index';

// TODO @Sandesh
xdescribe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  let mockNgxPermissionsService: NgxPermissionsService = mock(NgxPermissionsService);
  let ngxPermissionsService: NgxPermissionsService = instance(mockNgxPermissionsService);

  let mockPermissionsService: PermissionsService = mock(PermissionsService);
  let permissionsService: PermissionsService = instance(mockPermissionsService);
  configureTestSuite();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  beforeAll((done: any) => (async () => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        RouterTestingModule,
      ],
      declarations: [
        AppComponent,
      ],
      providers: [
        {
          provide: UserService,
          useValue: {
            user: EMPTY,
          },
        },
        {
          provide: PermissionsService,
          useValue: permissionsService,
        },
        {
          provide: NgxPermissionsService,
          useValue: ngxPermissionsService,
        },
      ],
    });
    await TestBed.compileComponents();
  })().then(done).catch(done.fail));

  it('should create the app', () => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', (done: DoneFn) => {
    expect(component).toBeTruthy();
    done();
  });

});

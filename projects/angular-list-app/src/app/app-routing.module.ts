import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GenericErrorComponent } from './shared/components/generic-error/generic-error.component';
import { AuthGuard } from './shared/functions/guards/auth-guard';
import { HomeRedirectComponent } from './shared/components/home-redirect/home-redirect.component';
import { LawsModule } from './laws/laws.module';

const routes: Routes = [
  {
    path: 'deny',
    component: GenericErrorComponent,
  },
  {
    path: 'error',
    component: GenericErrorComponent,
  },
  {
    path: 'laws',
    loadChildren: () => LawsModule,
  },
  {
    path: '',
    component: HomeRedirectComponent,
    canActivate: [AuthGuard],
  },
  {
    path: '**',
    redirectTo: 'laws',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule],
})
export class AppRoutingModule {
}

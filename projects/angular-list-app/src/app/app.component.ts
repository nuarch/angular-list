import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AbstractComponent } from './shared/components/abstract-component/abstract-component';
import { MatIconRegistry } from '@angular/material/icon';
import { UserService } from './shared/modules/user/services/user.service';
import { User } from './shared/modules/user/models/user';
import { NgxPermissionsService } from 'ngx-permissions';
import { isUndefined } from 'lodash';
import { PermissionsService } from './shared/modules/user/services/permissions.service';
import { filter } from 'rxjs/operators';


@Component({
  selector: 'nuarch-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent extends AbstractComponent {

  private user: User;
  loading: boolean = true;

  constructor(private domSanitizer: DomSanitizer,
              private iconRegistry: MatIconRegistry,
              private permissionsService: PermissionsService,
              private ngxPermissionsService: NgxPermissionsService,
              private userService: UserService) {
    super();
    this.iconRegistry.addSvgIconInNamespace(
      'assets', 'dsny',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/dsny.svg'),
    );
    this.userService.user
    .pipe(
      filter((user: User) => !isUndefined(user)),
    )
    .subscribe((user: User) => {
      this.user = user;
      this.loading = false;
      this.ngxPermissionsService.loadPermissions(this.permissionsService.permissions(user));
    });
  }

  afterOnDestroy(): void {
    // afterOnDestroy
  }

}

project = 'angular-list'

pipeline {
  agent {
    node {
      label 'smart'
    }
  }
  options {
    disableConcurrentBuilds()
    skipDefaultCheckout()
  }
  environment {
      SH_OUT = ""
      VERSION = ""
      TAG_NAME = ""
  }
  stages{
    stage('Preparation') {
      steps {
        slackSend (color: '#C1C1C1', message: "STARTED:\nJob '${env.JOB_NAME} [${env.BUILD_NUMBER}]'\n(${env.BUILD_URL})")

        checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "${env.BRANCH_NAME}"]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'WipeWorkspace'], [$class: 'CloneOption', depth: 0, noTags: false, reference: '', shallow: false], [$class: 'LocalBranch', localBranch: "${env.BRANCH_NAME}"]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'bitbucket', url: "https://bitbucket.org/nuarch/${project}.git"]]]

        script {
            gitTag = sh(returnStdout: true, script: "git describe --abbrev=0 --tags").trim()
            gitCommit = sh(returnStdout: true, script: 'git rev-parse --short HEAD | tr -d \'\n\'')

            sh "#!/bin/bash -l \n echo gitTag is ${gitTag}"
            sh "#!/bin/bash -l \n echo gitCommit is ${gitCommit}"

            VERSION = "'${gitTag}'-'${env.BUILD_NUMBER}''.'${gitCommit}"
            sh "#!/bin/bash -l \n echo 2 VERSION is ${VERSION}"
          }

        // Delete any existing local git tags (clean up failed operations)
        sh "#!/bin/bash -l \n git tag -l | xargs git tag -d"

        // Configure NPM
        sh "#!/bin/bash -l \n npm config set _auth ${NPM_AUTH}"
        sh "#!/bin/bash -l \n npm config set always-auth true"
        sh "#!/bin/bash -l \n npm config set email ${NPM_EMAIL}"
        sh "#!/bin/bash -l \n npm config set registry ${NPM_URL}"

        // Install node modules
        sh "#!/bin/bash -l \n npm install"
      }
    }
    stage('Lint') {
      steps {
        timeout(20) {
            sh "#!/bin/bash -l \n npm run lint"
        }
      }
    }
    stage('Test') {
      steps {
        timeout(20) {
            sh "#!/bin/bash -l \n npm run test:lib:ci"
        }
      }
    }
    stage('Build') {
      steps {
        timeout(50) {
            sh "#!/bin/bash -l \n npm run build:lib:ci"
        }
      }
    }
    stage('Publish') {
      steps {
        timeout(10) {
          dir('dist/nuarch/angular-lib') {
            script {
             if(env.BRANCH_NAME == 'develop') {
               sh "#!/bin/bash -l \n npm version '${VERSION}'"
               sh "#!/bin/bash -l \n npm publish --tag beta --registry '${NPM_PUBLISH_URL}'"
             } else if (env.BRANCH_NAME.startsWith('release/') ||
                        env.BRANCH_NAME.startsWith('feature/') ||
                        env.BRANCH_NAME.startsWith('hotfix/')) {
                  sh "#!/bin/bash -l \n npm version '${VERSION}'"
                  sh "#!/bin/bash -l \n npm publish --tag beta --registry '${NPM_PUBLISH_URL}'"
             }
            }
          }
        }
      }
    }
  }

  post {
    success {
      script {
        latestMessage = ''
        if (env.BRANCH_NAME == 'develop') {
          latestMessage = "\n---also tagged with 'latest'"
        }
      }
      slackSend (color: '#00FF00', message: "SUCCESSFUL\nJob '${env.JOB_NAME} [${env.BUILD_NUMBER}]")
    }
    failure {
      slackSend (color: '#FF0000', message: "FAILED\nJob '${env.JOB_NAME} [${env.BUILD_NUMBER}]'\n(${env.BUILD_URL})")
    }

  }

}
